package rdf.API;

import java.io.Serializable;

/**
 * Classe che modellizza la ruota sotto forma di array di interi
 *
 */
public class Wheel implements Serializable{
	
	public final static Integer[] values = {600, 400, 500, 300, 600, 1000, -1, 300, 400, 700, 500, 300, 400, -1, 300, 500, 400, 600, 500, 400, 300, -2, -3, 300};

}
