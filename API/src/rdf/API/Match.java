package rdf.API;

/**
 * Classe che contiene i dati di partita in corso.
 * Viene creata a partire da una lobby piena e mandata in partita dal server
 *
 */
public class Match extends LobbyData{

	public int match_id;
	public Manche[] manches = new Manche[5];
	public int[] jollies = {0, 0, 0};
	public int winner;
	
	/**
	 * Costruttore della classe Match
	 * @param lobby la lobby estesa
	 */
	public Match(LobbyData lobby) {
		super(lobby.getNumber(), lobby.getName(), lobby.getStatus(), lobby.getTime(),
				lobby.getPlayersList(), lobby.getObserversList());
	}
	
	/**
	 * Restituisce il punteggio della partita di ogni giocatore, calcolato dai punteggi delle manche
	 * @return array con i punteggi
	 */
	public int[] getScores()
	{
		int[] scores = {0, 0, 0};
		for(int j = 0; j < 5; j++)
		{
			if(manches[j] == null)
				break;
			int mancheWinner = manches[j].winner;
			scores[mancheWinner] += manches[j].points[mancheWinner];
		}
		
		return scores;
	}
}
