package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import rdf.API.*;

/**
 * Interfaccia (usata dal server) del modulo client che gestisce la partita
 *
 */
public interface GameManagerInterface extends Remote {
	
	/**
	 * metodo che avvia la partita nei client
	 * @param gameMaster oggetto remoto del gestore della partita lato server
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void startMatch(GameMasterInterface gameMaster, List<String> playersNameslist) throws RemoteException;
	
	/**
	 * aggiorna il tabellone delle lettere e dei punteggi
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void updateTable(Integer[] pos, char letter, int[] points) throws RemoteException;

	/**
	 * aggiorna il tabellone dei punteggi
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void updateScore(int[] points) throws RemoteException;
	
	/**
	 * aggiorna i jollies
	 * @param jollies array dei jollies
	 * @param order codice indicante l'utilizzo o consumo del jolly
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void updateJollies(int[] jollies, int order) throws RemoteException;
	
	/**
	 * inizio del nuovo turno
	 * @param order codice del giocatore corrente
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void newTurn(int order) throws RemoteException;

	/**
	 * inizio di una nuova manche
	 * @param SymbolsPositionsMap mappa contenente le associazioni carattere-posizione del tabellone iniziale
	 * @param nManche numero nuova manche
	 * @param theme tema della frase
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void newManche(HashMap<Character, Integer[]> SymbolsPositionsMap, int nManche, String theme) throws RemoteException;
	
	/**
	 * fine del turno corrente
	 * @param oldTurn dati del turno concluso
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void endTurn(Turn oldTurn) throws RemoteException;
	
	/**
	 * fine della manche corrente
	 * @param solution frase misteriosa
	 * @param usedLetters lettere usate
	 * @param nManche numero di manche finita
	 * @param points nuovo punteggio della partita
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void endManche(String solution, HashSet<Character> usedLetters, int nManche, int[] points) throws RemoteException;
	
	/**
	 * aggiorna il log dei messaggi
	 * @param text testo da aggiungere
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void updateLog(String text) throws RemoteException;
	
	/**
	 * comunica il risultato della ruota
	 * @param result risultato della ruota
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void spinResult(int result) throws RemoteException;
	
	/**
	 * inizializza il timer del tempo disponibile per eseguire una mossa
	 * @param t tempo
	 * @param gameSection tipo di mossa aspettata
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void setTimer(int t, int gameSection) throws RemoteException;
					
	/**
	 * termina la partita
	 * @param error se � terminata correttamente o a causa di errori
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	void endGame(boolean error) throws RemoteException;

	/**
	 * restituisce l'user del client
	 * @return user
	 * @throws RemoteException
	 */
	User getUser() throws RemoteException;

}
