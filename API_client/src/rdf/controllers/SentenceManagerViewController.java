package rdf.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;
import rdf.InstanceData;
import rdf.API.Sentence;
import rdf.API.StringUtilities;
import rdf.API.interfaces.SentenceManagerInterface;

/**
 * controller della finestra di gestione frasi
 *
 */
public class SentenceManagerViewController {

	@FXML
	private TabPane tpTabs;

    @FXML
    private Tab tbDB;

    @FXML
    private Button btSearch;
	
	@FXML
	private ComboBox<Character> cbInitial;
	
    @FXML
    private Button btInitial;

    @FXML
    private Button btAddDB;

    @FXML
    private Button btDeleteDB;

    @FXML
    private Button btSelectDeselectDB;

	@FXML
	private TextField tfDialogTheme;

	@FXML
	private TextArea taDialogSentence;

	@FXML
	private Label lbDialogSuccess;

	@FXML
	private Button btnDialog;

	@FXML
	private Text titleDialog;

    @FXML
    private Tab tbCSV;

	@FXML
	private TextField tfSearch;

	@FXML
	private TextArea taSentences;

	@FXML
	private ComboBox<String> cbNforPage;

	@FXML
	private TextField tfPage;

	@FXML
	private ListView<Sentence> lvSentencesDB;

	@FXML
	private ListView<Sentence> lvSentencesCSV;

	@FXML
	private Label lblLog;

	@FXML
	private ProgressIndicator piElaborationg;

	@FXML
	private Button btnUploadSelected;
	
    @FXML
    private Button btnDeleteSelected;

    @FXML
    private Button btnSelectDeselect;
    
    @FXML
    private Button btnLoadFile;
    
    @FXML
    private Button btPreviousPage;
    
    @FXML
    private Button btNextPage;

	SentenceManagerInterface sentenceManagerInterface;
	int sentenceN;
	boolean selectedAll;
	File file;
	List<Integer> errors;
	int errorLine;
	int pane = 0;

	int[] nForPage = new int[2];
	int[] sentencesToLoad = new int[] { 25, 25 };
	int[] page = new int[] { 1, 1 };
	int[] maxPage = new int[2];

	int dbSearchResult;
	LinkedList<Sentence> allSentences;
	ObservableList<Sentence> loadedSentences;
	ObservableList<Sentence> dbSentences;
	List<ListView<Sentence>> lvSentences;

	/**
	 * inizializza la finestra 
	 */
	public void initialize() {

		try {
			sentenceManagerInterface = InstanceData.stub.getSentenceManager();
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}

		Character letters[] = { ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
				'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
		cbInitial.setItems(FXCollections.observableArrayList(letters));
		cbInitial.getSelectionModel().selectFirst();
		String numbers[] = { "25", "50", "100", "500", "1K", "10K", "100K", "200K", "500K", "1M", "*" };
		cbNforPage.setItems(FXCollections.observableArrayList(numbers));
		cbNforPage.getSelectionModel().selectFirst();

		tfPage.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.matches("\\d*"))
				return;
			tfPage.setText(newValue.replaceAll("[^\\d]", ""));
		});

		lvSentencesCSV.setCellFactory(new Callback<ListView<Sentence>, ListCell<Sentence>>() {
			@Override
			public ListCell<Sentence> call(ListView<Sentence> listView) {
				return new CustomListCell();
			}
		});

		lvSentencesDB.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		lvSentencesDB.setCellFactory(new Callback<ListView<Sentence>, ListCell<Sentence>>() {
			@Override
			public ListCell<Sentence> call(ListView<Sentence> listView) {
				return new CustomListCell();
			}
		});

		lvSentencesCSV.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		lvSentences = new ArrayList<ListView<Sentence>>();
		lvSentences.add(lvSentencesDB);
		lvSentences.add(lvSentencesCSV);
		
		tpTabs.getSelectionModel().selectedItemProperty().addListener((ov, oldTab, newTab) -> {
			tabChanged(newTab);
		});
	}

	/**
	 * metodo per scorrere la pagina visualizzata
	 */
	@FXML
	public void changePage(ActionEvent ae) {

		if (tfPage.getText().equals("")) {
			tfPage.setText(page[pane] + "");
		} else {
			int newPage = Integer.parseInt(tfPage.getText());
			if (newPage < 1)
				page[pane] = 1;
			else if (newPage > maxPage[pane])
				page[pane] = maxPage[pane];
		}
		reloadSentences();
	}

	/**
	 * metodo per passare alla pagina successiva
	 */
	@FXML
	void loadNextPage(ActionEvent event) {

		if (page[pane] >= maxPage[pane])
			return;
		page[pane]++;
		Platform.runLater(() -> {
			tfPage.setText(page[pane] + "");
		});
		reloadSentences();
	}

	/**
	 * metodo per passare alla pagina precedente
	 */
	@FXML
	void loadPreviuosPage(ActionEvent event) {

		if (page[pane] <= 1)
			return;
		page[pane]--;
		Platform.runLater(() -> {
			tfPage.setText(page[pane] + "");
		});
		reloadSentences();
	}

	/**
	 * metodo per cambiare il numero di risultati per pagina
	 */
	@FXML
	void changeNforPage(ActionEvent event) {
		nForPage[pane] = cbNforPage.getSelectionModel().getSelectedIndex();
		recalculateVariables();
		reloadSentences();
	}

	/**
	 * metodo per ricalcolare le veriabili di servizio
	 */
	private void recalculateVariables() {
		page[pane] = 1;
		Platform.runLater(() -> {
			tfPage.setText(page[pane] + "");
		});

		String selectedN = cbNforPage.getSelectionModel().getSelectedItem();
		String prefix = selectedN.substring(0, selectedN.length() - 1);
		String suffix = selectedN.substring(selectedN.length() - 1);
		if (selectedN.equals("*"))
			sentencesToLoad[pane] = -1;
		else if (suffix.equals("K"))
			sentencesToLoad[pane] = Integer.parseInt(prefix + "000");
		else if (suffix.equals("M"))
			sentencesToLoad[pane] = Integer.parseInt(prefix + "000000");
		else
			sentencesToLoad[pane] = Integer.parseInt(selectedN);

		if (sentencesToLoad[pane] == -1)
			maxPage[pane] = 1;
		else {
			int totalSentencesN = pane == 0 ? dbSearchResult : allSentences.size();
			int newMaxPage = totalSentencesN / sentencesToLoad[pane];
			if (totalSentencesN % sentencesToLoad[pane] != 0)
				newMaxPage += 1;

			maxPage[pane] = newMaxPage;
		}
	}

	/**
	 * metodo per gestire il cambiamento di tab
	 * @param newTab
	 */
	private void tabChanged(Tab newTab) {
		pane = (pane + 1) % 2;
		cbNforPage.getSelectionModel().select(nForPage[pane]);

		Platform.runLater(() -> {
			tfPage.setText(page[pane] + "");
		});
	}

	/**
	 * metodo per caricare un file CSV
	 */
	@FXML
	void loadCSV(ActionEvent event) {

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open CSV File");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("CSV File", "*.csv"));
		file = fileChooser.showOpenDialog(null);

		if (file == null || !file.isFile())
			return;

		readCSVSentences();
		recalculateVariables();
		reloadSentences();
	}

	/**
	 * metodo per attivare o disattivare i bottoni
	 * @param value booleana che indica il valore dei bottoni
	 */
	private void buttonSwitch(boolean value) {
		if(pane == 0)
		{
			tbCSV.setDisable(!value);
			lvSentencesDB.setDisable(!value);
			btSearch.setDisable(!value);
			tfSearch.setDisable(!value);
			cbInitial.setDisable(!value);
			btInitial.setDisable(!value);
			btAddDB.setDisable(!value);
			btDeleteDB.setDisable(!value);
			btSelectDeselectDB.setDisable(!value);
		}
		else
		{
			tbDB.setDisable(!value);
			lvSentencesCSV.setDisable(!value);
			btnUploadSelected.setDisable(!value);
		    btnDeleteSelected.setDisable(!value);
		    btnSelectDeselect.setDisable(!value);
		    btnLoadFile.setDisable(!value);
		}
		cbNforPage.setDisable(!value);
		btNextPage.setDisable(!value);
		btPreviousPage.setDisable(!value);
		tfPage.setDisable(!value);
	}
	
	/**
	 * metodo per cancellare le frasi selezionate
	 */
	@FXML
	void deleteSelected(ActionEvent event) {

		buttonSwitch(false);
		piElaborationg.setVisible(true);
		new Thread(new Runnable() {
			public void run() {
				
				if (pane == 0) {
					ObservableList<Sentence> selectedSentences = lvSentencesDB.getSelectionModel().getSelectedItems();
					List<Sentence> sentences = new ArrayList<>(selectedSentences);
					try {
						sentenceManagerInterface.deleteSentences(sentences);
					} catch (RemoteException e) {
						InstanceData.interruptClient();
					}
				} else {
					ObservableList<Sentence> sentences = lvSentencesCSV.getSelectionModel().getSelectedItems();
					for (Sentence s : sentences)
						allSentences.remove(s);
				}
				
				Platform.runLater(() -> {
					recalculateVariables();
					reloadSentences();
					buttonSwitch(true);
					piElaborationg.setVisible(false);
				});
			}
		}).start();

	}

	/**
	 * metodo per selezionare o deselezionare tutte le frasi
	 */
	@FXML
	void selectDeselectAll(ActionEvent event) {
		
		buttonSwitch(false);
		piElaborationg.setVisible(true);
		new Thread(new Runnable() {
			public void run() {
				int selectedN = lvSentences.get(pane).getSelectionModel().getSelectedIndices().size();
				if(selectedN <= 1)
				{
					lvSentences.get(pane).getSelectionModel().selectAll();
					selectedAll = true;
				}
				else if(selectedN == sentencesToLoad[pane])
				{
					lvSentences.get(pane).getSelectionModel().clearSelection();
					selectedAll = false;
				}
				else {
					if(selectedAll)
						lvSentences.get(pane).getSelectionModel().clearSelection();
					else
						lvSentences.get(pane).getSelectionModel().selectAll();
					selectedAll = !selectedAll;
				}
				
				buttonSwitch(true);
				piElaborationg.setVisible(false);
			}
		}).start();		
	}

	/**
	 * metodo per caricare sul DB le frasi selezionata
	 */
	@FXML
	void uploadSelected(ActionEvent event) {

		buttonSwitch(false);
		piElaborationg.setVisible(true);
		new Thread(new Runnable() {
			private boolean result;

			public void run() {
				ObservableList<Sentence> selected = lvSentencesCSV.getSelectionModel().getSelectedItems();
				if (selected == null) {
					return;
				}

				List<Sentence> sentences = new ArrayList<>(selected);

				result = false;
				try {
					result = sentenceManagerInterface.insertSentences(sentences);
				} catch (RemoteException e) {
					InstanceData.interruptClient();
				}

				buttonSwitch(true);
				piElaborationg.setVisible(false);
				deleteSelected(event);
				Platform.runLater(() -> {
					lblLog.setText(result ? "Upload completed." : "Upload failed");
				});
			}
		}).start();

	}

	/**
	 * metodo per aprire il pop-up quando si clicca sulle frasi
	 */
	@FXML
	void clickOnSentence(MouseEvent event) {
		if (event.getButton() == MouseButton.SECONDARY) {
			Sentence sentence = lvSentences.get(pane).getSelectionModel().getSelectedItem();
			openDialog(false, sentence);
		}
	}

	/**
	 * metodo per aprire il pop-up per creare una nuova frase sul DB
	 */
	@FXML
	public void addSentenceToDBPopUp() {
		openDialog(true, null);
	}

	/**
	 * metodo per aggiungere la frase sul db
	 * @param sentence frase da aggiungere
	 * @return booleana di conferma operazione
	 */
	public int addSentence(Sentence sentence) {
		int result = -1;
		try {
			result = sentenceManagerInterface.insertSentence(sentence) ? 1 : 0;
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}

		recalculateVariables();
		reloadSentences();

		return result;
	}

	/**
	 * metodo per aggiornare una frase
	 * @param sentence frase aggiornata
	 */
	public void updateSentence(Sentence sentence) {
		if (pane == 0) {
			try {
				sentenceManagerInterface.updateSentence(sentence);
			} catch (RemoteException e) {
				InstanceData.interruptClient();
			}
		} else {
			Sentence oldSentence = lvSentencesCSV.getSelectionModel().getSelectedItem();
			int index = allSentences.indexOf(oldSentence);
			allSentences.set(index, sentence);
		}
		reloadSentences();
	}
	
	/**
	 * metodo per aprire il pop-up
	 * @param dialogType tipo di pop-up
	 * @param sentence frase da usare nel pop-up
	 */
	public void openDialog(boolean dialogType, Sentence sentence) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/rdf.view/DialogSentence.fxml"));
			Stage dialog = new Stage();
			dialog.setTitle(dialogType ? "Aggiungere frase" : "Modificare frase");
			Scene scene = new Scene(loader.load());
			dialog.setScene(scene);
			SentenceManagerDialogsController controller = loader.getController();
			controller.initialize(scene, this, dialogType, sentence);
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.initStyle(StageStyle.UNDECORATED);
			dialog.showAndWait();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * metodo per leggere le frasi dal CSV caricato
	 */
	public void readCSVSentences() {

		allSentences = new LinkedList<>();
		sentenceN = 1;
		errorLine = 0;
		errors = new ArrayList<>();
		try (Stream<String> lines = Files.lines(Paths.get(file.getPath()))) {
			lines.forEach(s -> {
				errorLine++;
				String[] data = s.split(",");
				if (data.length == 2 && StringUtilities.splitSentenceString(data[0]) != null)
					allSentences.add(new Sentence(sentenceN++, data[0], data[1]));
				else
					errors.add(errorLine);
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String log = "";
		if (errors.size() > 0) {
			log = errors.size() + " sentences not loaded. rows: ";
			for (int i : errors)
				log += i + " ";
		}
		lblLog.setText(log);
		lblLog.setTooltip(new Tooltip(log));
	}
	
	/**
	 * metodo per iniziare la ricerca delle frasi sul DB
	 */
	@FXML
	void seachSentences(ActionEvent event) {

		String search = tfSearch.getText().toUpperCase();
		try {
			dbSearchResult = sentenceManagerInterface.getSentencesCount(cbInitial.getSelectionModel().getSelectedItem(),
					search.equals("") ? null : search);
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}

		recalculateVariables();
		reloadSentences();
	}

	/**
	 * metodo per ricaricare le frasi visualizzate
	 */
	public void reloadSentences() {

		if (pane == 1 && allSentences == null)
			return;

		if (pane == 0)
			reloadDBSentences();
		else
			reloadCSVSentences();
	}

	/**
	 * metodo per ricaricare le frasi del DB
	 */
	public void reloadDBSentences() {
		lvSentencesDB.getItems().clear();
		Sentence[] sentences = null;
		String search = tfSearch.getText().toUpperCase();
		try {
			sentences = sentenceManagerInterface.getSentences(cbInitial.getSelectionModel().getSelectedItem(),
					search.equals("") ? null : search, page[pane] - 1, sentencesToLoad[pane]);

		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}

		dbSentences = FXCollections.observableArrayList();

		for (Sentence s : sentences)
			if (s != null)
				dbSentences.add(s);

		if (dbSentences.size() > 0) {
			lvSentencesDB.setItems(dbSentences);
		}
	}

	/**
	 * metodo per ricaricare le frasi del CSV
	 */
	public void reloadCSVSentences() {
		// sentenceN = ((page[pane] - 1) * sentencesToLoad[pane]) + 1;
		loadedSentences = FXCollections.observableArrayList();

		if (sentencesToLoad[pane] == -1)
			loadedSentences.addAll(allSentences);
		else {
			for (int i = 0; i < sentencesToLoad[pane]; i++) {
				int n = (page[pane] - 1) * sentencesToLoad[pane] + i;
				if (n > allSentences.size() - 1)
					break;
				loadedSentences.add(allSentences.get(n));
			}
		}

		lvSentencesCSV.setItems(loadedSentences);
	}

	/**
	 * metodo per ritornare alla home
	 */
	@FXML
	void gotoHome(ActionEvent event) {
		InstanceData.gotoHome();
	}

	/**
	 * classe usata nella finestra, per visualizzare le frasi
	 *
	 */
	private class CustomListCell extends ListCell<Sentence> {
		private HBox content;
		private Text sentence;
		private Text theme;
		private Text n;

		public CustomListCell() {
			super();
			sentence = new Text();
			theme = new Text();
			n = new Text();
			VBox vBox = new VBox(sentence, theme);
			content = new HBox(n, vBox);
			content.setSpacing(10);
		}

		@Override
		protected void updateItem(Sentence item, boolean empty) {
			super.updateItem(item, empty);
			if (item != null && !empty) { // <== test for null item and empty parameter
				sentence.setText(item.sentence);
				theme.setText(item.theme);
				n.setText(item.sentence_id + "");
				setGraphic(content);
			} else {
				setGraphic(null);
			}
		}
	}
}
