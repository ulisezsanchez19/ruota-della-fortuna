package rdf.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import rdf.DatabaseConnection;
import rdf.serverRdF;

/**
 * Oggetto DAO che si occupa di query generali. Non viene usato direttamente, ma viene
 * usato dagli altri oggetti DAO.
 *
 */
class GenericDAOImpl {
	
	Connection connection;
	
	/**
	 * costruttore generale che instanzia la connessione
	 */
	GenericDAOImpl()
	{
		connection = DatabaseConnection.getDatabaseConnection().getConnection();
	}

	/**
	 * metodo per eseguire insert sotto forma di batch
	 * @param names array formato da (nome tabella, nomi colonne)
	 * @param objects array formato dai valori da inserire
	 * @param replace booleana per ignorare i conflitti
	 * @return
	 */
	boolean insertBatch(String[] names, Object[] objects, boolean replace) {
		
		if(connection == null || names.length < 2 || objects.length % (names.length - 1) != 0) return false;
		
		try {
			
			String sql = generateInsertSQL(names, "", replace);
			PreparedStatement statement = connection.prepareStatement(sql);
			
			int column = 1;
			for(Object o : objects)
			{
				statement = setStatementVariable(statement, column++, o);
				if(column == names.length)		
				{	
					column = 1;
					statement.addBatch();
					
				}
			}
			
			statement.executeBatch();
			if(serverRdF.DEBUG_QUERY)
				System.out.println(statement);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * metodo per eseguire insert
	 * @param names array formato da (nome tabella, nomi colonne)
	 * @param objects array formato dai valori da inserire
	 * @param returningId colonna da ritornare dopo la insert
	 * @return l'id specificato in returningId
	 */
	int insert(String[] names, Object[] objects, String returningId) {
		
		if(connection == null || names.length < 2 || objects.length % (names.length - 1) != 0) return -1;
		
		try {
			
			String sql = generateInsertSQL(names, returningId, false);
			PreparedStatement statement = connection.prepareStatement(sql);
			
			int column = 1;
			for(Object o : objects)
			{
				setStatementVariable(statement, column++, o);
			}
			
			if(serverRdF.DEBUG_QUERY)
				System.out.println(statement);
			if(returningId == "")
			{
				statement.executeUpdate();
				return 0;
			}
			
			ResultSet result = statement.executeQuery();
			int id = -1;
			while (result.next()) {
				id =  result.getInt(returningId);
			}
			
			return id;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return -1;
	}

	/**
	 * metodo per eseguire update
	 * @param names array formato da (nome tabella, nomi colonne, nome colonna da confrontare)
	 * @param objects array formato dai valori da inserire e il valore da confrontare
	 * @return booleana di conferma operazione
	 */
	boolean update(String[] names, Object[] objects) {
		
		if(connection == null || names.length < 3 || (objects.length) % (names.length - 1) != 0) return false;
		
		try {
			String sql = generateUpdateSQL(names);
			PreparedStatement statement = connection.prepareStatement(sql);
			
			int column = 1;
			for(Object o : objects)
			{
				setStatementVariable(statement, column++, o);
			}
			
			if(serverRdF.DEBUG_QUERY)
				System.out.println(statement);
			statement.executeUpdate();
			
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * metodo per eseguire delete sotto forma di batch
	 * @param names names array formato da (nome tabella, nomi colonne da confrontare)
	 * @param objects array formato dai valori da confrontare
	 * @return boleana di conferma
	 */
	boolean deleteBatch(String[] names, Object[] objects)
	{
		if(connection == null || names.length < 2 || objects.length % (names.length - 1) != 0) return false;
		
		try {
			String sql = "DELETE FROM " + names[0] + " WHERE " + names[1] + " = ?";
			PreparedStatement statement = connection.prepareStatement(sql);
			
			int column = 1;
			for(Object o : objects)
			{
				statement.setInt(column, (int) o);
				statement.addBatch();
				if(serverRdF.DEBUG_QUERY)
					System.out.println(statement);
			}
			
			statement.executeBatch();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
		
	}
	
	/**
	 * metodo per generare una stringa SQL per un insert a partire dai parametri inseriti
	 * @param names array formato da (nome tabella, nomi colonne)
	 * @param returningId colonna da ritornare dopo la insert
	 * @param ignoreConflict booleana per ignorare i conflitti
	 * @return la stringa SQL
	 */
	private String generateInsertSQL(String[] names, String returningId, boolean ignoreConflict)
	{
		String columns = "";
		String variables = "";
		for(int i = 1; i < names.length; i++)
		{
			columns += names[i];
			variables += "?";
			if(i < names.length - 1)
			{
				columns += ", ";
				variables += ", ";
			}
		}
		
		String conflict = ignoreConflict ? "ON CONFLICT DO NOTHING " : "";
		String returning = returningId == "" ? ";" : "RETURNING " + returningId + ";";
		String sql = "INSERT INTO " + names[0] + " (" + columns + ") VALUES (" + variables + ")";
		
		return sql + conflict + returning;
	}

	/**
	 * metodo per generare una stringa SQL per un update a partire dai parametri inseriti
	 * @param names
	 * @return la stringa SQL
	 */
	private String generateUpdateSQL(String[] names)
	{
		String variables = "";
		for(int i = 1; i < names.length - 1; i++)
		{
			variables += names[i] + " = ?";
			if(i < names.length - 2)
			{
				variables += ", ";
			}
		
		}
		
		String sql = "UPDATE " + names[0] + " SET " + variables + " WHERE " + names[names.length - 1] + " = ?";
		return sql;
	}
	
	/**
	 * metodo per settare una variabile in un PreparedStatement
	 * @param statement lo statement
	 * @param column colonna della variabile
	 * @param o la variabile
	 * @return
	 * @throws SQLException
	 */
	private PreparedStatement setStatementVariable(PreparedStatement statement, int column, Object o) throws SQLException
	{
		if(o instanceof Integer)
			statement.setInt(column, (int) o);
		else if(o instanceof String)
			statement.setString(column, (String) o);
		else if(o instanceof Timestamp)
			statement.setTimestamp(column, (Timestamp) o);
		else if(o == null)
			statement.setNull(column, Types.INTEGER);
		else
			statement.setObject(column, o);
		
		return statement;
	}
	
	/**
	 * metodo per rilasciare la connessione
	 * @return boleana di conferma
	 */
	public boolean releaseConnection()
	{
		return DatabaseConnection.getDatabaseConnection().releaseConnection(connection);
	}

}
