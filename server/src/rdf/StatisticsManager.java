package rdf;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rdf.API.User;
import rdf.API.interfaces.StatisticsManagerInterface;

/**
 * modulo per la gestione delle statistiche
 *
 */
public class StatisticsManager extends UnicastRemoteObject implements StatisticsManagerInterface {

	final int[] queryFormat = new int[] {2, 2, 2, 2, 2, 2, 4, 1};
	List<String[]> gameStatisticsCache; // cancellate dopo 30 minuti
	String[] rdfStatisticsCache; // cancellate dopo 5 minuti
	
	protected StatisticsManager() throws RemoteException {
		super();
	}
	
	/**
	 * restituisce le statistiche relative a tutte le partite
	 * @return statistiche
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public List<String[]> getAllGamesStatics() throws RemoteException {
		
		List<String[]> result = new ArrayList<>();
		for(int i = 0; i < queryFormat.length; i++)
		{
			result.add(loadMoreResult(i, 1));
		}
		
		return result;
	}
	
	/**
	 * carica altri risultati per una statistica
	 * @param statistic la statistica
	 * @param rows limita i risultati
	 * @return statistiche
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public String[] loadMoreResult(int statistic, int rows) throws RemoteException {
		
		if(gameStatisticsCache == null)	
			refreshGamesStatics();
		if(statistic >= queryFormat.length) return null;
		
		int limit = rows > RdFStatistics.LIMIT ? RdFStatistics.LIMIT : rows;
		int totalResults = limit * queryFormat[statistic];
		
		if(gameStatisticsCache.get(statistic).length < totalResults)
			totalResults = gameStatisticsCache.get(statistic).length;
		
		String[] result = new String[totalResults];
		for(int i = 0; i < totalResults; i++)
			result[i] = gameStatisticsCache.get(statistic)[i];
		
		return result;
	}
	
	/**
	 * metodo che ottiene le statistiche di gioco e setta il timer per cancellarle 
	 */
	private void refreshGamesStatics()
	{
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
		  @Override
		  public void run() {
			  gameStatisticsCache = null;
		  }
		}, 1000*60*30);
		
		RdFStatistics statisticsObj = new RdFStatistics();
		gameStatisticsCache =  statisticsObj.getGamesStatics();
	}

	/**
	 * restituisce le statistiche del singolo giocatore
	 * @param user giocatore
	 * @return statistiche
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public String[] getUserStatics(User user) throws RemoteException {
		
		RdFStatistics statisticsObj = new RdFStatistics();
		return statisticsObj.getUserStatics(user);
	}

	/**
	 * restituisce le statistiche della piattaforma
	 * @return statistiche
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public String[] getRdFStatistics() throws RemoteException {
		
		if(rdfStatisticsCache == null)	
		{
			String[] result = new String[9];
			
			RdFStatistics statisticsObj = new RdFStatistics();
			result[0] = statisticsObj.getTotalUsers();
			result[1] = serverRdF.clients.keySet().size() + "";
			result[2] = serverRdF.lobbyManager.listeners.size() + "";
			result[3] = serverRdF.lobbyManager.playersMap.size() + "";
			result[4] = serverRdF.lobbyManager.observersMap.size() + "";
			result[5] = statisticsObj.getTotalMatch();
			result[6] = serverRdF.lobbyManager.gamesMap.keySet().size() + "";
			result[7] = statisticsObj.getTotalManche();
			result[8] = statisticsObj.getTotalSentences();
			
			rdfStatisticsCache = result;
			
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
				  rdfStatisticsCache = null;
			  }
			}, 1000*60*5);
		}
		
		return rdfStatisticsCache;
	}
}
