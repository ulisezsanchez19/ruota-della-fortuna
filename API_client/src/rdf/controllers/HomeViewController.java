package rdf.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import rdf.InstanceData;

/**
 * controller della finestra home
 *
 */
public class HomeViewController {

    @FXML
    private Button btnLobby;

    @FXML
    private Button btnRankings;

    @FXML
    private Button btnProfile;

    @FXML
    private Button btnStatistics;

    @FXML
    private Button btnSentenceManager;

    @FXML
    private MenuButton mbtnUsername;

    @FXML
    private MenuItem btnLogout;
    
	/**
	 * metodo per inizializzare la finestre.
	 * Se l'utente � amministratore, abiliter� il pannello di controllo
	 */
	public void initialize()
	{
		btnSentenceManager.setDisable(!InstanceData.isAdmin);
		btnSentenceManager.setVisible(InstanceData.isAdmin);
		
		mbtnUsername.setText(InstanceData.user.getUsername());
	}
	
    /**
     * metodo usato per cambiare scena
     */
    @FXML
    void gotoScene(ActionEvent event) {

    	String id = "";
		if(event.getSource() instanceof Control)
			id = ((Control) event.getSource()).getId();
		else if(event.getSource() instanceof MenuItem)
			id = ((MenuItem) event.getSource()).getId();		
    	
    	switch(id)
    	{
    	case "btnLobby":
    		InstanceData.gotoLobbyList();
    		break;
    	case "btnRankings":
    		InstanceData.gotoRankings();
    		break;
    	case "btnProfile":
        	InstanceData.gotoProfile();
    		break;
    	case "btnStatistics":
        	InstanceData.gotoStatistics();
    		break;
    	case "btnSentenceManager":
        	InstanceData.gotoSentencesManager();
    		break;
    	case "btnLogout":
    		InstanceData.returnToAuthentication(true);
    		break;
    	}
    }

}

