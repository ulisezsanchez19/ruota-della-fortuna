package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import rdf.API.LobbyData;

/**
 * interfaccia (usata dal server) del modulo client che gestisce le lobby
 *
 */
public interface LobbyListenerInterface extends Remote {

	/**
	 * aggiorna l'elenco delle lobby
	 * @param command codice della modifica da effetuare
	 * @param newData lobby
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void updateLobbyData(Integer command, LobbyData newData) throws RemoteException;

	/**
	 * invia messaggi alla lobby
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void sendMessage(String message) throws RemoteException;

}
