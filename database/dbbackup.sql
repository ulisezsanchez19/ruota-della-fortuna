--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES (1, 'pippoName', 'pippoSurname', 'pippo', 'pippo@email', '202CB962AC59075B964B07152D234B70', false);
INSERT INTO public.users VALUES (2, 'plutoName', 'plutoSurname', 'pluto', 'pluto@email', '202CB962AC59075B964B07152D234B70', false);
INSERT INTO public.users VALUES (3, 'topolinoName', 'topolinoSurname', 'topolino', 'topolino@email', '202CB962AC59075B964B07152D234B70', false);
INSERT INTO public.users VALUES (4, 'marioName', 'marioSurname', 'mario', 'mario@email', '202CB962AC59075B964B07152D234B70', false);
INSERT INTO public.users VALUES (5, 'luigiName', 'luigiSurname', 'luigi', 'luigi@email', '202CB962AC59075B964B07152D234B70', false);
INSERT INTO public.users VALUES (6, 'adminName', 'adminSurname', 'admin', 'admin@email', '202CB962AC59075B964B07152D234B70', true);


--
-- Data for Name: matches; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.matches VALUES (1, 'walter', '2019-12-07 13:27:40.571', 3, 2);
INSERT INTO public.matches VALUES (2, 'walter', '2019-12-07 13:38:12.476', NULL, 3);
INSERT INTO public.matches VALUES (3, 'walter', '2019-12-07 13:44:02.693', NULL, 3);
INSERT INTO public.matches VALUES (4, 'walter', '2019-12-07 14:09:23.242', 2, 2);
INSERT INTO public.matches VALUES (5, 'walter', '2019-12-10 00:31:34.549', NULL, 3);


--
-- Data for Name: sentences; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sentences VALUES (1, 'LA RUOTA DELLA FORTUNA', 'NOME DEL GIOCO');
INSERT INTO public.sentences VALUES (2, 'SOPRA LA PANCA LA CAPRA CAMPA', 'SCIOGLILINGUA');
INSERT INTO public.sentences VALUES (3, 'PIPPO PLUTO TOPOLINO', 'NOMI DISNEY');
INSERT INTO public.sentences VALUES (4, 'HELLO WORLD', 'PRORGAMMATORE DOC');
INSERT INTO public.sentences VALUES (5, 'CIAOCIAO TELETUBBIES, È L’ORA DEL LETTINO', 'IL SOLE SPLENDE, GIOCA A NASCONDINO');
INSERT INTO public.sentences VALUES (6, 'LO PUOI TROVARE SUL CANALE QUARANTASEI', 'CARTOONITO');
INSERT INTO public.sentences VALUES (7, 'I CIBI VANNO DICHIARATI ALLE AUTORITÀ COMPETENTI', 'AIRPORT SECURITY');
INSERT INTO public.sentences VALUES (8, 'CHI CONOSCE IL CODICE BINARIO E CHI NO', 'CI SONO 10 TIPI DI PERSONE');
INSERT INTO public.sentences VALUES (9, 'AIUTA TOPOLINO NELLE SUE ELETTRIZZANTI AVVENTURE', 'LO STRUMENTOPOLO MISTERIOSO');
INSERT INTO public.sentences VALUES (10, 'RACCONTA LA SCIENZA E LA TECNOLOGIA A SUPER QUARK', 'ANGELA AL LAVORO');
INSERT INTO public.sentences VALUES (11, 'KABUL È LA CAPITALE DELL’AFGHANISTAN, DOHA DEL QATAR', 'NOZIONI DI GEOGRAFIA');
INSERT INTO public.sentences VALUES (12, 'CESARE DISSE QUESTA FRASE OLTREPASSANDO IL RUBICONE', 'IL DADO È TRATTO');
INSERT INTO public.sentences VALUES (13, 'VINSERO MOLTE BATTAGLIE GRAZIE ALLA LORO FOGA', 'LE AMAZZONI');
INSERT INTO public.sentences VALUES (14, 'ERA SITUATA AL DI LÀ DELLE COLONNE D’ERCOLE', 'ATLANTIDE');
INSERT INTO public.sentences VALUES (15, 'USÒ L’INGANNO PER CONCLUDERE LA GUERRA E FU PUNITO', 'ULISSE');
INSERT INTO public.sentences VALUES (16, 'THOR È FIGLIO DI ODINO E FRATELLO DEL MALVAGIO LOKI', 'MITOLOGIA NORDICA');
INSERT INTO public.sentences VALUES (17, 'BAST ERA UNA DIVINITÀ CON LA TESTA DA FELINO', 'NELL’ANTICO EGITTO');
INSERT INTO public.sentences VALUES (18, 'UN OBIETTIVO È UN SOGNO CON UNA DATA DI SCADENZA', 'VITA');
INSERT INTO public.sentences VALUES (19, 'RIEMPI LA TUA VITA DI ESPERIENZE, NON DI COSE MATERIALI!', 'VITA');
INSERT INTO public.sentences VALUES (20, 'FAI IN MODO CHE LA FELICITÀ SIA IL TUO UNICO VIZIO', 'FELICITÀ');
INSERT INTO public.sentences VALUES (21, 'QUASI QUASI MOLLO TUTTO E DIVENTO FELICE', 'FELICITÀ');
INSERT INTO public.sentences VALUES (22, 'HO DECISO DI ESSERE FELICE PERCHÉ FA BENE ALLA MIA SALUTE', 'SUCCESSO');
INSERT INTO public.sentences VALUES (23, 'NON SMETTERE DI SOGNARE PERCHÈ UNA VOLTA HAI AVUTO UN INCUBO', 'SUCCESSO');
INSERT INTO public.sentences VALUES (24, 'NON HO PAURA DELLA MORTE MA HO PAURA DELL’AMORE', 'AMORE');
INSERT INTO public.sentences VALUES (25, 'SCEGLI UNA PERSONA CHE TI GUARDI COME SE FOSSE UNA MAGIA', 'AMORE');
INSERT INTO public.sentences VALUES (26, 'LE PERSONE FANNO SEMPRE COSE PAZZE, QUANDO SONO INNAMORATE', 'PERSONE');
INSERT INTO public.sentences VALUES (27, 'DENTRO OGNI PERSONA CI SONO SACRIFICI CHE LA GENTE NON VEDE', 'PERSONE');
INSERT INTO public.sentences VALUES (28, 'ERA UN SEMPLICE SGUARDO DALLE MILLE PAROLE', 'SGUARDO');
INSERT INTO public.sentences VALUES (29, 'OGNI OCCHIO HA IL SUO SGUARDO', 'SGUARDO');
INSERT INTO public.sentences VALUES (30, 'HO AFFOGATO I MIEI DOLORI, MA LORO HANNO IMPARATO A NUOTARE', 'ARTE');
INSERT INTO public.sentences VALUES (31, 'DIPINGO I FIORI PER NON FARLI MORIRE', 'ARTE');
INSERT INTO public.sentences VALUES (32, 'TUTTO QUELLO CHE HAI VUOI È DALL’ALTRO LATO DELLA PAURA', 'PAURA');
INSERT INTO public.sentences VALUES (33, 'SCOPRI CHI SEI E NON AVERE PAURA DI ESSERLO', 'PAURA');
INSERT INTO public.sentences VALUES (34, 'I GRANDI AVVENIMENTI DEL MONDO HANNO LUOGO NEL CERVELLO', 'MONDO');
INSERT INTO public.sentences VALUES (35, 'I SOGNATORI SONO I VERI SALVATORI DEL MONDO', 'MONDO');
INSERT INTO public.sentences VALUES (36, 'SE I TUOI SOGNI NON TI SPAVENTANO… SOGNA PIÙ GRANDE!', 'SOGNI');
INSERT INTO public.sentences VALUES (37, 'CHI RINUNCIA AI PROPRI SOGNI È DESTINATO A MORIRE', 'SOGNI');
INSERT INTO public.sentences VALUES (38, 'È L’ATTIVITÀ CHE CONVERTE I SOGNI DEGLI NELLA LORO REALTÀ', 'SOGNI');
INSERT INTO public.sentences VALUES (39, 'FAI DELLA TUA VITA UN SOGNO, E DI UN SOGNO, UNA REALTÀ', 'SOGNI');
INSERT INTO public.sentences VALUES (40, 'I SOGNI SI AVVERANO, SE SI HA IL CORAGGIO DI CREDERCI', 'SOGNI');
INSERT INTO public.sentences VALUES (41, 'NON ABBIAMO POCO TEMPO, MA NE ABBIAMO PERDUTO MOLTO', 'TEMPO');
INSERT INTO public.sentences VALUES (42, 'IL TEMPO SCOPRE LA VERITÀ', 'TEMPO');
INSERT INTO public.sentences VALUES (43, 'IL TEMPO CHE TI PIACE SPRECARE NON È SPRECATO', 'TEMPO');
INSERT INTO public.sentences VALUES (44, 'VIVERE SENZA UN SOGNO È SOLO UNO SPRECO DI TEMPO', 'TEMPO');
INSERT INTO public.sentences VALUES (45, 'LA FELICITÀ È IL MAGGIOR LIVELLO DI SUCCESSO', 'MOTIVAZIONALI');
INSERT INTO public.sentences VALUES (46, 'PONITI OBIETTIVI SPAVENTOSI ED ECCITANTI ALLO STESSO TEMPO', 'MOTIVAZIONALI');
INSERT INTO public.sentences VALUES (47, 'UNA VITA SENZA RICERCA NON È DEGNA DI ESSERE VISSUTA', 'MOTIVAZIONALI');
INSERT INTO public.sentences VALUES (48, 'VINCERE NON È TUTTO, LO È IL PROVARE A VINCERE', 'MOTIVAZIONALI');
INSERT INTO public.sentences VALUES (49, 'SE NON TI PERDI, NON TROVI STRADE NUOVE', 'MOTIVAZIONALI');
INSERT INTO public.sentences VALUES (50, 'NESSUNO PUÒ FARTI SENTIRE INFELICE SE TU NON GLIELO CONSENTI', 'CAMBIAMENTO');


--
-- Data for Name: manches; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.manches VALUES (1, 1, 12, 3, 2);
INSERT INTO public.manches VALUES (2, 1, 2, 2, 2);
INSERT INTO public.manches VALUES (3, 1, 5, 3, 2);
INSERT INTO public.manches VALUES (4, 1, 11, 3, 2);
INSERT INTO public.manches VALUES (5, 1, 23, 2, 2);
INSERT INTO public.manches VALUES (6, 2, 13, 3, 2);
INSERT INTO public.manches VALUES (7, 3, 26, 1, 2);
INSERT INTO public.manches VALUES (8, 3, 21, NULL, 3);
INSERT INTO public.manches VALUES (9, 4, 21, 2, 2);
INSERT INTO public.manches VALUES (10, 4, 22, 3, 2);
INSERT INTO public.manches VALUES (11, 4, 17, 2, 2);
INSERT INTO public.manches VALUES (12, 4, 19, 1, 2);
INSERT INTO public.manches VALUES (13, 4, 10, 3, 2);
INSERT INTO public.manches VALUES (14, 5, 27, 5, 2);
INSERT INTO public.manches VALUES (15, 5, 16, NULL, 3);


--
-- Data for Name: has_observed; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.has_observed VALUES (1, 6);
INSERT INTO public.has_observed VALUES (2, 7);
INSERT INTO public.has_observed VALUES (1, 14);


--
-- Data for Name: has_played; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.has_played VALUES (1, 1);
INSERT INTO public.has_played VALUES (2, 1);
INSERT INTO public.has_played VALUES (3, 1);
INSERT INTO public.has_played VALUES (1, 2);
INSERT INTO public.has_played VALUES (2, 2);
INSERT INTO public.has_played VALUES (3, 2);
INSERT INTO public.has_played VALUES (1, 3);
INSERT INTO public.has_played VALUES (2, 3);
INSERT INTO public.has_played VALUES (3, 3);
INSERT INTO public.has_played VALUES (1, 4);
INSERT INTO public.has_played VALUES (2, 4);
INSERT INTO public.has_played VALUES (3, 4);
INSERT INTO public.has_played VALUES (1, 5);
INSERT INTO public.has_played VALUES (2, 5);
INSERT INTO public.has_played VALUES (3, 5);
INSERT INTO public.has_played VALUES (2, 6);
INSERT INTO public.has_played VALUES (4, 6);
INSERT INTO public.has_played VALUES (3, 6);
INSERT INTO public.has_played VALUES (3, 7);
INSERT INTO public.has_played VALUES (1, 7);
INSERT INTO public.has_played VALUES (5, 7);
INSERT INTO public.has_played VALUES (3, 9);
INSERT INTO public.has_played VALUES (2, 9);
INSERT INTO public.has_played VALUES (1, 9);
INSERT INTO public.has_played VALUES (3, 10);
INSERT INTO public.has_played VALUES (2, 10);
INSERT INTO public.has_played VALUES (1, 10);
INSERT INTO public.has_played VALUES (3, 11);
INSERT INTO public.has_played VALUES (2, 11);
INSERT INTO public.has_played VALUES (1, 11);
INSERT INTO public.has_played VALUES (3, 12);
INSERT INTO public.has_played VALUES (2, 12);
INSERT INTO public.has_played VALUES (1, 12);
INSERT INTO public.has_played VALUES (3, 13);
INSERT INTO public.has_played VALUES (2, 13);
INSERT INTO public.has_played VALUES (1, 13);
INSERT INTO public.has_played VALUES (3, 14);
INSERT INTO public.has_played VALUES (2, 14);
INSERT INTO public.has_played VALUES (5, 14);


--
-- Data for Name: has_seen; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.has_seen VALUES (12, 1);
INSERT INTO public.has_seen VALUES (12, 2);
INSERT INTO public.has_seen VALUES (12, 3);
INSERT INTO public.has_seen VALUES (2, 1);
INSERT INTO public.has_seen VALUES (2, 2);
INSERT INTO public.has_seen VALUES (2, 3);
INSERT INTO public.has_seen VALUES (5, 1);
INSERT INTO public.has_seen VALUES (5, 2);
INSERT INTO public.has_seen VALUES (5, 3);
INSERT INTO public.has_seen VALUES (11, 1);
INSERT INTO public.has_seen VALUES (11, 2);
INSERT INTO public.has_seen VALUES (11, 3);
INSERT INTO public.has_seen VALUES (23, 1);
INSERT INTO public.has_seen VALUES (23, 2);
INSERT INTO public.has_seen VALUES (23, 3);
INSERT INTO public.has_seen VALUES (13, 1);
INSERT INTO public.has_seen VALUES (13, 2);
INSERT INTO public.has_seen VALUES (13, 4);
INSERT INTO public.has_seen VALUES (13, 3);
INSERT INTO public.has_seen VALUES (26, 2);
INSERT INTO public.has_seen VALUES (26, 3);
INSERT INTO public.has_seen VALUES (26, 1);
INSERT INTO public.has_seen VALUES (26, 5);
INSERT INTO public.has_seen VALUES (21, 3);
INSERT INTO public.has_seen VALUES (21, 2);
INSERT INTO public.has_seen VALUES (21, 1);
INSERT INTO public.has_seen VALUES (22, 3);
INSERT INTO public.has_seen VALUES (22, 2);
INSERT INTO public.has_seen VALUES (22, 1);
INSERT INTO public.has_seen VALUES (17, 3);
INSERT INTO public.has_seen VALUES (17, 2);
INSERT INTO public.has_seen VALUES (17, 1);
INSERT INTO public.has_seen VALUES (19, 3);
INSERT INTO public.has_seen VALUES (19, 2);
INSERT INTO public.has_seen VALUES (19, 1);
INSERT INTO public.has_seen VALUES (10, 3);
INSERT INTO public.has_seen VALUES (10, 2);
INSERT INTO public.has_seen VALUES (10, 1);
INSERT INTO public.has_seen VALUES (27, 1);
INSERT INTO public.has_seen VALUES (27, 3);
INSERT INTO public.has_seen VALUES (27, 2);
INSERT INTO public.has_seen VALUES (27, 5);


--
-- Data for Name: moves; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.moves VALUES (1, 0, '500', 1, 1, 3, false);
INSERT INTO public.moves VALUES (2, 500, 'F', 1, 2, 3, false);
INSERT INTO public.moves VALUES (3, 0, 'Punti insufficienti', 1, 3, 3, true);
INSERT INTO public.moves VALUES (4, 0, '300', 1, 1, 1, false);
INSERT INTO public.moves VALUES (5, 600, 'R', 1, 2, 1, false);
INSERT INTO public.moves VALUES (6, 0, '', 1, 4, 1, true);
INSERT INTO public.moves VALUES (7, 0, '500', 1, 1, 2, false);
INSERT INTO public.moves VALUES (8, 0, 'Nessuna scelta', 1, 2, 2, true);
INSERT INTO public.moves VALUES (9, 0, '300', 1, 1, 3, false);
INSERT INTO public.moves VALUES (10, 900, 'S', 1, 2, 3, false);
INSERT INTO public.moves VALUES (11, 0, 'FRASE MISTERIOSA8', 1, 4, 3, false);
INSERT INTO public.moves VALUES (12, 0, '400', 2, 1, 1, false);
INSERT INTO public.moves VALUES (13, 400, 'S', 2, 2, 1, false);
INSERT INTO public.moves VALUES (14, 0, 'Punti insufficienti', 2, 3, 1, true);
INSERT INTO public.moves VALUES (15, 0, '600', 2, 1, 2, false);
INSERT INTO public.moves VALUES (16, 1200, 'R', 2, 2, 2, false);
INSERT INTO public.moves VALUES (17, 0, 'SOPRA LA PANCA LA CAPRA CAMPA', 2, 4, 2, false);
INSERT INTO public.moves VALUES (18, 0, '400', 3, 1, 3, false);
INSERT INTO public.moves VALUES (19, 400, 'F', 3, 2, 3, false);
INSERT INTO public.moves VALUES (20, 0, '1000', 3, 1, 1, false);
INSERT INTO public.moves VALUES (21, 0, 'Nessuna scelta', 3, 2, 1, true);
INSERT INTO public.moves VALUES (22, 0, '500', 3, 1, 2, false);
INSERT INTO public.moves VALUES (23, 0, 'Nessuna scelta', 3, 2, 2, true);
INSERT INTO public.moves VALUES (24, 0, '400', 3, 1, 3, false);
INSERT INTO public.moves VALUES (25, 0, 'Nessuna scelta', 3, 2, 3, true);
INSERT INTO public.moves VALUES (26, 0, '500', 3, 1, 1, false);
INSERT INTO public.moves VALUES (27, 0, 'Nessuna scelta', 3, 2, 1, true);
INSERT INTO public.moves VALUES (28, 0, '400', 3, 1, 2, false);
INSERT INTO public.moves VALUES (29, 0, 'E', 3, 2, 2, true);
INSERT INTO public.moves VALUES (30, 0, '400', 3, 1, 3, false);
INSERT INTO public.moves VALUES (31, 1200, 'S', 3, 2, 3, false);
INSERT INTO public.moves VALUES (32, 0, 'FRASE MISTERIOSA1', 3, 4, 3, false);
INSERT INTO public.moves VALUES (33, 0, '400', 4, 1, 1, false);
INSERT INTO public.moves VALUES (34, 0, 'Nessuna scelta', 4, 2, 1, true);
INSERT INTO public.moves VALUES (35, 0, 'LOSE', 4, 1, 2, false);
INSERT INTO public.moves VALUES (36, 0, '600', 4, 1, 3, false);
INSERT INTO public.moves VALUES (37, 600, 'F', 4, 2, 3, false);
INSERT INTO public.moves VALUES (38, 0, 'FRASE MISTERIOSA7', 4, 4, 3, false);
INSERT INTO public.moves VALUES (39, 0, '400', 5, 1, 1, false);
INSERT INTO public.moves VALUES (40, 400, 'F', 5, 2, 1, false);
INSERT INTO public.moves VALUES (41, 0, 'Punti insufficienti', 5, 3, 1, true);
INSERT INTO public.moves VALUES (42, 0, '500', 5, 1, 2, false);
INSERT INTO public.moves VALUES (43, 1000, 'R', 5, 2, 2, false);
INSERT INTO public.moves VALUES (44, 0, 'FRASE MISTERIOSA19', 5, 4, 2, false);
INSERT INTO public.moves VALUES (45, 0, '600', 7, 1, 3, false);
INSERT INTO public.moves VALUES (46, 0, 'Nessuna scelta', 7, 2, 3, true);
INSERT INTO public.moves VALUES (47, 0, '300', 7, 1, 1, false);
INSERT INTO public.moves VALUES (48, 300, 'F', 7, 2, 1, false);
INSERT INTO public.moves VALUES (49, 0, 'FRASE MISTERIOSA22', 7, 4, 1, false);
INSERT INTO public.moves VALUES (50, 0, 'LOSE', 9, 1, 3, false);
INSERT INTO public.moves VALUES (51, 0, '600', 9, 1, 2, false);
INSERT INTO public.moves VALUES (52, 600, 'F', 9, 2, 2, false);
INSERT INTO public.moves VALUES (53, 0, '', 9, 4, 2, true);
INSERT INTO public.moves VALUES (54, 0, '600', 9, 1, 1, false);
INSERT INTO public.moves VALUES (55, 0, 'Nessuna scelta', 9, 2, 1, true);
INSERT INTO public.moves VALUES (56, 0, '700', 9, 1, 3, false);
INSERT INTO public.moves VALUES (57, 0, 'Nessuna scelta', 9, 2, 3, true);
INSERT INTO public.moves VALUES (58, 0, '500', 9, 1, 2, false);
INSERT INTO public.moves VALUES (59, 1000, 'R', 9, 2, 2, false);
INSERT INTO public.moves VALUES (60, 0, 'FRASE MISTERIOSAQ', 9, 4, 2, false);
INSERT INTO public.moves VALUES (61, 0, '500', 10, 1, 1, false);
INSERT INTO public.moves VALUES (62, 500, 'F', 10, 2, 1, false);
INSERT INTO public.moves VALUES (63, 0, 'Mossa nulla', 10, NULL, 1, true);
INSERT INTO public.moves VALUES (64, 0, '400', 10, 1, 3, false);
INSERT INTO public.moves VALUES (65, 1200, 'R', 10, 2, 3, false);
INSERT INTO public.moves VALUES (66, 0, 'FRASE MISTERIOSAR', 10, 4, 3, false);
INSERT INTO public.moves VALUES (67, 0, '400', 11, 1, 2, false);
INSERT INTO public.moves VALUES (68, 400, 'F', 11, 2, 2, false);
INSERT INTO public.moves VALUES (69, 0, 'FRASE MISTERIOSAM', 11, 4, 2, false);
INSERT INTO public.moves VALUES (70, 0, '300', 12, 1, 1, false);
INSERT INTO public.moves VALUES (71, 300, 'F', 12, 2, 1, false);
INSERT INTO public.moves VALUES (72, 0, 'FRASE MISTERIOSAO', 12, 4, 1, false);
INSERT INTO public.moves VALUES (73, 0, '300', 13, 1, 3, false);
INSERT INTO public.moves VALUES (74, 600, 'R', 13, 2, 3, false);
INSERT INTO public.moves VALUES (75, 0, 'FRASE MISTERIOSAF', 13, 4, 3, false);
INSERT INTO public.moves VALUES (76, 0, '300', 14, 1, 3, false);
INSERT INTO public.moves VALUES (77, 0, 'Nessuna scelta', 14, 2, 3, true);
INSERT INTO public.moves VALUES (78, 0, '700', 14, 1, 2, false);
INSERT INTO public.moves VALUES (79, 2100, 'S', 14, 2, 2, false);
INSERT INTO public.moves VALUES (80, -1000, 'E', 14, 3, 2, false);
INSERT INTO public.moves VALUES (81, -1000, 'R', 14, 3, 2, true);
INSERT INTO public.moves VALUES (82, 0, '700', 14, 1, 5, false);
INSERT INTO public.moves VALUES (83, 1400, 'R', 14, 2, 5, false);
INSERT INTO public.moves VALUES (84, -1000, 'U', 14, 3, 5, true);
INSERT INTO public.moves VALUES (85, 0, '400', 14, 1, 3, false);
INSERT INTO public.moves VALUES (86, 400, 'F', 14, 2, 3, false);
INSERT INTO public.moves VALUES (87, 0, 'S<FSDFSDF', 14, 4, 3, true);
INSERT INTO public.moves VALUES (88, -100, 'LOSE', 14, 1, 2, false);
INSERT INTO public.moves VALUES (89, 0, 'PASS', 14, 1, 5, false);
INSERT INTO public.moves VALUES (90, -400, 'LOSE', 14, 1, 3, false);
INSERT INTO public.moves VALUES (91, 0, '400', 14, 1, 2, false);
INSERT INTO public.moves VALUES (92, 400, 'T', 14, 2, 2, false);
INSERT INTO public.moves VALUES (93, 0, 'Punti insufficienti', 14, 3, 2, true);
INSERT INTO public.moves VALUES (94, 0, '700', 14, 1, 5, false);
INSERT INTO public.moves VALUES (95, 700, 'M', 14, 2, 5, false);
INSERT INTO public.moves VALUES (96, 0, 'FRASE MISTERIOSAW', 14, 4, 5, false);


--
-- Data for Name: scores; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.scores VALUES (1, 600, 1, NULL, 1);
INSERT INTO public.scores VALUES (2, 0, 2, NULL, 1);
INSERT INTO public.scores VALUES (3, 1400, 3, NULL, 1);
INSERT INTO public.scores VALUES (4, 400, 1, NULL, 2);
INSERT INTO public.scores VALUES (5, 1200, 2, NULL, 2);
INSERT INTO public.scores VALUES (6, 0, 3, NULL, 2);
INSERT INTO public.scores VALUES (7, 0, 1, NULL, 3);
INSERT INTO public.scores VALUES (8, 0, 2, NULL, 3);
INSERT INTO public.scores VALUES (9, 1600, 3, NULL, 3);
INSERT INTO public.scores VALUES (10, 0, 1, NULL, 4);
INSERT INTO public.scores VALUES (11, 0, 2, NULL, 4);
INSERT INTO public.scores VALUES (12, 600, 3, NULL, 4);
INSERT INTO public.scores VALUES (13, 400, 1, NULL, 5);
INSERT INTO public.scores VALUES (14, 1000, 2, NULL, 5);
INSERT INTO public.scores VALUES (15, 0, 3, NULL, 5);
INSERT INTO public.scores VALUES (16, 0, 1, 1, NULL);
INSERT INTO public.scores VALUES (17, 2200, 2, 1, NULL);
INSERT INTO public.scores VALUES (18, 3600, 3, 1, NULL);
INSERT INTO public.scores VALUES (19, 0, 3, NULL, 7);
INSERT INTO public.scores VALUES (20, 300, 1, NULL, 7);
INSERT INTO public.scores VALUES (21, 0, 5, NULL, 7);
INSERT INTO public.scores VALUES (22, 0, 3, NULL, 9);
INSERT INTO public.scores VALUES (23, 1600, 2, NULL, 9);
INSERT INTO public.scores VALUES (24, 0, 1, NULL, 9);
INSERT INTO public.scores VALUES (25, 1200, 3, NULL, 10);
INSERT INTO public.scores VALUES (26, 0, 2, NULL, 10);
INSERT INTO public.scores VALUES (27, 500, 1, NULL, 10);
INSERT INTO public.scores VALUES (28, 0, 3, NULL, 11);
INSERT INTO public.scores VALUES (29, 400, 2, NULL, 11);
INSERT INTO public.scores VALUES (30, 0, 1, NULL, 11);
INSERT INTO public.scores VALUES (31, 0, 3, NULL, 12);
INSERT INTO public.scores VALUES (32, 0, 2, NULL, 12);
INSERT INTO public.scores VALUES (33, 300, 1, NULL, 12);
INSERT INTO public.scores VALUES (34, 600, 3, NULL, 13);
INSERT INTO public.scores VALUES (35, 0, 2, NULL, 13);
INSERT INTO public.scores VALUES (36, 0, 1, NULL, 13);
INSERT INTO public.scores VALUES (37, 1800, 3, 4, NULL);
INSERT INTO public.scores VALUES (38, 2000, 2, 4, NULL);
INSERT INTO public.scores VALUES (39, 300, 1, 4, NULL);
INSERT INTO public.scores VALUES (40, 0, 3, NULL, 14);
INSERT INTO public.scores VALUES (41, 400, 2, NULL, 14);
INSERT INTO public.scores VALUES (42, 1100, 5, NULL, 14);

--
-- Name: manches_manche_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.manches_manche_id_seq', 15, true);


--
-- Name: matches_match_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.matches_match_id_seq', 5, true);


--
-- Name: move_types_move_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.move_types_move_type_id_seq', 5, true);


--
-- Name: moves_move_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.moves_move_id_seq', 96, true);


--
-- Name: scores_score_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.scores_score_id_seq', 42, true);


--
-- Name: sentences_sentence_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentences_sentence_id_seq', 50, true);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_id_seq', 6, true);


--
-- PostgreSQL database dump complete
