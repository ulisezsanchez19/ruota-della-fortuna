package rdf.DAO;

import rdf.API.Manche;
import rdf.API.Match;
import rdf.API.Sentence;
import rdf.API.User;
import rdf.DAO.interfaces.MancheDAO;

/**
 * Oggetto DAO che si occupa delle query riguardanti le manche.
 * Fa utilizzo della classe GenericDAO.
 *
 */
class MancheDAOImpl extends GenericDAOImpl implements MancheDAO {

	@Override
	public int insertManche(Match match, Sentence sentence) {
		
		String[] names = {"manches", "id_match", "id_sentence", "id_game_status"};
		Object[] objects = {match.match_id, sentence.sentence_id, 1};
		
		return insert(names, objects, "manche_id");
	}

	@Override
	public boolean addWinner(Manche manche, User user) {

		String[] names = {"manches", "id_winner", "id_game_status", "manche_id"};
		Object[] objects = {user.getId(), 2, manche.manche_id};
		
		return update(names, objects);
	}
	
	@Override
	public boolean resetMancheStatus() {
		
		String[] names = {"manches", "id_game_status", "id_game_status"};
		Object[] objects = {3, 1};
		
		return update(names, objects);
	}

}
