package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interfaccia dell'oggetto remoto del client usato dal server
 *
 */
public interface ClientRdFInterface extends Remote{
	
	/**
	 * forza la disconnesione e il log-out del client
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void forceDisconnect() throws RemoteException;

}
