package rdf.controllers;

import java.io.IOException;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import rdf.API.*;
import rdf.InstanceData;
import rdf.LobbyListener;

/**
 * controller della finestra lobbyList
 *
 */
public class LobbyListViewController {

	@FXML
	MenuButton mbtnUsername;
	@FXML
	TableView<LobbyData> tvListaLobby;
	@FXML
	TableColumn<LobbyData, Integer> tcNumber;
	@FXML
	TableColumn<LobbyData, String> tcName;
	@FXML
	TableColumn<LobbyData, Integer> tcPlayers;
	@FXML
	TableColumn<LobbyData, String> tcStatus;
	@FXML
	Button btCreateLobby;
	@FXML
	Button btJoinAsPlayer;
	@FXML
	Button btJoinAsObserver;

	ObservableList<LobbyData> lobbyList;

	/**
	 * metodo che inizializza la finestra
	 */
	@FXML
	public void initialize() {

		if (InstanceData.isAdmin) {
			btCreateLobby.setDisable(true);
			btJoinAsPlayer.setDisable(true);
		}

		InstanceData.lobbyListener = new LobbyListener(this);
		lobbyList = FXCollections.observableArrayList();
		lobbyList = FXCollections.observableArrayList(InstanceData.lobbyListener.getLobbiesList());

		mbtnUsername.setText(InstanceData.user.getUsername());

		tcNumber.setCellValueFactory(new PropertyValueFactory<LobbyData, Integer>("Number"));
		tcName.setCellValueFactory(new PropertyValueFactory<LobbyData, String>("Name"));
		tcPlayers.setCellValueFactory(new PropertyValueFactory<LobbyData, Integer>("PlayersNumber"));
		tcStatus.setCellValueFactory(new PropertyValueFactory<LobbyData, String>("Status"));

		tvListaLobby.setItems(lobbyList);
	}

	@FXML
	void goToMain(ActionEvent event) throws IOException {
		InstanceData.gotoHome();
	}

	/**
	 * metodo per avviare la procedura di crezione lobby
	 */
	@FXML
	void createLobby(ActionEvent event) {

		TextInputDialog dialog = new TextInputDialog("");
		dialog.setTitle("Create Lobby");
		dialog.setContentText("Lobby name:");
		dialog.setHeaderText(null);

		String lobbyName = "";
		Optional<String> inputName = dialog.showAndWait();
		if (!inputName.isPresent() || (lobbyName = inputName.get()).equals(""))
			return;

		InstanceData.lobbyListener.createLobby(lobbyName);
	}

	/**
	 * metodo per aggiornare la lista delle lobby
	 * @param command comando da effettuare
	 * @param newData la lista sul quale effettuare il comando
	 */
	public void UpdateLobbyData(Integer command, LobbyData newData) {

		if (command == 1) {
			lobbyList.add(newData);
		} else {
			int index = -1;
			for (int i = 0; i < lobbyList.size(); i++) {
				if (lobbyList.get(i).getNumber() == newData.getNumber()) {
					index = i;
					break;
				}
			}

			if (command == 0)
				lobbyList.set(index, newData);
			else
				lobbyList.remove(index);
		}
	}

	/**
	 * metodo per abilitare o disattivare i bottoni
	 * @param value nuovo valore dei bottoni
	 */
	public void buttonsSwitch(boolean value) {
		btCreateLobby.setDisable(!(value && !InstanceData.isAdmin));
		btJoinAsPlayer.setDisable(!(value && !InstanceData.isAdmin));
		btJoinAsObserver.setDisable(!value);
	}

	/**
	 * metodo per inizializzare il join in una lobby come giocatore
	 * @throws IOException lanciata in caso di errori di comunicazione col server
	 */
	@FXML
	void joinLobbyAsPlayer(ActionEvent event) throws IOException {

		LobbyData selected = tvListaLobby.getSelectionModel().getSelectedItem();
		if (selected == null)
			return;
		if (selected.getStatus() == Status.CLOSED) {
			return;
		}
		
		InstanceData.lobbyListener.joinLobbyAsPlayer(selected);
	}

	/**
	 * metodo per inizializzare il join in una lobby come osservatore
	 * @throws IOException lanciata in caso di errori di comunicazione col server
	 */
	@FXML
	void joinLobbyAsObserver(ActionEvent event) throws IOException {

		LobbyData selected = tvListaLobby.getSelectionModel().getSelectedItem();
		if (selected == null)
			return;

		InstanceData.lobbyListener.joinLobbyAsObserver(selected);
	}

	/**
     * metodo usato per cambiare scena
     */
	@FXML
	public void gotoScene(ActionEvent event) {
		
		String id = "";
		if(event.getSource() instanceof Control)
			id = ((Control) event.getSource()).getId();
		else if(event.getSource() instanceof MenuItem)
			id = ((MenuItem) event.getSource()).getId();		
    	
    	switch(id)
    	{
    	case "btnHome":
    		InstanceData.gotoHome();
    		break;
    	case "btnMenuPanel":
    		InstanceData.gotoRankings();
    		break;
    	case "btnProfile":
        	InstanceData.gotoProfile();
    		break;
    	case "btnAuthentication":
    		InstanceData.returnToAuthentication(true);
    		break;
    	}

	}

}
