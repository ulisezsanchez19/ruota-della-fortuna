package rdf.controllers;

import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import rdf.ClientRdF;
import rdf.InstanceData;
import rdf.API.StringUtilities;
import rdf.API.User;
import rdf.API.UserDetails;
import rdf.API.interfaces.AccountManagerInterface;
import rdf.API.interfaces.ClientRdFInterface;

/**
 * controller della finestra di autenticazione
 *
 */
public class AuthenticationViewController {

	// Node parent
	@FXML
	private StackPane parentRoot;

	@FXML
	private VBox loginPanel;

	@FXML
	private VBox signUpPanel;

	// Login widgets
	@FXML
	private Text txtLinkToSignUp;

	@FXML
	private TextField tfEmail;

	@FXML
	private PasswordField pfPassword;

	@FXML
	private Button btnLoginAction;
	
    @FXML
    private Label lblLoginLog;

	// SingUp panel widgets

	@FXML
	private Text txtLinkToLogin;

	@FXML
	private TextField tfSignUpName;

	@FXML
	private TextField tfSignUpSurname;

	@FXML
	private TextField tfSignUpUsername;

	@FXML
	private TextField tfSignUpEmail;

	@FXML
	private PasswordField tfSignUpPassword;

	@FXML
	private PasswordField tfSignUpConfirmPassword;

	@FXML
	private Button btnSingUpAction;

	@FXML
	private Label lblSignUpLog;

	@FXML
	private ProgressIndicator piElaborationg;

	private UserDetails userDetails;

	private AccountManagerInterface accountManager;

	/**
	 * metodo di inializzazione che ottiene l'account manager dal server
	 */
	public void initialize() {

		try {
			accountManager = InstanceData.stub.getAccountManager();
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}

		paneSwitch(true);
	}

	public void loadLoginPane(MouseEvent event) {
		paneSwitch(true);
	}

	public void loadSignupView(MouseEvent event) {
		paneSwitch(false);
	}

	public void gotoPasswordRecovery(MouseEvent event) {
		openDialog(false);
	}

	/**
	 * metodo per cambiare il pane correntemente visualizzato
	 * @param login boleana per rendere visibile o meno il panel login
	 */
	private void paneSwitch(boolean login) {
		loginPanel.setDisable(!login);
		loginPanel.setVisible(login);
		signUpPanel.setDisable(login);
		signUpPanel.setVisible(!login);
	}

	/**
	 * metodo per eseguire il login
	 */
	public void doLogin(ActionEvent event) {

		lblLoginLog.setText("");
		String md5 = StringUtilities.getMD5(pfPassword.getText());
		if (md5 == null) {
			System.err.println("ERROR: md5 failed!");
			return;
		}

		String email = tfEmail.getText();
		if (accountManager == null) {
			System.out.println("accountManager NULL");
			return;
		}
		ClientRdFInterface stub = null;
		try {
			stub = (ClientRdFInterface) UnicastRemoteObject.exportObject((ClientRdF) InstanceData.ThisclientRdF, 0);
		} catch (RemoteException e1) {
			InstanceData.interruptClient();
		}
		User user = null;
		try {
			user = accountManager.login(email, md5, InstanceData.isAdmin, stub);
		} catch (RemoteException e1) {
			InstanceData.interruptClient();
		}

		if (user == null) {
			lblLoginLog.setText("Login error");
			try {
				UnicastRemoteObject.unexportObject(InstanceData.ThisclientRdF, true);
			} catch (NoSuchObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}

		InstanceData.user = user;
		InstanceData.gotoHome();
	}

	/**
	 * metodo per eseguire la registrazione
	 */
	public void doSignUp(ActionEvent event) {

		lblSignUpLog.setText("");
		if (checkData()) {
			
			piElaborationg.setVisible(true);
			btnSingUpAction.setDisable(true);

			new Thread(new Runnable() {
				private int result;

				public void run() {
					result = 0;

					userDetails = new UserDetails(0, tfSignUpUsername.getText(), tfSignUpEmail.getText(),
							tfSignUpName.getText(), tfSignUpSurname.getText());
					try {
						result = accountManager.register(userDetails,
								StringUtilities.getMD5(tfSignUpPassword.getText()), InstanceData.isAdmin);
					} catch (Exception e) {
						e.printStackTrace();
					}
					Platform.runLater(() -> {
						piElaborationg.setVisible(false);
						btnSingUpAction.setDisable(false);
					
						switch (result) {
						case 1:
							loadLoginPane(null);
							openDialog(true);
							return;
						case 0:
							lblSignUpLog.setText("ERROR!");
							break;
						case -1:
							lblSignUpLog.setText("Email already used!");
							break;
						case -2: 
							lblSignUpLog.setText("Username already used!");
							break;
						case -3: 
							lblSignUpLog.setText("Confirmation code already sent!");
							break;
						}
					});
				}
			}).start();

		}
	}

	/**
	 * metodo per controllare i cambi
	 * @return booleana del risultato della verifica
	 */
	private boolean checkData() {
		if (!checkEmptySignUpFields()) {
			lblSignUpLog.setText(" Complete all fields !");
			return false;
		}

		if (!StringUtilities.checkEmail(tfSignUpEmail.getText())) {
			lblSignUpLog.setText("Invalid email!");
			return false;
		}

		if (!StringUtilities.checkPassword(tfSignUpPassword.getText())) {
			lblSignUpLog.setText("Password too weak!");
			return false;
		}

		if (!tfSignUpPassword.getText().equals(tfSignUpConfirmPassword.getText())) {
			lblSignUpLog.setText("The passwords do not match!");
			return false;
		}

		return true;
	}

	/**
	 * metodo per creare la finestra pop-up
	 * @param dialogType il tipo di pop-up
	 */
	public void openDialog(boolean dialogType) {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/rdf.view/DialogAccountConfirmationView.fxml"));
			Stage dialog = new Stage();
			dialog.setTitle(dialogType ? "Confirm email" : "Recovery password");
			Scene scene = new Scene(loader.load());
			dialog.setScene(scene);
			AuthenticationManagerDialogsController controller = loader.getController();
			controller.initialize(dialogType, this, scene);
			// AuthenticationViewController controller= loader.getController();
			// controller.UserDetails(user,dialog);
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.initStyle(StageStyle.UNDECORATED);
			dialog.showAndWait();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public UserDetails getUserDeails() {
		return userDetails;
	}

	/**
	 * metodo per eseguire il controllo sul cambi vuoti
	 * @return booleana del risultato della verifica
	 */
	private boolean checkEmptySignUpFields() {
		if (tfSignUpName.getText() == null || tfSignUpName.getText().isEmpty())
			return false;
		if (tfSignUpSurname.getText() == null || tfSignUpSurname.getText().isEmpty())
			return false;
		if (tfSignUpUsername.getText() == null || tfSignUpUsername.getText().isEmpty())
			return false;
		if (tfSignUpEmail.getText() == null || tfSignUpEmail.getText().isEmpty())
			return false;
		if (tfSignUpPassword.getText() == null || tfSignUpPassword.getText().isEmpty())
			return false;
		if (tfSignUpConfirmPassword.getText() == null || tfSignUpConfirmPassword.getText().isEmpty())
			return false;

		return true;
	}
}
