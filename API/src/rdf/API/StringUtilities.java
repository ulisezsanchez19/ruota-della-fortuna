package rdf.API;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Classe contenente metodi di servizio
 *
 */
public class StringUtilities {

	public static boolean checkPassword(String password) {
		/*
		^                 # start-of-string
		(?=.*[0-9])       # a digit must occur at least once
		(?=.*[a-z])       # a lower case letter must occur at least once
		(?=.*[A-Z])       # an upper case letter must occur at least once
		(?=.*[@#$%^&+=])  # a special character must occur at least once
		(?=\S+$)          # no whitespace allowed in the entire string
		.{8,}             # anything, at least eight places though
		$                 # end-of-string
	 * */
		String regex = "^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[^a-zA-Z0-9\\\\s]).{6,}";
		return password.matches(regex);
	}
	
	public static boolean checkEmail(String email) {
		String regex = "[a-z0-9\\._%+!$&*=^|~#%'`?{}/\\-]+@([a-z0-9\\-]+\\.){1,}([a-z]{2,16})";
		return email.matches(regex);
	}
	
	public static String getMD5(String plainText) {
		
		String md5 = "";
        try { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(plainText.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            md5 = no.toString(16); 
            while (md5.length() < 32) { 
            	md5 = "0" + md5; 
            } 
            return md5.toUpperCase(); 
        }  
        catch (NoSuchAlgorithmException e) { 
        	// TODO Auto-generated catch block
        	e.printStackTrace(); 
        }
        
        return md5;
	}
	
	/**
	 * Funzione usata per suddividere la frase misteriosa nel formato richiesto dal tabellone
	 * @param sentence frase misteriosa
	 * @return la stringa formattata
	 */
	public static String splitSentenceString(String sentence)
	{	
		int temp = 0, row = 0, start = 0, end = 0;
		String sub = "";
		String result = "";
		boolean last = false;
		
		while(end < sentence.length()) //� possibile migliorare la condizione di uscita con:"&& riga < RIGHE" ma ho preferito fare un if con un break 
		{	
			if(row >= 4) 
			{
				System.out.println("ERROR! Formatting error: the sentences is too long.\n" + sentence);
				return null;
			}
			
			if(sentence.charAt(start) == ' ')
				start++;
			
			if((sentence.length() - start) > 15)
				end = start + 15;
			else
			{
				end = sentence.length();
				last = true;
			}
			
			sub = sentence.substring(start, end);		
			
			if(!last && !(sub.endsWith(" ") || sentence.charAt(end) == ' ')) // La condizione � composta dall'AND tra la negazione della variabile booleana "ultimo" e la condizione vera e propria. In questo modo la la variabile "ultimo" sar� ininfluente ogni volta che sar� falsa, mentre quando si trover� in uno stato di true (1. quindi false, perch� !true = false. 2. quindi l'ultima scomposizione della frase da esaminare) produrra sempre un risultato false, questo fatto ci evita che nell'ultima iterazioni venga generata un ecceeezione dal controllo: "frase.charAt(fine) == ' '" 
			{
				temp = -1;
				for(String s : new String[] {" ", ",", "'", ".", ":", ";", "�"})					
					temp = (sub.lastIndexOf(s) > temp) ? sub.lastIndexOf(s) : temp;
		
				if(temp++ != -1)
				{
					sub = sub.substring(0, temp);					
					start += temp;										
				}
				else 
					return null;
			}
			else
				start = end;
			row++;
			if(sub.endsWith(" "))
				sub = sub.substring(0, sub.length()-1);
			result += sub + "+";			
						
		}
		return result.substring(0, result.length()-1);
	}
	
	/**
	 * metodo che genera una stringa casuale di lunghezza N
	 * @return stringa casuale
	 */
	public static String getRandomString(int n) 
    {
		String Characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvxyz";
        String random = ""; 
  
        for (int i = 0; i < n; i++) {
            int randomCharIndex = (int)(Characters.length() * Math.random()); 
            random += Characters.charAt(randomCharIndex); 
        } 
  
        return random; 
    } 
}
