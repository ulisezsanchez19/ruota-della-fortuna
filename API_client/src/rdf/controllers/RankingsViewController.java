package rdf.controllers;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import rdf.InstanceData;

/**
 * controller della finestra dei rankings
 *
 */
public class RankingsViewController {

    @FXML
    private TabPane tpRankings;
    
    @FXML
    private Tab tbGlobal;

    @FXML
    private VBox vbGlobal;

    @FXML
    private Tab tbPersonal;

    @FXML
    private VBox vbPersonal;

	List<String[]> gameStatistics;
	TitledPane[] tpGlobal;
	List<String[]> gamesQueriesFormat;
	List<String[]> userQueriesFormat;

	/**
	 * metodo per inizializzare la finestra prendendo i valori dal server
	 */
	public void initialize() {

		try {
			gameStatistics = InstanceData.stub.getStatisticsManager().getAllGamesStatics();
			if (InstanceData.userStatisticsCache == null)
				InstanceData.userStatisticsCache = InstanceData.stub.getStatisticsManager()
						.getUserStatics(InstanceData.user);
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
		
		initializeQueriesFormat();

		tpGlobal = new TitledPane[gamesQueriesFormat.size()];
		for (int i = 0; i < gamesQueriesFormat.size(); i++) {
			tpGlobal[i] = new TitledPane();
			tpGlobal[i].setText(gamesQueriesFormat.get(i)[0]);
			loadGamesResult(i, 1);
		}		
		
		vbGlobal.getChildren().addAll(tpGlobal);
		
		TitledPane[] tps = new TitledPane[userQueriesFormat.size()];
		for (int i = 0; i < userQueriesFormat.size(); i++) {
			tps[i] = new TitledPane();
			tps[i].setText(userQueriesFormat.get(i)[0]);
			loadUserResult(i, tps[i]);
		}
		vbPersonal.getChildren().addAll(tps);
		
	}
	
	/**
	 * metodo che inizializza la formattazione delle query
	 */
	private void initializeQueriesFormat()
	{
		gamesQueriesFormat = new ArrayList<>();
		gamesQueriesFormat.add(new String[] { "Matches' top scores", "Username:", "Score:" });
		gamesQueriesFormat.add(new String[] { "Manches' top scores","Username:", "Score:" });
		gamesQueriesFormat.add(new String[] { "Users with most manches played", "Username:", "Manches played:" });
		gamesQueriesFormat.add(new String[] { "Users with highest average manches points", "Username:", "AVG Points:" });
		gamesQueriesFormat.add(new String[] { "Users with highest errors count", "Username:", "Errors count:" });
		gamesQueriesFormat.add(new String[] { "Users with highest \"Lose\" extracted from wheel count", "Username:", "Loses count:" });
		gamesQueriesFormat.add(new String[] { "Consonant calls with highest point earned", "Username:", "Consonant:", "Points:", "Sentence:" });
		gamesQueriesFormat.add(new String[] { "Average manches moves before guessing the sentence", "Moves count:" });
		
		userQueriesFormat = new ArrayList<>();
		userQueriesFormat.add(new String[] { "Manches played count", "Manches played:" });
		userQueriesFormat.add(new String[] { "Matches played count", "Matches played:" });
		userQueriesFormat.add(new String[] { "Manches observed count", "Manches observed:" });
		userQueriesFormat.add(new String[] { "Matches observed count", "Matches observed:" });
		userQueriesFormat.add(new String[] { "Manches won count", "Manches won:" });
		userQueriesFormat.add(new String[] { "Matches won count", "Matches won:" });
		userQueriesFormat.add(new String[] { "Average matches score", "AVG matchess score:" });
		userQueriesFormat.add(new String[] { "Manches' average \"Pass\" extracted from wheel", "AVG Pass:" });
		userQueriesFormat.add(new String[] { "Matches' average \"Pass\" extracted from wheel", "AVG Pass:" });
		userQueriesFormat.add(new String[] { "Manches' average \"Lose\" extracted from wheel", "AVG Lose:" });
		userQueriesFormat.add(new String[] { "Matches' average \"Lose\" extracted from wheel", "AVG Lose:" });
	}

	/**
	 * metodo che carica i valori delle query del gioco
	 * @param n l'indice della query
	 * @param rows quanti risultati si richiedono
	 */
	private void loadGamesResult(int n, int rows) {

		int totalResult = rows * (gamesQueriesFormat.get(n).length - 1);
		int limit = rows;
		if (rows > 1) {
			try {
				String[] result = InstanceData.stub.getStatisticsManager().loadMoreResult(n, rows);
				if ((result.length) < totalResult) {
					totalResult = result.length;
					limit = totalResult / (gamesQueriesFormat.get(n).length - 1);
				}
				gameStatistics.set(n, result);
			} catch (RemoteException e) {
				InstanceData.interruptClient();
			}
		}
		
		GridPane grid = new GridPane();
		int count = 0;
		for (int i = 0; i < limit; i++) {
			int col = 0;
			for(int s = 1; s < gamesQueriesFormat.get(n).length; s++)
			{
				Label lbForm = new Label(gamesQueriesFormat.get(n)[s]);
				lbForm.setPadding(new Insets(10, 10, 10, 10));
				lbForm.setStyle("-fx-text-fill:#F57F17;-fx-font-weight:bold;");
				grid.add(lbForm, col++, i);
				
				Label lbValue = new Label(gameStatistics.get(n)[count++]);
				lbValue.setPadding(new Insets(10, 10, 10, 10));
				lbValue.setStyle("-fx-text-fill:white;-fx-font-size:12px");
				grid.add(lbValue, col++, i);
			}
		}

		if (rows < 100 && limit == rows) {
			Button bt = new Button();
			bt.getStyleClass().setAll(getClass().getResource("/rdf.styles/rankings.css").toExternalForm());
			bt.getStyleClass().setAll("button-extend");
		    bt.setId(n + "");
			bt.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					loadGamesResult(n, rows == 1 ? 10 : (rows + 10));
				}
			});
			grid.add(bt, 0, limit);
		}
		tpGlobal[n].setContent(grid);
	}

	/**
	 * metodo che carica i valori delle query dell'utente
	 * @param n l'indice della query
	 * @param tps title pane da utilizzare
	 * @return
	 */
	private void loadUserResult(int n, TitledPane tps) {

		GridPane grid = new GridPane();
		grid.setVgap(4);
		grid.setPadding(new Insets(5, 5, 5, 5));

		Label lbForm = new Label(userQueriesFormat.get(n)[1]);
		lbForm.setPadding(new Insets(10, 10, 10, 10));
		lbForm.setStyle("-fx-text-fill:#F57F17;-fx-font-weight:bold;");
		grid.add(lbForm, 0, 0);
		
		Label lbValue = new Label(InstanceData.userStatisticsCache[n]);
		lbValue.setPadding(new Insets(10, 10, 10, 10));
		lbValue.setStyle("-fx-text-fill:white;-fx-font-size:12px");
		grid.add(lbValue, 1, 0);

		tps.setContent(grid);
	}

    /**
     * metodo per passare ai ranking globali
     */
    @FXML
    void switchToGlobal(ActionEvent event) {
    	tpRankings.getSelectionModel().select(0);
    }

    /**
     * metodo per passare ai ranking personali
     */
    @FXML
    void switchToPersonal(ActionEvent event) {
    	tpRankings.getSelectionModel().select(1);
    }
	
    /**
     * metodo per tornare alla home
     */
	@FXML
	void gotoHome(ActionEvent event) throws IOException {
		InstanceData.gotoHome();
	}
}