Università degli Studi dell'Insubria
Laboratorio interdisciplinare B a.a. 2019/2020 - La Ruota della Fortuna (Esame del 14/02/2020)

Battocchio Gianluca
Borsoi Luca
Genovese Mirko 
Sanchez Ulises

Utilizzata la JDK 1.8 di Java 8

Il progetto è suddiviso nei seguenti programmi: server, player e admin. Inoltre gli ultimi due programmi citati dipendono dalla 
libreria ./API_client/lib/API_client.jar, mentre tutti e 3 dipendono dalla libreria ./API/lib/API.jar.

Inoltre, Il programma server fa riferimento ad ulteriori  due librerie, in dettaglio:
	-libreria di postegresql 42.8;
	-javax mail.
	
Per fare il build del progetto è stato scelto Apache ANT.

ISTRUZIONI:

	1.	Per compilarlo tutto è necessario, una volta posizionati nella root del progetto, eseguire dal terminale il comando "ant" 
		(abbreviazione di "ant buildProject").
	2.	Ora, dallo stesso path, è necessario eseguire il comando per eseguire la creazione delle tabelle sul database. Per far ciò è necessario 
		passare come argomento l'host, l'username e la password inerenti al database postgresql al quale ci si vuole connettere. Il comando è: 			
			-"db_setup": Inizializza il database creando le tabelle;
		(Esempio formato: ant -DURL="dbURL" -Dusername="YourUsername" -Dpassword="YourPassword" db_setup)
		N.B. Nell'url del database è neccessario che ci sia anche il nome del database. Es. "jdbc:postgresql://localhost:5432/dbrdf"
	3.	Successivamente aver eseguito i precedenti passaggi è possibile avviare il programma utilizzando i seguenti comandi:
			-"run_server": 	avvia il server. Oltre al comando è neccessario, in maniera simile al punto precedente, passare anche
							come argomento i dati neccessari al collegamento al database;
							(Esempio formato:ant -DURL="dbURL" -Dusername="YourUsername" -Dpassword="YourPassword" run_server)
			-"run_player": avvia un player;
			-"run_admin": avvia un admin.
	4.	Dopo aver eseguito il passaggio numero (1) è possibile lanciare il comando "ant doc" per la generazione dei commenti
		(I file generati verrano memorizzati all'interno della directory "doc" presente in ogni cartella di tutti i programmi).
	5.	In qualsiasi momento è possibile eseguire dei comandi per pulire il progetto dalle cartelle/file generati.
			-"clean_build": cancella tutte le cartelle bin;
			-"clean_doc": cancella tutte le cartelle doc;
			-"clean_lib": cancella le cartelle contenenti i jar di API e API_client;
			-"clean_all": equivalente all'eseguire tutti i 3 comandi precedenti.
			
N.B. Durante le configurazioni del server non sarà possibile leggere quello che si sta scrivendo da tastiera finche non si premerà invio.
		
	