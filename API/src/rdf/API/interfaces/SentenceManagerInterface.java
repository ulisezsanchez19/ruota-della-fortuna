package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import rdf.API.Sentence;

/**
 * interfaccia (usata dal client) del modulo server per la gestione delle frasi
 *
 */
public interface SentenceManagerInterface extends Remote {
	
	public boolean insertSentence(Sentence sentence) throws RemoteException;
	
	public boolean insertSentences(List<Sentence> sentences) throws RemoteException;
	
	/**
	 * metodo per ricevere frasi dal db in base ai filtri
	 * @param letter lettera iniziale delle frasi da ricevere
	 * @param search parametro che indica la corrispondenza da trovare
	 * @param page offset dei risultati voluti
	 * @param maxNumber limita il numero di risultati
	 * @return array delle frasi
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public Sentence[] getSentences(char letter, String search, int page, int maxNumber) throws RemoteException;
	
	/**
	 * restituisce il numero totale di frasi che corrispondono ai filtri
	 * @param letter lettera iniziale delle frasi da ricevere
	 * @param search parametro che indica la corrispondenza da trovare
	 * @return numero del totale
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public int getSentencesCount(char letter, String search) throws RemoteException;

	public boolean updateSentence(Sentence sentence) throws RemoteException;
		
	public boolean deleteSentences(List<Sentence> sentences) throws RemoteException;
}
