package rdf.API;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe che modellizza il turno di un giocatore nella partita
 *
 */
public class Turn implements Serializable {
	
	public User player;
	public List<Move> moves = new ArrayList<>();
	
	public Turn(User player)
	{
		this.player = player;
	}

}
