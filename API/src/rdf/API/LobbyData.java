package rdf.API;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Contiene i dati della lobby
 *
 */
public class LobbyData implements Serializable {
	
	private static final int MAXPLAYER = 3;	
	private static final long serialVersionUID = 1L;
	private final int number;
	private final String name;
	private Status status;
	private final LocalDateTime time;
	private List<User> playersList;
	private List<User> observersList;
	
	/**
	 * Costruttore dell'oggetto LobbyData
	 * 
	 * @param number numero identificativo della lobby
	 * @param name	nome della lobby
	 * @param status indica lo stato corrente della lobby
	 * @param time tempo di creazione della lobby
	 * @param playersList lista giocatori
	 * @param observersList lista osservatori
	 */
	public LobbyData(int number, String name, Status status, LocalDateTime time, List<User> playersList,
			List<User> observersList) {
		super();
		this.number = number;
		this.name = name;
		this.status = status;
		this.time = time;
		this.playersList = playersList;
		this.observersList = observersList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getNumber() {
		return number;
	}
	public String getName() {
		return name;
	}
	public Status getStatus() {
		return status;
	}
	public LocalDateTime getTime() {
		return time;
	}
	public synchronized List<User> getPlayersList() {
		return playersList;
	}
	public synchronized List<User> getObserversList() {
		return observersList;
	}
	
	public synchronized List<String> getObserversNames() {
		
		List<String> names = new ArrayList<>();
		for(User u : observersList)
			names.add(u.getUsername());
		
		return names;
	}
	
	public synchronized List<Integer> getObserversIDs() {
		
		List<Integer> idList = new ArrayList<>();
		for(User u : observersList)
			idList.add(u.getId());
		
		return idList;
	}
	
	public synchronized int getPlayersNumber() {
		return playersList.size();
	}
	
	public synchronized List<String> getPlayersNames() {
		
		List<String> names = new ArrayList<>();
		for(User u: playersList)
			names.add(u.getUsername());
		
		return names;
	}
	
	public synchronized List<Integer> getPlayersIDs() {
		
		List<Integer> idList = new ArrayList<>();
		for(User u : playersList)
			idList.add(u.getId());
		
		return idList;
	}
	
	public synchronized int getObserversNumber() {
		return observersList.size();
	}
	
	/**
	 * Se la lobby � ancora aperta, aggiunge il giocatore alla lista giocatori.
	 * Una volta che la lista giocatori � piena, chiude la lobby.
	 * @param player
	 * @return boolean di conferma inserimento.
	 */
	public synchronized boolean addPlayer(User player) {
		if(status == Status.CLOSED)
			return false;
		
		playersList.add(player);
		
		if(playersList.size() >= MAXPLAYER)
			status = Status.CLOSED;
		
		return true;
	}
	
	public synchronized void addObvserver(User observer) {
		observersList.add(observer);
	}

	/**
	 * Rimuove il giocatore della lobby, e riapre la lobby nel caso fosse chiusa.
	 * @param player
	 * @return
	 */
	public synchronized boolean removePlayer(User player) {
		if(!playersList.remove(player)) return false;
		if(playersList.size() < 3 && status == Status.CLOSED)
			status = Status.OPEN;
		return true;
	}
	
	public synchronized boolean removeObserver(User observer) {
		return observersList.remove(observer);
	}
	
	/**
	 * Imposta lo stato della lobby a "PLAYNG".
	 */
	public void start()
	{
		status = Status.PLAYING;
	}
	

}
