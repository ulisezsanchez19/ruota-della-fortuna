package rdf.API;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contiene i dati di una manche e i metodi necessari durante una partita
 *
 */
public class Manche implements Serializable {
	
	final static int ROWS = 4;
	final static int COLUMNS = 15;
	
	public int manche_id;
	public int[] points = {0, 0, 0};
	int consonantsNumber;
	Map<Character, List<Integer>> lettersPositionsMap;
	public Sentence objSentence;
	public List<Turn> turnsList = new ArrayList<>();
	public int winner;
	public Manche() {}
	
	/**
	 * Costruttore della classe Manche, che inizializza i dati
	 * sulla base della frase data
	 * @param objSentence frase misteriosa
	 */
	public Manche(Sentence objSentence)
	{
		this.objSentence = objSentence;
		inizializeConsonantNumber(objSentence.sentence);
		initializeLettersPositionsMap(objSentence.sentence);
	}
	
	/**
	 * Restituisce la posizione di tutte le occorrenze del carattere dato
	 * all'interno del tabellone
	 * @param type specifica il tipo di carattere ricercato: vocale, consonante, simbolo di punteggiatura
	 * @param input il carattere da ricercare
	 * @return array delle posizioni del carattere
	 */
	public Integer[] getLetter(int type, char input)
	{
		Character letter = Character.toUpperCase(input);
		int letterType = 0;
		if(!Character.isLetter(letter))
			letterType = 2;
		else if(letter == 'A' || letter == 'E' || letter == 'I' || letter == 'O' || letter == 'U') 
			letterType = 1;
		if(letterType != type) return null;
		
		List<Integer> pos = lettersPositionsMap.get(letter);
		if(letterType == 0 && pos != null) consonantsNumber -= pos.size(); 
		
		if(pos == null)
			return null;
		else
			return pos.toArray(new Integer[pos.size()]); 
	}


	public boolean consonantsRemaining() {
		return consonantsNumber > 0;
	}
	
	/**
	 * Conta il numero di consonanti all'interno della frase misteriosa
	 * @param sentence frase misteriosa
	 */
	private void inizializeConsonantNumber(String sentence)
	{
		consonantsNumber = 0;
		for(int i = 0; i < sentence.length(); i++)
		{
			char c = sentence.charAt(i);
			if(Character.isLetter(c) && c != 'A' && c != 'E' && c != 'I' && c != 'O' && c != 'U')
			{
				consonantsNumber++;
			}
		}
	}
	
	/**
	 * Data la frase misteriosa, crea una mappa di tutti i caratteri con le relative posizioni
	 * @param sentence frase misteriosa
	 */
	private void initializeLettersPositionsMap(String sentence)
	{
		// Formatta la frase nel formato richiesto dal tabellone
		String formattedSentence = StringUtilities.splitSentenceString(sentence);
		String[] sections = formattedSentence.split("\\+");
        int startingRow = sections.length >= 3 ? 0 : 1;
        
        List<Integer> usedPositions = new ArrayList<>();
        Map<Character, List<Integer>> lettersPositionsMap = new HashMap<>();
        
        // per ogni carattere, aggiunge le posizioni alla mappa
        for(int i = 0; i < sections.length; i++)
        {
            String section = sections[i];
            int startingColumn = (COLUMNS - section.length()) / 2;
            
            for(int a = 0; a < section.length(); a++)
            {
                char letter = section.charAt(a);
                if(letter != ' ')
                {
                    int position = ((startingRow + i) * COLUMNS) + startingColumn + a;
                    usedPositions.add(position);
                    List<Integer> positions = lettersPositionsMap.get(letter);
                    if(positions == null)
                    {
                        positions = new ArrayList<>();
                    }
                    
                    positions.add(position);
                    lettersPositionsMap.put(letter, positions);
                }
            }
        }
        
        // Aggiunge alla mappa il carattere speciale "*", che simboleggia uno spazio vuoto
        List<Integer> blankPositions = new ArrayList<>();
        int positionCount = 0;
        for(int i = 0; i < 60; i++)
        {
            if(usedPositions.size() > positionCount && usedPositions.get(positionCount) == i)
                positionCount++;
            else
                blankPositions.add(i);
            
        }
        lettersPositionsMap.put('*', blankPositions);
        
        this.lettersPositionsMap = lettersPositionsMap;
	}
	

	
	public Map<Character, List<Integer>> getLettersPositionsMap()
	{
		return lettersPositionsMap;
	}
	
	public String getSentence() { return objSentence.sentence; }
}
