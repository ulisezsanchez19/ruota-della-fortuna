package rdf.DAO;

/**
 * Factory dei DAO. ristituisce il DAO richiesto in base al database settato.
 * � necessario settare un tipo di database prima di utilizzarla.
 *
 */
public class DAOFactory {
	
	static String databaseType = "";
	
	/**
	 * metodo per settare il database utilizzato
	 * @param type tipo database
	 */
	static public void setDatabaseType(String type) {
		databaseType = type;
	}
	
	/**
	 * metodo per ottenere gli oggetti DAO richiesti.
	 * � necessario effettuare il cast dell'oggetto DAO generico restituito
	 * nell'oggetto DAO richiesto. 
	 * @param name nome del dao richiesto
	 * @return oggetto DAO generico
	 */
	static public GenericDAOImpl getDAO(String name) {
		
		if(databaseType.equals("")) return null;
		
		switch(name)
		{
		case "MancheDAO":
			if(databaseType.equals("PostgreSQL"))
				return new MancheDAOImpl();
			break;
		case "MatchDAO":
			if(databaseType.equals("PostgreSQL"))
				return new MatchDAOImpl();
			break;
		case "MoveDAO":
			if(databaseType.equals("PostgreSQL"))
				return new MoveDAOImpl();
			break;
		case "ScoreDAO":
			if(databaseType.equals("PostgreSQL"))
				return new ScoreDAOImpl();
			break;
		case "SentenceDAO":
			if(databaseType.equals("PostgreSQL"))
				return new SentenceDAOImpl();
			break;
		case "UserDAO":
			if(databaseType.equals("PostgreSQL"))
				return new UserDAOImpl();
			break;
		}
		
		return null;
	}

}
