package playerRdF;

import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import rdf.ClientRdF;

public class playerRdF extends Application {
	
	ClientRdF main;
			
	@Override
	public void start(Stage primaryStage) throws IOException {
		
		ClientRdF main = new ClientRdF();		
		main.start(primaryStage, false);
		
	}

	public static void main(String[] args) {
		launch(args);
	}
	
}