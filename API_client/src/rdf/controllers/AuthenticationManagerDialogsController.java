package rdf.controllers;

import java.rmi.RemoteException;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rdf.InstanceData;
import rdf.API.interfaces.AccountManagerInterface;

/**
 * controller dei pop-up della finestra di autenticazione 
 */
public class AuthenticationManagerDialogsController {

	@FXML
	private Text closeDialog;

	@FXML
	private Text txtTitle;

	@FXML
	private TextField tfVerification;

	@FXML
	private Button btnSend;

	@FXML
	private Label lbConfirmationCodeLog;

	@FXML
	private Button btnSendAgain;

	@FXML
	private ProgressIndicator piElaborationg;

	private Scene scene;

	private AuthenticationViewController authenticationController;

	/**
	 * metodo per inizializzare il pop-up in base a un tipo passato come parametro
	 * tipi disponibili: conferma email, recupero password.
	 * @param typeDialog tipo di pop-up
	 * @param authenticationController
	 * @param scene
	 */
	public void initialize(boolean typeDialog, AuthenticationViewController authenticationController, Scene scene) {

		this.scene = scene;
		this.authenticationController = authenticationController;

		Platform.runLater(() -> {
			if (typeDialog) {
				txtTitle.setText(" Confirm your email ");
				tfVerification.setPromptText("Insert your code");
				setActionButton(typeDialog);
			} else {
				txtTitle.setText(" Recovery Password ");
				tfVerification.setPromptText("Insert your email");
				btnSendAgain.setVisible(typeDialog);
				setActionButton(typeDialog);

			}
		});
	}

	/**
	 * metodo usato per inviare il codice di verifica
	 */
	@FXML
	public void sendVerificationCode() {
		
		piElaborationg.setVisible(true);
		btnSend.setDisable(true);
		btnSendAgain.setDisable(true);

		new Thread(new Runnable() {
			public void run() {
				AccountManagerInterface accountManager;
				try {
					accountManager = InstanceData.stub.getAccountManager();
					int result = accountManager.confirmRegistration(authenticationController.getUserDeails(), tfVerification.getText());
					
					Platform.runLater(() -> {
						String log = "";
						if (result == 0)
							log = "Registration complete!";
						else {
							switch (result) {
							case 1:
								log = "Account not found!";
								break;
							case 2:
								log = "Wrong code!";
								break;
							case 3:
								log = "ERROR!";
								break;
							}

							btnSend.setDisable(false);
							btnSendAgain.setDisable(false);
						}
						tfVerification.setText("");
						lbConfirmationCodeLog.setText(log);
						piElaborationg.setVisible(false);
					});
					
					if(result == 0) close(3000);
				} catch (RemoteException e) {
					InstanceData.interruptClient();
				}
			}
		}).start();
	}

	/**
	 * metodo usato per inviare l'email durante il rest della password
	 */
	@FXML
	void sendEmail() {
		
		piElaborationg.setVisible(true);
		btnSend.setDisable(true);
		
		new Thread(new Runnable() {
			public void run() {
				boolean result = false;
				try {
					AccountManagerInterface accountManager = InstanceData.stub.getAccountManager();
					result = accountManager.resetPassword(tfVerification.getText(), InstanceData.isAdmin);
					String log = result ? "Email sent " : "Error";
					
					Platform.runLater(() -> {
						lbConfirmationCodeLog.setText(log);
					});
					
					piElaborationg.setVisible(false);
					if(result)
					{
						btnSend.setDisable(true);
						close(3000);
					}
				} catch (RemoteException e) {
					InstanceData.interruptClient();
				}	
			}			
		}).start();
	}

	/**
	 * metodo usato per programmare la chiusura del pop-up dopo un certo tempo
	 * @param t tempo
	 */
	private void close(int t) {
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
		  @Override
		  public void run() {
			  closeDialog(null);
		  }
		}, t);
	}

	/**
	 * metodo per chiudere il pop-up
	 * @param event
	 */
	@FXML
	void closeDialog(MouseEvent event) {
		Platform.runLater(() -> {
			Stage window = (Stage) (scene.getWindow());
			window.close();
		});
	}

	/**
	 * Settare l'azione del bottone in base al pop-up creato
	 * @param dialogType tipo del pop-up
	 */
	private void setActionButton(boolean dialogType) {
		btnSend.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (dialogType)
					sendVerificationCode();
				else
					sendEmail();
			}
		});

	}

}
