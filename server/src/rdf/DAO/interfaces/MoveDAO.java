package rdf.DAO.interfaces;

import rdf.API.Manche;

public interface MoveDAO {

	boolean insertMovesFromMancheBatch(Manche manche);
	
	boolean releaseConnection();
}
