package rdf;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rdf.API.User;

/**
 * classe utilizzata per ottenere le statistiche dal database
 *
 */
public class RdFStatistics {

	Connection connection;
	public final static int LIMIT = 100;
	
	/**
	 *  costruttore della classe. Stabilisce la connesione col DB
	 */
	RdFStatistics()
	{
		connection = DatabaseConnection.getDatabaseConnection().getConnection();
	}
	
	/**
	 * metodo che restituisce la lista completa delle statistiche del gioco
	 * @return lista statistiche
	 */
	public List<String[]> getGamesStatics() {
		
		List<String[]> statics = new ArrayList<>();		
		statics.add(getTopUserMatchScore());
		statics.add(getTopUserMancheScore());
		statics.add(getUserWithMostManchePlayed());
		statics.add(getUserWithHighestAvgPointsPerManche());
		statics.add(getUserWithHighestErrorCount());
		statics.add(getUserWithHighestLoseFromWheelCount());
		statics.add(getConsonantCallWithHighestPointEarned());
		statics.add(new String[] {getAvgMoveCountPerManche()});
		
		return statics;
	}

	/**
	 * metodo che restituisce la lista completa delle statistiche personali di un utente
	 * @param user utente
	 * @return lista statistiche
	 */
	public String[] getUserStatics(User user) {
		
		String[] statics = new String[11];
		int cont = 0;
		
		statics[cont++] = getManchePlayedCount(user.getId());
		statics[cont++] = getMatchPlayedCount(user.getId());
		statics[cont++] = getMancheObservedCount(user.getId());
		statics[cont++] = getMatchObservedCount(user.getId());
		statics[cont++] = getMancheWonCount(user.getId());
		statics[cont++] = getMatchWonCount(user.getId());
		statics[cont++] = getAvgMatchScore(user.getId());
		statics[cont++] = getAvgPassFromWheelFromManche(user.getId());
		statics[cont++] = getAvgPassFromWheelFromMatch(user.getId());
		statics[cont++] = getAvgLoseFromWheelFromManche(user.getId());
		statics[cont++] = getAvgLoseFromWheelFromMatch(user.getId());
		
		return statics;
	}
	
	private String[] getTopUserMatchScore()
	{
		String sql = "SELECT username, points " +
						"FROM users JOIN scores ON id_user = user_id " +
						"AND id_match IS NOT NULL " +
						"ORDER BY points DESC LIMIT " + LIMIT;
		String[] names = new String[] { "username", "points" };

		return doQuery(sql, names, LIMIT,  "noData");
	}
	
	private String[] getTopUserMancheScore()
	{
		String sql = "SELECT username, points " +
						"FROM users JOIN scores ON id_user = user_id " +
						"AND id_manche IS NOT NULL " +
						"ORDER BY points DESC LIMIT " + LIMIT;
		String[] names = new String[] { "username", "points" };
		
		return doQuery(sql, names, LIMIT,  "noData");
	}
	
	private String[] getUserWithMostManchePlayed()
	{
		String sql = "SELECT COUNT(id_user) AS cont, username " + 
						"FROM has_played " + 
						"JOIN users ON user_id = id_user " + 
						"GROUP BY id_user, username " + 
						"ORDER BY cont DESC LIMIT " + LIMIT;
		String[] names = new String[] { "username", "cont" };
		
		return doQuery(sql, names, LIMIT,  "noData");
	}
	
	private String[] getUserWithHighestAvgPointsPerManche()
	{
		String sql = "SELECT ROUND(AVG(points),2) AS av, username " + 
				"		FROM scores " + 
				"		JOIN users ON user_id = id_user " + 
				"		WHERE id_match IS NULL " + 
				"		GROUP BY id_user, username " + 
				"		ORDER BY av DESC LIMIT " + LIMIT;
		String[] names = new String[] { "username", "av" };
		
		return doQuery(sql, names, LIMIT,  "noData");
	}
	
	private String[] getUserWithHighestErrorCount()
	{
		String sql = "SELECT COUNT(error) AS cont, username " + 
				"		FROM moves " + 
				"		JOIN users ON user_id = id_user " + 
				"		WHERE error = true " + 
				"		GROUP BY username " + 
				"		ORDER by cont DESC LIMIT " + LIMIT;
		String[] names = new String[] { "username", "cont" };
		
		return doQuery(sql, names, LIMIT,  "noData");
	}
	
	private String[] getUserWithHighestLoseFromWheelCount()
	{
		String sql = "SELECT COUNT(id_user) AS cont, username " + 
				"		FROM moves " + 
				"		JOIN users ON user_id = id_user " + 
				"		WHERE details = 'LOSE' " + 
				"		GROUP BY username " + 
				"		ORDER BY cont DESC LIMIT " + LIMIT;
		String[] names = new String[] { "username", "cont" };
		
		return doQuery(sql, names, LIMIT,  "noData");
	}
	
	private String[] getConsonantCallWithHighestPointEarned()
	{
		String sql = "SELECT details, points, sentence, username FROM moves " + 
				"	JOIN users ON user_id = id_user " + 
				"	JOIN manches ON id_manche = manche_id " + 
				"	JOIN sentences ON id_sentence = sentence_id " + 
				"	WHERE id_move_type = 2 " + 
				"	ORDER BY points DESC LIMIT " + LIMIT;
		String[] names = new String[] { "username", "details", "points", "sentence" };
		
		return doQuery(sql, names, LIMIT,  "noData");
	}
	
	private String getAvgMoveCountPerManche()
	{
		String sql = "SELECT ROUND(AVG(cont),2) AS avg FROM (SELECT COUNT(id_manche) AS CONT, id_manche FROM moves " + 
				"	GROUP BY id_manche) AS sub";
		String[] names = new String[] { "avg" };
		
		return doQuery(sql, names, 1, "noData")[0];
	}
	
	private String getManchePlayedCount(int id)
	{
		String sql = "SELECT COUNT(id_manche) AS count FROM has_played " + 
				"		WHERE id_user = " + id;
		String[] names = new String[] { "count" };
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getMatchPlayedCount(int id)
	{
		String sql = "SELECT COUNT(match_id) AS count FROM (SELECT match_id FROM has_played " + 
				"		JOIN manches ON id_manche = manche_id " + 
				"		JOIN matches ON id_match = match_id " + 
				"		WHERE id_user = " + id + 
				"		GROUP BY match_id) AS sub";
		String[] names = new String[] { "count" };
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getMancheObservedCount(int id)
	{
		String sql = "SELECT COUNT(id_manche) AS count FROM has_observed " + 
				"		WHERE id_user = " + id;
		String[] names = new String[] { "count" };
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getMatchObservedCount(int id)
	{
		String sql = "SELECT COUNT(match_id) AS count FROM (SELECT match_id FROM has_observed " + 
				"		JOIN manches ON id_manche = manche_id " + 
				"		JOIN matches ON id_match = match_id " + 
				"		WHERE id_user = " + id + 
				"		GROUP BY match_id) AS sub";
		String[] names = new String[] { "count" };
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getMancheWonCount(int id)
	{
		String sql = "SELECT COUNT(id_winner) AS count FROM manches " + 
				"		WHERE id_winner = " + id;
		String[] names = new String[] { "count" };
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getMatchWonCount(int id)
	{
		String sql = "SELECT COUNT(id_winner) AS count FROM matches " + 
				"		WHERE id_winner = " + id;
		String[] names = new String[] { "count" };
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getAvgMatchScore(int id)
	{
		String sql = "SELECT ROUND(AVG(points),2) AS avg " + 
				"		FROM scores " + 
				"		WHERE id_user = " + id + " AND id_match IS NOT NULL";
		String[] names = new String[] { "avg"};
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getAvgPassFromWheelFromManche(int id)
	{
		String sql = "SELECT ROUND(( " + 
				"			COUNT(CASE details " + 
				"				  WHEN 'PASS' THEN 1 " + 
				"				  ELSE null " + 
				"				  END) * 1.0 / NULLIF(" + 
				"			(SELECT COUNT(*) FROM has_played " + 
				"			 WHERE id_user = " + id + 
				"			), 0) " + 
				"	   ),2) AS avg FROM moves WHERE id_user = " + id;
		String[] names = new String[] { "avg"};
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getAvgPassFromWheelFromMatch(int id)
	{
		String sql = "SELECT ROUND(( " + 
				"			COUNT(CASE details " + 
				"				WHEN 'PASS' THEN 1 " + 
				"				ELSE null " + 
				"				END) * 1.0 / NULLIF(" + 
				"			(SELECT COUNT(*) " + 
				"				FROM (SELECT match_id FROM has_played " + 
				"					  JOIN manches ON id_manche = manche_id " + 
				"					  JOIN matches ON id_match = match_id " + 
				"					  WHERE id_user = " + id + 
				"					  GROUP BY match_id) AS contMatch " + 
				"			), 0) " + 
				"	   ),2) AS avg FROM moves WHERE id_user = " + id;
		String[] names = new String[] { "avg"};
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getAvgLoseFromWheelFromManche(int id)
	{
		String sql = "SELECT ROUND(( " + 
				"			COUNT(CASE details " + 
				"				  WHEN 'LOSE' THEN 1 " + 
				"				  ELSE null " + 
				"				  END) * 1.0 / NULLIF(" + 
				"			(SELECT COUNT(*) FROM has_played " + 
				"			 WHERE id_user = " + id + 
				"			), 0) " + 
				"	   ),2) AS avg FROM moves WHERE id_user = " + id;
		String[] names = new String[] { "avg"};
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	private String getAvgLoseFromWheelFromMatch(int id)
	{
		String sql = "SELECT ROUND(( " + 
				"			(COUNT(CASE details " + 
				"				WHEN 'LOSE' THEN 1 " + 
				"				ELSE null " + 
				"				END) " + 
				"			) * 1.0 / NULLIF(" + 
				"			(SELECT COUNT(*) " + 
				"				FROM ( " + 
				"					SELECT id_match FROM has_played " + 
				"					JOIN manches ON manche_id = id_manche " + 
				"					JOIN matches ON match_id = id_match " + 
				"					WHERE id_user = " + id + 
				"					GROUP BY id_match " + 
				"				) AS contMatch " + 
				"			), 0) " + 
				"	   ),2) AS avg FROM moves WHERE id_user = " + id;
		String[] names = new String[] { "avg"};
		
		return doQuery(sql, names, 1, "0")[0];
	}
	
	public String getTotalUsers()
	{
		String sql = "SELECT COUNT(*) AS COUNT FROM users";
		String[] names = new String[] { "COUNT" };

		return doQuery(sql, names, 1,  "noData")[0];
	}
	
	public String getTotalMatch()
	{
		String sql = "SELECT COUNT(*) AS COUNT FROM matches";
		String[] names = new String[] { "COUNT" };

		return doQuery(sql, names, 1,  "noData")[0];
	}
	
	public String getTotalManche()
	{
		String sql = "SELECT COUNT(*) AS COUNT FROM manches";
		String[] names = new String[] { "COUNT" };

		return doQuery(sql, names, 1,  "noData")[0];
	}
	
	public String getTotalSentences()
	{
		String sql = "SELECT COUNT(*) AS COUNT FROM sentences";
		String[] names = new String[] { "COUNT" };

		return doQuery(sql, names, 1,  "noData")[0];
	}
	
	/**
	 * metodo che esegue le query sul DB.
	 * @param sql query da eseguire
	 * @param names nomi delle colonne da restituire
	 * @param rows numero di risultati da ottenere
	 * @param nullValue il valore da assegnare quando non ci sono risultati
	 * @return risultato query
	 */
	private String[] doQuery(String sql, String[] names, int rows, String nullValue)
	{
		if(connection == null) return null;
		
		try {
			
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			String[] stringResult = new String[names.length * rows];
			
			int cont = 0;
			while (result.next()) {
				for(int i = 0; i < names.length; i++)
				{
					stringResult[cont++] = result.getString(names[i]);
				}
			}
			
			if(cont < names.length * rows)
			{
				String[] temp = new String[cont];
				for(int i = 0; i < cont; i++)
					temp[i] = stringResult[i];
				stringResult = temp;
			}
			
			if(stringResult == null || stringResult.length == 0 || stringResult[0] == null)
			{
				stringResult = new String[names.length];
				for(int i = 0; i < stringResult.length; i++)
					stringResult[i] = nullValue;
			}

			return stringResult;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;		
	}
}
