package rdf.controllers;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import rdf.InstanceData;

/**
 * controller della finestra delle statistiche
 *
 */
public class StatisticsViewController {

	@FXML
	private Label lbData;

	@FXML
	private Label lbHour;

	@FXML
	private GridPane gpStatistics;

	Thread timeThread;
	String[] rdfStatistics;
	String[] rdfStatisticsFormat = new String[] { "Total users:", "Online users", "User looking for a lobby", "Players",
			"Observers", "Total matches", "Matches currentrly playng", "Total manches", "Total sentences" };
	
	String[] colors = new String[] { "INDIGO", "TEAL", "LIME", "INDIGO", "TEAL", "LIME", "INDIGO", "TEAL", "LIME" };

	/**
	 * inizializza la finestra prendendo i dati dal server
	 */
	public void initialize() {
		showCurrentData();
		timeThread = new Thread(this::handleThread);
		timeThread.start();

		try {
			rdfStatistics = InstanceData.stub.getStatisticsManager().getRdFStatistics();
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}

		int col = 0;
		int row = 0;
		for (int i = 0; i < rdfStatistics.length; i++) {
			VBox vb = new VBox();
			vb.setAlignment(Pos.CENTER);
			GridPane.setMargin(vb, new Insets(5, 5, 5, 5));
			vb.getStyleClass().setAll("VBox-GridPaneAdmin");
			vb.getStylesheets().setAll(getClass().getResource("/rdf.styles/statistics.css").toExternalForm());
			
			Text tx = new Text(rdfStatisticsFormat[i]);
			tx.setFill(Paint.valueOf(colors[i]));

			Label lb = new Label(rdfStatistics[i]);
			vb.getChildren().add(tx);
			vb.getChildren().add(lb);
			
			gpStatistics.add(vb, col, row);
			col++;
			if(col == 3)
			{
				row++;
				col = 0;
			}
		}
	}

	/**
	 * metodo per aggironare la data corrente
	 */
	private void showCurrentData() {
		Date currentDate = new Date();
		SimpleDateFormat formatdate = new SimpleDateFormat("dd/MM/yyyy");
		lbData.setText(formatdate.format(currentDate));

	}

	/**
	 * metodo per invocarre il thread della data
	 */
	private void handleThread() {

		DateFormat dateformat = new SimpleDateFormat("HH:mm:ss", Locale.ITALIAN);

		while (!Thread.interrupted()) {

			String dateString = "" + dateformat.format(new Date());

			Platform.runLater(() -> {
				lbHour.setText(dateString);
			});

			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
			}
		}
	}

	/**
	 * metodo per tornare alla home
	 */
	@FXML
	void goToHome(ActionEvent event) {
		timeThread.interrupt();
		InstanceData.gotoHome();
	}
}
