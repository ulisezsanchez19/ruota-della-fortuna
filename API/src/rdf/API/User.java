package rdf.API;

import java.io.Serializable;

/**
 * Contiene i dati base di un utente
 *
 */
public class User implements Serializable {
	
	int user_id;
	String username;
	
	public User(int user_id, String username) {
		super();
		this.user_id = user_id;
		this.username = username;
	}

	public int getId() { return user_id; }

	public String getUsername() { return username; }
	
	@Override
    public boolean equals(Object o) { 
  
		if (o == this) { return true; } 
        if (!(o instanceof User)) { return false; } 
          
        User u = (User) o;
          
        return user_id == u.user_id;
    } 

}
