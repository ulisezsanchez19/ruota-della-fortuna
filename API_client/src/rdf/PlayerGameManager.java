package rdf;

import java.rmi.RemoteException;

import rdf.API.interfaces.PlayerGameManagerInterface;

/**
 * estesione del modulo gameManager destinata alla gestione dai player
 *
 */
public class PlayerGameManager extends GameManager implements PlayerGameManagerInterface {


	/**
	 * metodo usato per comunicare al server la mossa scelta
	 * @param btnId id della mossa scelta
	 */
	public void selectMove(String btnId)
	{
		Integer move = -1;
		switch(btnId)
		{
			case "btSelectWheel":
				move = 0;
				break;
			case "btBuyVocal":
				move = 1;
				break;
			case "btGuessSolution":
				move = 2;
				break;
			case "btJollyYes":
				move = 3;
				break;
			case "btJollyNo":
				move = 4;
				break;
			case "tbtWheel":
				move = 5;
				break;
		}
		try {
			gameMaster.selectMove(move);
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}		
	}
	
	/**
	 * metodo usato per inviare la risposta al server
	 * @param answer risposta
	 */
	public void sendAnswer(String answer)
	{
		try {
			gameMaster.giveAnswer(answer);
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
	}
}
