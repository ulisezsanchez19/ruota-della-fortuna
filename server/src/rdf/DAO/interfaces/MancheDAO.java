package rdf.DAO.interfaces;

import rdf.API.Manche;
import rdf.API.Match;
import rdf.API.Sentence;
import rdf.API.User;

public interface MancheDAO {
		
	int insertManche(Match match, Sentence sentence);
	
	boolean addWinner(Manche manche, User user);

	boolean resetMancheStatus();
	
	boolean releaseConnection();
	
}
