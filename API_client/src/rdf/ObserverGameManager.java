package rdf;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rdf.API.*;
import rdf.API.interfaces.ObserverGameManagerInterface;

/**
 * estesione del modulo gameManager destinata alla gestione degli osservatori
 *
 */
public class ObserverGameManager extends GameManager implements ObserverGameManagerInterface {

	/**
	 * metodo per inviare i dati della partita ad un osservatore appena entrato
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void sendGameData(HashMap<Character, Integer[]> table, String theme, Map<Character, List<Integer>> letters, int[] jollies, int[] mancheScore, int[] matchScore, Manche[] manches)
			throws RemoteException {

		gameViewController.initializeNewManche(table, theme);
		gameViewController.updateMatchScore(matchScore);
		gameViewController.updateMancheScore(mancheScore);
		gameViewController.updateJollies(jollies, 0);
		
		Set<Character> lettersSet = letters.keySet();
		for(Character l : lettersSet)
		{
			List<Integer> pos = letters.get(l);
			gameViewController.updateTable(letters.get(l).toArray(new Integer[pos.size()]), l);
		}
		
		for(int i = 0; i < 5; i++)
		{
			if(manches[i] != null)
			{
				gameViewController.detailsNewManche(i);
				for(Turn t : manches[i].turnsList)
					gameViewController.detailsNewTurn(t);
			}
			else
				break;
		}
	}



}
