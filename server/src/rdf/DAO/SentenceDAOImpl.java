package rdf.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import rdf.serverRdF;
import rdf.API.Sentence;
import rdf.API.User;
import rdf.DAO.interfaces.SentenceDAO;

/**
 * Oggetto DAO che si occupa delle query riguardanti le frasi.
 * Fa utilizzo della classe GenericDAO.
 *
 */
class SentenceDAOImpl extends GenericDAOImpl implements SentenceDAO {
		
	public SentenceDAOImpl()
	{
		super();
	}

	@Override
	public boolean insertSentences(List<Sentence> sentences) {
		
		Object[] objects = new Object[sentences.size() * 2];
		int i = 0;
		for(Sentence s : sentences)
		{
			objects[i++] = s.sentence;
			objects[i++] = s.theme;
		}
		
		String[] names = {"sentences", "sentence", "theme"};
		
		return insertBatch(names, objects, false);
	}
	
	@Override
	public boolean insertSentence(Sentence sentence) {
		
		Object[] objects = new Object[2];
		objects[0] = sentence.sentence;
		objects[1] = sentence.theme;
		
		String[] names = {"sentences", "sentence", "theme"};
		
		return insert(names, objects, "") == 0 ? true : false;
	}

	@Override
	public Sentence[] getGameSentences(List<User> players) {
		
		if(connection == null) return null;
		
		try {
			String sql = "SELECT  * FROM sentences WHERE sentences.sentence_id NOT IN("+ 
							"SELECT sentences.sentence_id FROM sentences " + 
							"LEFT JOIN has_seen ON sentences.sentence_id = has_seen.id_sentence " + 
							"WHERE has_seen.id_user = ? OR has_seen.id_user = ? OR has_seen.id_user = ? " + 
							"GROUP BY sentences.sentence_id)" + 
						"ORDER BY RANDOM() LIMIT 5";
			PreparedStatement statement = connection.prepareStatement(sql);
			
			for(int i = 0; i < players.size(); i++)
				statement.setInt(i + 1, players.get(i).getId());
						
			ResultSet result = statement.executeQuery();
			
			Sentence[] sentences = new Sentence[5];
			int cont = 0;
			while (result.next()) {
				Sentence sentence =  new Sentence(
						result.getInt("sentence_id"),
						result.getString("sentence"),
						result.getString("theme")
						);
				sentences[cont++] = sentence;
			}
			
			return cont < 5 ? null : sentences; 
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Sentence[] getSentences(char letter, String search, int page, int maxNumber) {
		if(connection == null) return null;
		try {
			String where = "";
			if(letter != ' ')
				where = "WHERE sentence LIKE '" + letter + "%'";
			else if(search != null)
				where = "WHERE sentence LIKE '%" + search + "%'";
			
			String sql = "SELECT * FROM sentences " + where +
					"ORDER BY sentence_id " + 
					(maxNumber == -1 ? "" : ("LIMIT " + maxNumber + " OFFSET " + page * maxNumber));
			
			
			if(serverRdF.DEBUG_QUERY)
				System.out.println(sql);
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			
			Sentence[] sentences = null;
			if(maxNumber < 0)
			{
				int count = getSentencesCount(letter, search);
				sentences = new Sentence[count];
			}
			else
				sentences = new Sentence[maxNumber];
			int cont = 0;
			while (result.next()) {
				Sentence sentence =  new Sentence(
						result.getInt("sentence_id"),
						result.getString("sentence"),
						result.getString("theme")
						);
				sentences[cont++] = sentence;
			}
			
			return sentences; 
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean updateSentence(Sentence sentence) {

		String[] names = {"sentences", "sentence", "theme", "sentence_id"};
		Object[] objects = {sentence.sentence, sentence.theme, sentence.sentence_id};
		
		return update(names, objects);
	}

	@Override
	public boolean deleteSentences(List<Sentence> sentences) {
		String[] names = {"sentences", "sentence_id"};
		
		Object[] objects = new Object[sentences.size()];
		int i = 0;
		for(Sentence s : sentences)
		{
			objects[i++] = s.sentence_id;
		}
		return deleteBatch(names, objects);
	}

	@Override
	public int getSentencesCount(char letter, String search) {
		
		if(connection == null) return -1;
		try {
			String where = "";
			if(letter != ' ')
				where = "WHERE sentence LIKE '" + letter + "%'";
			else if(search != null)
				where = "WHERE sentence LIKE '%" + search + "%'";
			
			String sql = "SELECT COUNT(*) AS COUNT FROM sentences " + where;
			
			if(serverRdF.DEBUG_QUERY)
				System.out.println(sql);
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			
			int count = 0;
			while (result.next()) {
				count = result.getInt("COUNT");
			};
			
			return count; 
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
