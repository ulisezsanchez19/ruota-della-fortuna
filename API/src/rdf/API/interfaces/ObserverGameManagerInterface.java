package rdf.API.interfaces;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rdf.API.*;

/**
 * interfaccia (usata dal server) dell' estesione del modulo gameManager destinata alla gestione degli osservatori
 *
 */
public interface ObserverGameManagerInterface  extends GameManagerInterface{
	
	/**
	 * metodo per inviare i dati della partita ad un osservatore appena entrato
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void sendGameData(HashMap<Character, Integer[]> table, String theme, Map<Character, List<Integer>> letters, int[] jollies, int[] mancheScore, int[] matchScore, Manche[] manches)  throws RemoteException;

}
