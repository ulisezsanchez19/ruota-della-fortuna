package rdf;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Classe che si occupa dell'invio dei messagi email
 *
 */
public class EmailSender {
	
	public static String accountSystem;
	public static String passwordSystem;

	/**
	 * Metodo di servizio per l'invio di email
	 * @param recepient
	 * @param subject
	 * @param text
	 * @return boolean di conferma invio
	 */
	private static boolean sendEmail(String recepient, String subject, String text) {
		
		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp.office365.com");
		properties.put("mail.smtp.starttls.enable","true");
		properties.put("mail.smtp.port",587);
		
		Session session = Session.getInstance(properties);
		Message message = new MimeMessage(session);
		
		try {
			message.setFrom(new InternetAddress(accountSystem));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
			message.setSubject(subject);
			message.setText(text);
		} catch (MessagingException e) {
			return false; 
		}
		
	    try {
			Transport.send(message,accountSystem,passwordSystem);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    
	    return true;
	}
	
	/**
	 * metodo per l'invio di email di conferma precompilato.
	 * @param recepient email destinatario
	 * @param code codice di conferma
	 * @return boolean di conferma invio.
	 */
	public static boolean sendConfirmEmail(String recepient, String code) {
		
		String subject = " Confirm your RDF account ";
		String text = "Confirm your email by using the following code: \n\n " + code;
		
		return sendEmail(recepient, subject, text);
	}
	
	/**
	 * motodo per l'invio di email di recupero password precompilato.
	 * @param recepient email destinatario
	 * @param password password di recupero
	 * @return boolean di conferma invio.
	 */
	public static boolean sendRecoveryPasswordEmail(String recepient, String password) {

		String subject = " RDF password recovery ";
		String text = "Your new recovery password is:  \n\n " + password;
		
		return sendEmail(recepient, subject, text);
	}
}
