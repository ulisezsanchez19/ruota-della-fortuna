package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * interfaccia del server, usata dal client per ricevere i moduli
 *
 */
public interface serverRdFInterface extends Remote {
	
	public AccountManagerInterface getAccountManager() throws RemoteException;
	
	public LobbyManagerInterface getLobbyManager() throws RemoteException;

	public SentenceManagerInterface getSentenceManager() throws RemoteException;
	
	public StatisticsManagerInterface getStatisticsManager() throws RemoteException;
}
