package rdf.API;

import java.io.Serializable;

/**
 * Classe che modellizza le mosse dei giocatori in partita
 *
 */
public class Move implements Serializable {
	
	MoveType moveType;
	Integer points;
	String details;
	boolean error;
	
	public Move(MoveType moveType, Integer points, String details, boolean error) {
		super();
		this.moveType = moveType;
		this.points = points;
		this.details = details;
		this.error = error;
	}
	
	public MoveType getMoveType() {
		return moveType;
	}
	
	public Integer getPoints() {
		return points;
	}

	public String getDetails() {
		return details;
	}
	
	public boolean getError() {
	 return error;
	}
}
