package rdf.API;

import java.io.Serializable;

/**
 * Contiene i dati aggiuntivi di un utente
 *
 */
public class UserDetails extends User implements Serializable{
	
	String email;
	String name;
	String surname;	
	
	public UserDetails(int user_id, String username, String email, String name, String surname) {
		super(user_id, username);
		this.email = email;
		this.name = name;
		this.surname = surname;
	}
	
	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

}
