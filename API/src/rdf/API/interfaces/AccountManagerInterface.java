package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import rdf.API.User;
import rdf.API.UserDetails;

/**
 * Interfaccia del modulo dedicato alla gestione degli account
 *
 */
public interface AccountManagerInterface extends Remote {
	
	/**
	 * effettua il login di un utente
	 * @param client oggetto remoto del client
	 * @return oggetto dell'utente loggato se il login va a buon fine
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public User login(String email, String pswHash, boolean isAdmin, ClientRdFInterface client) throws RemoteException;


	/**
	 * effettua il logout di un utente
	 * @param user utente da disconnettere
	 * @param client oggetto remoto dell'utente
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void logout(User user, ClientRdFInterface client) throws RemoteException;
	
	/**
	 * avvia la procedura di registrazione di un utente
	 * @return codice di conferma e di gestione errori
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public int register(UserDetails user, String pswHash, boolean isAdmin) throws RemoteException;
	
	/**
	 * conferma la email di un utente durante la procedura di registrazione
	 * @return codice di conferma e di gestione errori
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public int confirmRegistration(UserDetails user, String code) throws RemoteException;
	
	/**
	 * aggiorna il profilo di un utente
	 * @return codice di conferma e di gestione errori
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public int updateProfile(UserDetails user) throws RemoteException;

	/**
	 * aggiorna la password di un utente
	 * @return risultato dell'operazione
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public boolean updatePassword(User user, String oldPswHash, String newPswHash) throws RemoteException;

	/**
	 * resetta la password di un utente
	 * @return risultato dell'operazione
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public boolean resetPassword(String email, boolean isAdmin) throws RemoteException;

	/**
	 * restituisce i dettagli di un utente
	 * @return l'oggetto con UserDetails contenente i dettagli
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public UserDetails getUserProfile(User user) throws RemoteException;


}
