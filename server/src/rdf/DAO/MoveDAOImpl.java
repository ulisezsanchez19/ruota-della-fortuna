package rdf.DAO;

import rdf.API.Manche;
import rdf.API.Move;
import rdf.API.Turn;
import rdf.DAO.interfaces.MoveDAO;

/**
 * Oggetto DAO che si occupa delle query riguardanti le mosse.
 * Fa utilizzo della classe GenericDAO.
 *
 */
class MoveDAOImpl extends GenericDAOImpl implements MoveDAO {

	@Override
	public boolean insertMovesFromMancheBatch(Manche manche) {
		
		int cont = 0;
		for(Turn t : manche.turnsList)
		{
			cont += t.moves.size();
		}
		Object[] objects = new Object[cont * 6];
		
		int i = 0;
		for(Turn t : manche.turnsList)
		{
			for(Move m : t.moves)
			{
				objects[i++] = m.getPoints();
				objects[i++] = m.getDetails();
				objects[i++] = manche.manche_id;
				objects[i++] = m.getMoveType() == null ? null :  m.getMoveType().ordinal() + 1;
				objects[i++] = t.player.getId();
				objects[i++] = m.getError();
			}
		}
		
		String[] names = {"moves", "points", "details", "id_manche", "id_move_type", "id_user", "error"};
		
		return insertBatch(names, objects, false);
	}
}
