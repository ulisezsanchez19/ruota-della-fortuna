package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

import rdf.API.LobbyData;

/**
 * interefaccia (usata dal client) del modulo server che gestisce le lobby
 *
 */
public interface LobbyManagerInterface extends Remote {

	/**
	 * restituisce l'elenco delle lobby presenti
	 * @return lista lobby
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public Map<Integer, LobbyData> getLobbiesList() throws RemoteException;
	
	/**
	 * avvia la procudura per creare una lobby
	 * @param name nome lobby
	 * @param player oggetto remoto del player che crea la lobby
	 * @return la lobby, se viene creata
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public LobbyData createLobby(String name, PlayerGameManagerInterface player) throws RemoteException;
	
	/**
	 * iscrive il client alla ricezione degli aggiornamenti delle lobby
	 * @param userId user da iscrivere
	 * @param listener oggetto remoto del modulo lobbyListener del client
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void enableLobbyUpdate(int userId, LobbyListenerInterface listener) throws RemoteException;
	
	/**
	 * disiscrive il client alla ricezione degli aggiornamenti delle lobby
	 * @param userId user da disiscivere
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void disableLobbyUpdate(int userId) throws RemoteException;
	
	/**
	 * avvia la procedura per entrare in una lobby come giocatore
	 * @param lobbyNumber numero della lobby in cui si vuole entrare
	 * @param player oggetto remoto del modulo GameManager del client
	 * @return la lobby in cui si � entrati, se l'operazione � andata a buon fine
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public LobbyData joinLobbyAsPlayer(Integer lobbyNumber, PlayerGameManagerInterface player) throws RemoteException;
	
	/**
	 * avvia la procedura per entrare in una lobby come osservatore
	 * @param lobbyNumber numero della lobby in cui si vuole entrare
	 * @param observer oggetto remoto del modulo GameManager del client
	 * @return la lobby in cui si � entrati, se l'operazione � andata a buon fine
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public LobbyData joinLobbyAsObserver(Integer lobbyNumber, ObserverGameManagerInterface observer) throws RemoteException;
	
	/**
	 * avvia la procedura per uscire da una lobby nella quale si � giocatore
	 * @param lobbyNumber numero della lobby dalla quale si vuole uscire
	 * @param player oggetto remoto del modulo GameManager del client
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void leaveLobbyAsPlayer(Integer lobbyNumber, Integer player) throws RemoteException;
	
	/**
	 * avvia la procedura per uscire da una lobby nella quale si � osservatore
	 * @param lobbyNumber numero della lobby dalla quale si vuole uscire
	 * @param observer oggetto remoto del modulo GameManager del client
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void leaveLobbyAsObserver(Integer lobbyNumber, Integer observer) throws RemoteException;
}
