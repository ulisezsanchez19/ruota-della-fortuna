package rdf.DAO;

import java.sql.Timestamp;
import java.time.ZoneOffset;

import rdf.API.Match;
import rdf.API.User;
import rdf.DAO.interfaces.MatchDAO;

/**
 * Oggetto DAO che si occupa delle query riguardanti le partite.
 * Fa utilizzo della classe GenericDAO.
 *
 */
class MatchDAOImpl extends GenericDAOImpl implements MatchDAO {
	
	public MatchDAOImpl()
	{
		super();
	}

	@Override
	public int insertMatch(Match match) {
		
		Timestamp ts = new Timestamp(match.getTime().toInstant(ZoneOffset.UTC).toEpochMilli());
		
		String[] names = {"matches", "name", "\"time\"", "id_game_status"};
		Object[] objects = {match.getName(), ts, 1};
		
		return insert(names, objects, "match_id");
	}

	@Override
	public boolean addWinner(Match match, User user) {

		String[] names = {"matches", "id_winner", "id_game_status", "match_id" };
		Object[] objects = {user.getId(), 2, match.match_id};
		
		return update(names, objects);
	}
	
	@Override
	public boolean resetMatchStatus() {
		
		String[] names = {"matches", "id_game_status", "id_game_status"};
		Object[] objects = {3, 1};
		
		return update(names, objects);
	}

}
