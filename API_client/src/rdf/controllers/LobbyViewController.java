package rdf.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import rdf.API.*;
import rdf.API.interfaces.LobbyManagerInterface;
import rdf.InstanceData;

/**
 * controller per la finestra della lobby
 *
 */
public class LobbyViewController {

	@FXML
	private Label lblNumber;
	@FXML
	private Label lblName;
	@FXML
	private Label lblPlayersN;
	@FXML
	private Label lblStatus;
	@FXML
	private Label lblPlayer1;
	@FXML
	private Label lblPlayer2;
	@FXML
	private Label lblPlayer3;
	@FXML
	private ListView<String> lvObservers;
	@FXML
	private TextArea taLog;

	private Scene lobbyScene;

	private Label[] lblPlayers;
	
	public int lobbyNumber;

	/**
	 * metodo per inizializzare la finestra
	 * @param lobbyManager
	 * @param lobbyData la lobby che ver� visualizzata nella finestra
	 * @param lobbyScene
	 */
	public void initialize(LobbyManagerInterface lobbyManager, LobbyData lobbyData,
			Scene lobbyScene) {
		this.lobbyScene = lobbyScene;
		this.lobbyNumber = lobbyData.getNumber();
		lblPlayers = new Label[] { lblPlayer1, lblPlayer2, lblPlayer3 };
	}

	/**
	 * metodo per uscire dalla lobby
	 */
	@FXML
	void leaveLobbyButton(ActionEvent event) {
		InstanceData.lobbyListener.leaveLobby();
		closeLobbyView();
	}

	/**
	 * metodo per ricaricare i dati della lobby
	 * @param lobby dati nuovi
	 */
	public void refreshLobby(LobbyData lobby) {
		lblNumber.setText("" + lobby.getNumber());
		lblName.setText(lobby.getName());
		lblPlayersN.setText("" + lobby.getPlayersNumber());
		lblStatus.setText(lobby.getStatus().toString());

		User[] users = lobby.getPlayersList().toArray(new User[3]);
		for (int i = 0; i < 3; i++) {
			if (users[i] != null)
				lblPlayers[i].setText(users[i].getUsername());
			else
				lblPlayers[i].setText("");
		}

		lvObservers.getItems().clear();
		for (User s : lobby.getObserversList())
			lvObservers.getItems().add(s.getUsername());
	}

	/**
	 * metodo usato per forzare l'uscita dalla lobby
	 */
	public void forceLobbyLeave() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Lobby eliminata");
		alert.setHeaderText(null);
		alert.setContentText("La lobby � stata eliminata. Ritorno alla lista lobby.");

		alert.showAndWait();

		InstanceData.gotoLobbyList();
	}

	/**
	 * metodo usato per aggiornare il log della lobby
	 * @param text testo da aggiungere al log
	 */
	public void updateLog(String text) {
		taLog.appendText(text + "\n");
	}

	/**
	 * metodo per chiudere la finestra della lobby
	 */
	public void closeLobbyView() {
		Platform.runLater(() -> {
			Stage window = (Stage) (lobbyScene.getWindow());
			window.close();
		
		});
	}

}