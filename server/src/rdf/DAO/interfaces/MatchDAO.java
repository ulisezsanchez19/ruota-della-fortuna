package rdf.DAO.interfaces;

import rdf.API.Match;
import rdf.API.User;

public interface MatchDAO {
		
	int insertMatch(Match match);
	
	boolean addWinner(Match match, User user);

	boolean resetMatchStatus();
	
	boolean releaseConnection();
}
