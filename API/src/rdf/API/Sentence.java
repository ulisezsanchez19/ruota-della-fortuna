package rdf.API;

import java.io.Serializable;

/**
 * Classe che modellizza le frasi
 *
 */
public class Sentence implements Serializable {
	
	public int sentence_id = -1;
	public String sentence;
	public String theme;
	
	public Sentence(int id, String sentence, String theme) {
		super();
		this.sentence_id = id;
		this.sentence = sentence;
		this.theme = theme;
	}
	
	public Sentence(String sentence, String theme) {
		super();
		this.sentence = sentence;
		this.theme = theme;
	}

}
