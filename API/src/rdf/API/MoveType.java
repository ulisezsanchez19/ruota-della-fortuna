package rdf.API;

/**
 * classe che rappresenta i tipi di mosse. 
 * ("Gira la ruota", "Dai consonante", "Compra vocale", "Dai soluzione", "Usa un jolly")
 *
 */
public enum MoveType {
	SPINTHEWHEEL("Spin the wheel"),
	GIVECONSONANT("Guess consonant"),
	BUYVOCAL("Buy vocal"),
	GIVESOLUTION("Guess solution"),
	USEJOLLY("Use jolly");
	
    public final String label;
	 
    private MoveType(String label) {
        this.label = label;
    }
}
