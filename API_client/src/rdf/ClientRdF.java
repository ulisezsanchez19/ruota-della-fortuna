package rdf;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javafx.stage.Stage;
import rdf.API.interfaces.ClientRdFInterface;
import rdf.API.interfaces.serverRdFInterface;

public class ClientRdF implements ClientRdFInterface {

	public void start(Stage primaryStage, boolean isAdmin) throws IOException {
		
		if(!startConnection())
			InstanceData.interruptClient();
		
		primaryStage.setTitle("Wheel of Fortune" + (isAdmin ? " - ADMIN" : ""));
		primaryStage.setOnCloseRequest(e -> {
			e.consume();
			if(InstanceData.user != null)
				InstanceData.returnToAuthentication(true);
			System.exit(0);
		});
		
		InstanceData.ThisclientRdF = this;
		InstanceData.isAdmin = isAdmin;	
		InstanceData.stage = primaryStage;
		primaryStage.setWidth(1280);
		primaryStage.setHeight(720);
		
		InstanceData.gotoAuthentication();
	}
	
	private boolean startConnection()
	{
		Registry registry;
		try {
			registry = LocateRegistry.getRegistry(1099);
			serverRdFInterface stub = (serverRdFInterface) registry.lookup("ServerRdF");
			
			InstanceData.stub = stub;
			
		} catch (NotBoundException | RemoteException e) {
			return false;
		}
		
		return true;
	}

	/**
	 * forza la disconnesione e il log-out del client
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void forceDisconnect() throws RemoteException {
		InstanceData.returnToAuthentication(false);
	}

	
}