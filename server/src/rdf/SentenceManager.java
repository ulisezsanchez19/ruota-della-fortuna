package rdf;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import rdf.API.Sentence;
import rdf.API.interfaces.SentenceManagerInterface;
import rdf.DAO.DAOFactory;
import rdf.DAO.interfaces.SentenceDAO;

/**
 * modulo server per la gestione delle frasi
 *
 */
public class SentenceManager extends UnicastRemoteObject implements SentenceManagerInterface {

	protected SentenceManager() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean insertSentences(List<Sentence> sentences) throws RemoteException {
		boolean ris;
		SentenceDAO sentenceDAO = (SentenceDAO) DAOFactory.getDAO("SentenceDAO");
		ris = sentenceDAO.insertSentences(sentences);
		sentenceDAO.releaseConnection();
		return ris;
	}

	/**
	 * metodo per ricevere frasi dal db in base ai filtri
	 * @param letter lettera iniziale delle frasi da ricevere
	 * @param search parametro che indica la corrispondenza da trovare
	 * @param page offset dei risultati voluti
	 * @param maxNumber limita il numero di risultati
	 * @return array delle frasi
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public Sentence[] getSentences(char letter, String search, int page, int maxNumber) throws RemoteException {		
		Sentence[] ris;
		SentenceDAO sentenceDAO = (SentenceDAO) DAOFactory.getDAO("SentenceDAO");
		ris = sentenceDAO.getSentences(letter, search, page, maxNumber);
		sentenceDAO.releaseConnection();		
		return ris;
	}

	@Override
	public boolean insertSentence(Sentence sentence) throws RemoteException {		
		boolean ris;
		SentenceDAO sentenceDAO = (SentenceDAO) DAOFactory.getDAO("SentenceDAO");
		ris = sentenceDAO.insertSentence(sentence);
		sentenceDAO.releaseConnection();
		return ris;
	}

	@Override
	public boolean updateSentence(Sentence sentence) throws RemoteException {
		boolean ris;
		SentenceDAO sentenceDAO = (SentenceDAO) DAOFactory.getDAO("SentenceDAO");
		ris = sentenceDAO.updateSentence(sentence);
		sentenceDAO.releaseConnection();
		return ris;
	}

	@Override
	public boolean deleteSentences(List<Sentence> sentences) throws RemoteException {
		boolean ris;
		SentenceDAO sentenceDAO = (SentenceDAO) DAOFactory.getDAO("SentenceDAO");
		ris = sentenceDAO.deleteSentences(sentences);
		sentenceDAO.releaseConnection();
		return ris;
	}

	/**
	 * restituisce il numero totale di frasi che corrispondono ai filtri
	 * @param letter lettera iniziale delle frasi da ricevere
	 * @param search parametro che indica la corrispondenza da trovare
	 * @return numero del totale
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public int getSentencesCount(char letter, String search) throws RemoteException {
		int ris;
		SentenceDAO sentenceDAO = (SentenceDAO) DAOFactory.getDAO("SentenceDAO");
		ris = sentenceDAO.getSentencesCount(letter, search);
		sentenceDAO.releaseConnection();
		return ris;
	}

}
