package rdf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Classe che si occupa della connessione al database Realizzata utilizzando il
 * design-patter Singleton
 *
 */
public class DatabaseConnection {

	private static DatabaseConnection instance;

	private static List<Connection> connectionPool;
	private static List<Connection> usedConnections = new ArrayList<>();
	private static int INITIAL_POOL_SIZE = 10;

	public static DatabaseConnection getDatabaseConnection() {
		if (instance == null)
			instance = new DatabaseConnection();

		return instance;
	}

	/**
	 * metodo che prepara la pool di connessioni
	 * 
	 * @param url indirizzo url del database
	 * @param username
	 * @param password
	 */
	public void InitDatabaseConnection(String url, String username, String password) throws SQLException {
		connectionPool = new ArrayList<>(INITIAL_POOL_SIZE);
		for (int i = 0; i < INITIAL_POOL_SIZE; i++)
			connectionPool.add(DriverManager.getConnection(url, username, password));
	}

	/**
	 * metodo che restituisce una connessione dalla pool di connessioni. Se la pool non ha
	 * connessioni disponibili, il thread richiedente viene messo in attesa.
	 * 
	 * @return una connessione al database
	 */
	public synchronized Connection getConnection() {

		if (connectionPool.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		Connection connection = connectionPool.remove(connectionPool.size() - 1);
		usedConnections.add(connection);
		return connection;
	}

	/**
	 * rilascia una connessione nella pool
	 * @param connection
	 * @return
	 */
	public synchronized boolean releaseConnection(Connection connection) {
		boolean ris;
		connectionPool.add(connection);
		ris = usedConnections.remove(connection);
		notify();
		return ris;
	}

	/**
	 * termina la pool, rilasciando le eventuali connessioni ancora attive
	 */
	public synchronized void close() {
		for (Connection c : usedConnections)
			releaseConnection(c);
		if(connectionPool != null) //evita eccezione quando si chiude il server senza averlo nenache avviato
		{
			for (Connection c : connectionPool)
				try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			connectionPool.clear();
		}
	}

}
