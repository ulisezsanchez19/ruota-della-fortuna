package rdf;

import java.io.Serializable;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import rdf.API.*;
import rdf.API.interfaces.GameManagerInterface;
import rdf.API.interfaces.GameMasterInterface;
import rdf.controllers.GameViewController;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * modulo client che gestisce la partita
 *
 */
public abstract class GameManager implements GameManagerInterface, Serializable {

	GameMasterInterface gameMaster;
	public int spinResult = 0;
	boolean isMyTurn = false;
	public List<String> playersNames;
	public GameViewController gameViewController;
	public int myTurn = 0;

	/**
	 * metodo che avvia la partita nei client
	 * @param gameMaster oggetto remoto del gestore della partita lato server
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void startMatch(GameMasterInterface gameMaster, List<String> playersNames) throws RemoteException {
		this.gameMaster = gameMaster;
		this.playersNames = playersNames;
		
		for(int i = 0; i < 3; i++)
		{
			String player = playersNames.get(i);
			if(player.equals(InstanceData.user.getUsername()))
			{
				myTurn = i;
				break;
			}
		}
		
		if(InstanceData.lobbyListener.lobbyViewController != null)
			InstanceData.lobbyListener.lobbyViewController.closeLobbyView();
		InstanceData.gotoGame();
	}

	/**
	 * aggiorna il tabellone delle lettere e dei punteggi
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void updateTable(Integer[] pos, char letter, int[] points) throws RemoteException {
		gameViewController.updateTable(pos, letter);
		updateScore(points);
	}

	/**
	 * aggiorna il tabellone dei punteggi
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void updateScore(int[] points) throws RemoteException {
		gameViewController.updateMancheScore(points);
	}

	/**
	 * aggiorna i jollies
	 * @param jollies array dei jollies
	 * @param order codice indicante l'utilizzo o consumo del jolly
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void updateJollies(int[] jollies, int order) throws RemoteException {
		gameViewController.updateJollies(jollies, myTurn);
		if (order >= 0)
		{
			gameViewController.updateLog(playersNames.get(order) + " used a jolly!");
			if(order == myTurn)
				gameViewController.canWheelSpin = true;
		}
	}

	/**
	 * inizio del nuovo turno
	 * @param order codice del giocatore corrente
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void newTurn(int order) throws RemoteException {
		String playerName = playersNames.get(order);
		updateLog("New Turn... it's " + playerName + "'s turn!");
		if(!InstanceData.isAdmin)
			isMyTurn = order == myTurn;

		gameViewController.inputSwitch(isMyTurn);
		gameViewController.updatePlaying(order);
	}

	/**
	 * inizio di una nuova manche
	 * @param SymbolsPositionsMap mappa contenente le associazioni carattere-posizione del tabellone iniziale
	 * @param nManche numero nuova manche
	 * @param theme tema della frase
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void newManche(HashMap<Character, Integer[]> SymbolsPositionsMap, int nManche, String theme) throws RemoteException {
		updateLog("New manche started!");
		gameViewController.initializeNewManche(SymbolsPositionsMap, theme);
		gameViewController.detailsNewManche(nManche);
	}

	/**
	 * fine del turno corrente
	 * @param oldTurn dati del turno concluso
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void endTurn(Turn oldTurn) throws RemoteException {
		gameViewController.detailsNewTurn(oldTurn);
		gameViewController.resetTab();
	}

	/**
	 * fine della manche corrente
	 * @param solution frase misteriosa
	 * @param usedLetters lettere usate
	 * @param nManche numero di manche finita
	 * @param points nuovo punteggio della partita
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void endManche(String solution, HashSet<Character> usedLetters, int nManche, int[] points)
			throws RemoteException {
		Manche completed = new Manche(new Sentence(0, solution, ""));
		Map<Character, List<Integer>> lettersPositionsMap = completed.getLettersPositionsMap();

		for (int i = 0; i < solution.length(); i++) {
			char c = solution.charAt(i);
			if (!Character.isLetter(c))
				continue;
			if (!usedLetters.contains(c)) {
				usedLetters.add(c);
				List<Integer> pos = lettersPositionsMap.get(c);
				gameViewController.updateTable(pos.toArray(new Integer[pos.size()]), c);
			}
		}

		usedLetters.clear();
		gameViewController.updateMatchScore(points);
	}

	/**
	 * aggiorna il log dei messaggi
	 * @param text testo da aggiungere
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void updateLog(String text) throws RemoteException {
		gameViewController.updateLog(text);
	}

	/**
	 * comunica il risultato della ruota
	 * @param result risultato della ruota
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void spinResult(int result) throws RemoteException {
		spinResult = result;
		updateLog("It's time to spin the wheel!");
		if (isMyTurn) {
			gameViewController.forceWheelSpin = true;
			updateLog("IT'S YOUR TURN, SPIN THE WHEEL!");
		}
	}

	/**
	 * inizializza il timer del tempo disponibile per eseguire una mossa
	 * @param t tempo
	 * @param gameSection tipo di mossa aspettata
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void setTimer(int t, int gameSection) throws RemoteException {
		if (isMyTurn) {
			switch (gameSection) {
			case 0:
				gameViewController.switchToWheelTab(null);
				gameViewController.canWheelSpin = true;
				break;
			case 1:
				gameViewController.switchToSendAnswerTab();
				gameViewController.canWheelSpin = false;
				break;
			case 2:
				gameViewController.switchToMovesTab();
				gameViewController.canWheelSpin = false;
				break;
			case 3:
				gameViewController.switchToJolliesTab();
				gameViewController.canWheelSpin = false;
				break;
			}
		}

		gameViewController.setTimerThread(t);
	}

	/**
	 * termina la partita
	 * @param error se � terminata correttamente o a causa di errori
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void endGame(boolean error) throws RemoteException {
		leaveGame(false, error);
	}

	/**
	 * metodo che avvia la procedura per abbandonare la partita in corso
	 * @param userAction se la procedura � stata avviata dall' user corrente
	 * @param error se la procedura � stata avviata a causa di errori
	 */
	public void leaveGame(boolean userAction, boolean error) {
		if (userAction && gameMaster != null) {
			try {
				gameMaster.leaveGame(this);
			} catch (RemoteException e) {
				InstanceData.interruptClient();
			}
		} else if (error) {
			Platform.runLater(() -> {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Game teminated");
				alert.setHeaderText(null);
				alert.setContentText("The match terminated in a unusual way. Returning to the home...");
				alert.showAndWait();
			});
		}
		gameViewController.forceWheelSpin = false;
		gameViewController.timerTurn++;

		try {
			UnicastRemoteObject.unexportObject(this, true);
		} catch (NoSuchObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		InstanceData.gameManager = null;
		InstanceData.gotoHome();
	}

	/**
	 * restituisce l'user del client
	 * @return user
	 * @throws RemoteException
	 */
	@Override
	public User getUser() {
		return InstanceData.user;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GameManager)) {
			return false;
		}
		GameManager other = (GameManager) o;
		return getUser().equals(other.getUser());
	}

	@Override
	public int hashCode() {
		return getUser().hashCode();
	}
}
