package rdf.DAO.interfaces;

import java.util.List;

import rdf.API.Sentence;
import rdf.API.User;

public interface SentenceDAO {
		
	boolean insertSentences(List<Sentence> sentences);
		
	Sentence[] getGameSentences(List<User> list);
	
	Sentence[] getSentences(char letter, String search, int page, int maxNumber);

	boolean insertSentence(Sentence sentence);

	boolean updateSentence(Sentence sentence);

	boolean deleteSentences(List<Sentence> sentences);

	int getSentencesCount(char letter, String search);
	
	boolean releaseConnection();

}
