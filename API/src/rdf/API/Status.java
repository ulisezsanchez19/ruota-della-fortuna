package rdf.API;

/**
 * Classe che rappresenta gli stati di una lobby. ("OPEN", "CLOSED", "PLAYNG")
 *
 */
public enum Status {
	OPEN,
	CLOSED,
	PLAYING;
}
