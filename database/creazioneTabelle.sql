DROP TABLE IF EXISTS status CASCADE;
CREATE TABLE status
(
    status_id serial NOT NULL PRIMARY KEY,
	status VARCHAR NOT NULL
);

DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users
(
    user_id serial NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL,
    surname VARCHAR NOT NULL,
    username VARCHAR NOT NULL UNIQUE,
    email VARCHAR NOT NULL UNIQUE,
    psw_hash CHAR(32) NOT NULL,
    is_admin boolean NOT NULL
);

DROP TABLE IF EXISTS game_status CASCADE;
CREATE TABLE game_status
(
    game_status_id serial NOT NULL PRIMARY KEY,
	game_status VARCHAR NOT NULL
);

DROP TABLE IF EXISTS matches CASCADE;
CREATE TABLE matches
(
    match_id serial NOT NULL PRIMARY KEY,
	name VARCHAR NOT NULL,
	time TIMESTAMP NOT NULL,
	id_winner INTEGER,
	id_game_status INTEGER NOT NULL,
	CONSTRAINT id_winner FOREIGN KEY (id_winner)
        REFERENCES users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
	CONSTRAINT id_game_status FOREIGN KEY (id_game_status)
        REFERENCES game_status (game_status_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

DROP TABLE IF EXISTS sentences CASCADE;
CREATE TABLE sentences
(
    sentence_id serial NOT NULL PRIMARY KEY,
    sentence VARCHAR NOT NULL UNIQUE,
	theme VARCHAR NOT NULL
);

DROP TABLE IF EXISTS manches CASCADE;
CREATE TABLE manches
(
    manche_id serial NOT NULL PRIMARY KEY,
    id_match INTEGER NOT NULL,
	id_sentence INTEGER,
    id_winner INTEGER,
	id_game_status INTEGER NOT NULL,
	CONSTRAINT id_match FOREIGN KEY (id_match)
        REFERENCES matches (match_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT id_sentence FOREIGN KEY (id_sentence)
        REFERENCES sentences (sentence_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE SET NULL
        NOT VALID,
	CONSTRAINT id_winner FOREIGN KEY (id_winner)
        REFERENCES users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
		NOT VALID,
	CONSTRAINT id_game_status FOREIGN KEY (id_game_status)
        REFERENCES game_status (game_status_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

DROP TABLE IF EXISTS scores CASCADE;
CREATE TABLE scores
(
    score_id serial NOT NULL PRIMARY KEY,
    points INTEGER NOT NULL,
    id_user INTEGER NOT NULL,
    id_match INTEGER,
    id_manche INTEGER,
    CONSTRAINT id_user FOREIGN KEY (id_user)
        REFERENCES users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
	CONSTRAINT id_match FOREIGN KEY (id_match)
        REFERENCES matches (match_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
	CONSTRAINT id_manche FOREIGN KEY (id_manche)
        REFERENCES manches (manche_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

DROP TABLE IF EXISTS move_types CASCADE;
CREATE TABLE move_types
(
    move_type_id serial NOT NULL PRIMARY KEY,
    move_type VARCHAR NOT NULL
);

DROP TABLE IF EXISTS moves CASCADE;
CREATE TABLE moves
(
    move_id serial NOT NULL PRIMARY KEY,
    points INTEGER NOT NULL,
    details VARCHAR NOT NULL,
	id_manche INTEGER NOT NULL,
    id_move_type INTEGER,
    id_user INTEGER NOT NULL,
	error BOOLEAN NOT NULL,
	CONSTRAINT id_manche FOREIGN KEY (id_manche)
        REFERENCES manches (manche_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT id_move_type FOREIGN KEY (id_move_type)
        REFERENCES move_types (move_type_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT id_user FOREIGN KEY (id_user)
        REFERENCES users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

DROP TABLE IF EXISTS has_seen CASCADE;
CREATE TABLE has_seen
(
    id_sentence INTEGER NOT NULL,
    id_user INTEGER NOT NULL,
    PRIMARY KEY (id_sentence, id_user),
    CONSTRAINT id_sentence FOREIGN KEY (id_sentence)
        REFERENCES sentences (sentence_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT id_user FOREIGN KEY (id_user)
        REFERENCES users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
);
		
DROP TABLE IF EXISTS has_observed CASCADE;
CREATE TABLE has_observed
(
	id_user INTEGER NOT NULL,
    id_manche INTEGER NOT NULL,    
    PRIMARY KEY (id_user, id_manche),
    CONSTRAINT id_user FOREIGN KEY (id_user)
        REFERENCES users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
	CONSTRAINT id_manche FOREIGN KEY (id_manche)
        REFERENCES manches (manche_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

DROP TABLE IF EXISTS has_played CASCADE;
CREATE TABLE has_played
(
    id_user INTEGER NOT NULL,
    id_manche INTEGER NOT NULL,
	PRIMARY KEY (id_user, id_manche),
    CONSTRAINT id_user FOREIGN KEY (id_user)
        REFERENCES users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
	CONSTRAINT id_manche FOREIGN KEY (id_manche)
        REFERENCES manches (manche_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);	

INSERT INTO move_types(move_type)
	VALUES
		('SPINTHEWHEEL'),
		('GIVECONSONANT'),
		('BUYVOCAL'),
		('GIVESOLUTION'),
		('USEJOLLY');

INSERT INTO game_status(game_status)
	VALUES
		('PLAYNG'),
		('ENDED'),
		('ERROR');