package rdf.DAO.interfaces;

import java.util.List;

import rdf.API.Manche;
import rdf.API.Match;
import rdf.API.User;

public interface ScoreDAO {

	public boolean insertMancheScoresBatch(Manche manche, List<User> list);
	
	public boolean insertMatchScoresBatch(Match match);
	
	boolean releaseConnection();
}
