package rdf;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import rdf.API.StringUtilities;
import rdf.API.User;
import rdf.API.UserDetails;
import rdf.API.interfaces.AccountManagerInterface;
import rdf.API.interfaces.ClientRdFInterface;
import rdf.DAO.DAOFactory;
import rdf.DAO.interfaces.UserDAO;

/**
 * modulo dedicato alla gestione degli account
 *
 */
public class AccountManager extends UnicastRemoteObject implements AccountManagerInterface {
	
	private HashMap<String, Object[]> tempUsersMap;
		
	protected AccountManager() throws RemoteException {
		super();

		tempUsersMap = new HashMap<>();
	}

	/**
	 * effettua il login di un utente
	 * @param client oggetto remoto del client
	 * @return oggetto dell'utente loggato se il login va a buon fine
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public User login(String email, String pswHash, boolean isAdmin, ClientRdFInterface client) throws RemoteException {
		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		User result =  userDAO.login(email, pswHash, isAdmin);
		userDAO.releaseConnection();
		if(result != null)
		{
			if(serverRdF.clients.containsKey(result.getId()))
			{
				if(serverRdF.DEBUG)
					System.out.println("User already online!");
				serverRdF.lobbyManager.forceClientDisconnection(result, true);
				if(serverRdF.clients.get(result.getId()) != null)
				{
					try {
						serverRdF.clients.get(result.getId()).forceDisconnect();
					} catch (RemoteException e) {
						if(serverRdF.DEBUG)
							System.out.println("Old session disconnected");
					}
				}
			}

			serverRdF.clients.put(result.getId(), client);
		}
		
		return result;
	}
	
	/**
	 * effettua il logout di un utente
	 * @param user utente da disconnettere
	 * @param client oggetto remoto dell'utente
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void logout(User user, ClientRdFInterface client) {
		serverRdF.lobbyManager.listeners.remove(user.getId());
		serverRdF.clients.remove(user.getId(), client);
	}

	/**
	 * avvia la procedura di registrazione di un utente
	 * @return codice di conferma e di gestione errori
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public int register(UserDetails user, String pswHash, boolean isAdmin) {
		
		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		
		if(userDAO.checkAlreadyUsedEmail(user.getEmail(), -1)) 
		{
			userDAO.releaseConnection();
			return -1;
		}
		if(userDAO.chechAlreadyUsedUsername(user.getUsername(), -1)) 
		{
			userDAO.releaseConnection();
			return -2;
		}
		if(tempUsersMap.containsKey(user.getUsername() + "@" + user.getEmail())) return -3;
		
		String verificationCode = StringUtilities.getRandomString(5);
		tempUsersMap.put(user.getUsername() + "@" + user.getEmail(), new Object[] { user, pswHash, isAdmin, verificationCode});
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			String userKey = user.getUsername() + "@" + user.getEmail();
			  @Override
			  public void run() {
				  if(serverRdF.DEBUG)
					  System.out.println("Temporary account expired: " + userKey);
				  tempUsersMap.remove(userKey);
			  }
			}, 1000 * 60 * 10);
		
		return EmailSender.sendConfirmEmail(user.getEmail(), verificationCode) ? 1 : 0;
	}
	
	/**
	 * conferma la email di un utente durante la procedura di registrazione
	 * @return codice di conferma e di gestione errori
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public int confirmRegistration(UserDetails user, String code) throws RemoteException {
		Object[] tempData = tempUsersMap.get(user.getUsername() + "@" + user.getEmail());
		
		if(tempData == null || tempData.length < 4) return 1;
		if(!((String) tempData[3]).equals(code)) return 2;		
		
		UserDetails userDetails = (UserDetails) tempData[0];
		String pswHash = (String) tempData[1];
		boolean isAdmin = (boolean) tempData[2];
		
		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		boolean tmp = userDAO.registerUser(userDetails, pswHash, isAdmin);
		userDAO.releaseConnection();
		if(tmp)
		{
			tempUsersMap.remove(user.getUsername() + "@" + user.getEmail());
			return 0;
		}
		
		return 3;
	}

	/**
	 * aggiorna il profilo di un utente
	 * @return codice di conferma e di gestione errori
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public int updateProfile(UserDetails user) {
		
		boolean ris;
		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		if(userDAO.chechAlreadyUsedUsername(user.getUsername(), user.getId())) 
		{
			userDAO.releaseConnection();
			return -2;
		}
		ris = userDAO.updateUserDetails(user);
		userDAO.releaseConnection();
		return ( ris ? 1 : 0);
	}

	/**
	 * aggiorna la password di un utente
	 * @return risultato dell'operazione
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public boolean updatePassword(User user, String oldPswHash, String newPswHash) {
		boolean ris;
		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		if(!userDAO.verifyPassword(user, oldPswHash))
		{
			userDAO.releaseConnection();
			return false;
		}
		ris = userDAO.updateUserPassword(user, newPswHash);
		return ris;
	}
	
	/**
	 * resetta la password di un utente
	 * @return risultato dell'operazione
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public boolean resetPassword(String email, boolean isAdmin) throws RemoteException {
		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		String newPassword = StringUtilities.getRandomString(10);
		if(serverRdF.DEBUG)
		{
			System.out.println("new password: " + newPassword);
			System.out.println("md5 password: " + StringUtilities.getMD5(newPassword));
		}
		boolean queryResult = userDAO.resetUserPassword(email, StringUtilities.getMD5(newPassword));
		userDAO.releaseConnection();
		if(serverRdF.DEBUG_QUERY)
			System.out.println("query result : " + queryResult);
		if(queryResult)
			EmailSender.sendRecoveryPasswordEmail(email, newPassword);
		return queryResult;
	}
	
	/**
	 * restituisce i dettagli di un utente
	 * @return l'oggetto con UserDetails contenente i dettagli
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public UserDetails getUserProfile(User user) throws RemoteException
	{
		UserDetails ris;
		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		ris = userDAO.getUserProfile(user);
		userDAO.releaseConnection();
		return ris;
	}

}
