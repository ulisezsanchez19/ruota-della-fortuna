package rdf;

import java.io.IOException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import rdf.API.LobbyData;
import rdf.API.interfaces.LobbyListenerInterface;
import rdf.API.interfaces.LobbyManagerInterface;
import rdf.API.interfaces.ObserverGameManagerInterface;
import rdf.API.interfaces.PlayerGameManagerInterface;
import rdf.controllers.LobbyListViewController;
import rdf.controllers.LobbyViewController;

/**
 * modulo client che gestisce le lobby
 *
 */
public class LobbyListener  implements LobbyListenerInterface {

	LobbyManagerInterface lobbyManager;
	LobbyListViewController lobbyListViewController;
	LobbyViewController lobbyViewController;
	
	/**
	 * costruttore della classe, ottiene il lobbyManager dal server e abilita gli aggiornamenti lobby
	 */
	public LobbyListener(LobbyListViewController lobbyListViewController) {
		
		this.lobbyListViewController = lobbyListViewController;
		try {
			lobbyManager = (LobbyManagerInterface) InstanceData.stub.getLobbyManager();
			lobbyManager.enableLobbyUpdate(InstanceData.user.getId(), (LobbyListenerInterface)UnicastRemoteObject.exportObject(this, 0));
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
	}
	
	/**
	 * metodo che restituisce la lista delle lobby
	 * @return lista lobby
	 */
	public Collection<LobbyData> getLobbiesList() {
		
		try {
			return lobbyManager.getLobbiesList().values();
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
		return null;
	}

	/**
	 * aggiorna l'elenco delle lobby
	 * @param command codice della modifica da effetuare
	 * @param newData lobby
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void updateLobbyData(Integer command, LobbyData newData) throws RemoteException {

		if(lobbyListViewController != null)
		{
			lobbyListViewController.UpdateLobbyData(command, newData);
		}
		
		if(lobbyViewController != null && newData.getNumber() == lobbyViewController.lobbyNumber)
		{
			if (command == 2)
				Platform.runLater(() -> {if(lobbyViewController != null) lobbyViewController.forceLobbyLeave();});
			else
				Platform.runLater( () -> {if(lobbyViewController != null) lobbyViewController.refreshLobby(newData);});
		}
	}
	
	/**
	 * invia messaggi alla lobby
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void sendMessage(String message) throws RemoteException {

		if(lobbyViewController != null)
		{
			lobbyViewController.updateLog(message);
		}
	}
	
	/**
	 * metodo usato per creare una lobby
	 * @param lobbyName nome lobby da creare
	 */
	public void createLobby(String lobbyName) {

		InstanceData.gameManager = new PlayerGameManager();
		PlayerGameManagerInterface stub = null;
		try {
			stub = (PlayerGameManagerInterface) UnicastRemoteObject.exportObject((PlayerGameManager) InstanceData.gameManager, 0);
		} catch (RemoteException e1) {
			InstanceData.interruptClient();
		}

		try {
			LobbyData newlobby = lobbyManager.createLobby(lobbyName, stub);
			goToLobby(newlobby);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * metodo usato per entrare in una lobby come player
	 * @param selected lobby in cui entrara
	 */
	public void joinLobbyAsPlayer(LobbyData selected) {

		InstanceData.gameManager = new PlayerGameManager();
		PlayerGameManagerInterface stub = null;
		try {
			stub = (PlayerGameManagerInterface) UnicastRemoteObject.exportObject((PlayerGameManager) InstanceData.gameManager, 0);
		} catch (RemoteException e1) {
			InstanceData.interruptClient();
		}
		try {
			LobbyData result = lobbyManager.joinLobbyAsPlayer(selected.getNumber(), stub);
			if (result != null) {
				goToLobby(result);
			}
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
	}

	/**
	 * metodo usato per entrare in una lobby come osservatore
	 * @param selected lobby in cui entrara
	 */
	public void joinLobbyAsObserver(LobbyData selected) {

		InstanceData.gameManager = new ObserverGameManager();
		ObserverGameManagerInterface stub = null;
		try {
			stub = (ObserverGameManagerInterface) UnicastRemoteObject.exportObject((ObserverGameManager) InstanceData.gameManager, 0);
		} catch (RemoteException e1) {
			InstanceData.interruptClient();
		}
		try {
			LobbyData result = lobbyManager.joinLobbyAsObserver(selected.getNumber(), stub);
			if (result != null) {
				goToLobby(result);
			}
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
	}
	
	/**
	 * metodo usato per passare alla finestra lobby
	 * @param lobby i ncui entrare
	 */
	public void goToLobby(LobbyData lobby) {

		lobbyListViewController.buttonsSwitch(false);
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/rdf.view/LobbyView.fxml"));
			Stage lobbyStage = new Stage();
			Scene lobbyScene = new Scene(loader.load());
			lobbyStage.setScene(lobbyScene);
			lobbyViewController = loader.getController();
			lobbyViewController.initialize(lobbyManager, lobby, lobbyScene);
			lobbyViewController.refreshLobby(lobby);
			lobbyStage.initOwner(InstanceData.stage);
			lobbyStage.initModality(Modality.NONE);
			lobbyStage.initStyle(StageStyle.TRANSPARENT);
			// lobbyStage.setOnCloseRequest(e->e.consume());
			lobbyStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * metodo usato per uscire una lobby
	 */
	public void leaveLobby() {
		lobbyListViewController.buttonsSwitch(true);
		if(lobbyViewController == null) return;
		try {
			if (InstanceData.gameManager instanceof PlayerGameManager)
				lobbyManager.leaveLobbyAsPlayer(lobbyViewController.lobbyNumber, InstanceData.user.getId());
			else if (InstanceData.gameManager instanceof ObserverGameManager)
				lobbyManager.leaveLobbyAsObserver(lobbyViewController.lobbyNumber, InstanceData.user.getId());			
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
		if(InstanceData.gameManager != null)
		try {
			UnicastRemoteObject.unexportObject(InstanceData.gameManager, true);
		} catch (NoSuchObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lobbyViewController = null;
	}
	
}
