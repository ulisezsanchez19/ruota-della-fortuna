package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import rdf.API.User;

/**
 * interfaccia (usata dal client) del modulo per la gestione della statistiche
 *
 */
public interface StatisticsManagerInterface extends Remote{

	/**
	 * restituisce le statistiche relative a tutte le partite
	 * @return statistiche
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public List<String[]> getAllGamesStatics() throws RemoteException;
	
	/**
	 * restituisce le statistiche del singolo giocatore
	 * @param user giocatore
	 * @return statistiche
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public String[] getUserStatics(User user) throws RemoteException;

	/**
	 * carica altri risultati per una statistica
	 * @param statistic la statistica
	 * @param rows limita i risultati
	 * @return statistiche
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	String[] loadMoreResult(int statistic, int rows) throws RemoteException;

	/**
	 * restituisce le statistiche della piattaforma
	 * @return statistiche
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	String[] getRdFStatistics() throws RemoteException;
}
