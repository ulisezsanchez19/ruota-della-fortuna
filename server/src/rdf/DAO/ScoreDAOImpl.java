package rdf.DAO;

import java.util.List;

import rdf.API.Manche;
import rdf.API.Match;
import rdf.API.User;
import rdf.DAO.interfaces.ScoreDAO;

/**
 * Oggetto DAO che si occupa delle query riguardanti i punteggi.
 * Fa utilizzo della classe GenericDAO.
 *
 */
class ScoreDAOImpl extends GenericDAOImpl implements ScoreDAO {

	@Override
	public boolean insertMancheScoresBatch(Manche manche, List<User> users) {
		
		String[] names = {"scores", "points", "id_user", "id_manche"};
		Object[] objects = new Object[9];
		int o = 0;
		for(int i = 0; i < 3; i++)
		{
			objects[o++] = manche.points[i];
			objects[o++] = users.get(i).getId();
			objects[o++] = manche.manche_id;
		}
		
		return insertBatch(names, objects, false);
	}

	@Override
	public boolean insertMatchScoresBatch(Match match) {
		
		String[] names = {"scores", "points", "id_user", "id_match"};
		Object[] objects = new Object[9];
		int o = 0;
		for(int i = 0; i < 3; i++)
		{
			objects[o++] = match.getScores()[i];
			objects[o++] = match.getPlayersList().get(i).getId();
			objects[o++] = match.match_id;
		}
		
		return insertBatch(names, objects, false);
	}

}
