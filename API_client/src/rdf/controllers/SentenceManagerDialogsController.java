package rdf.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rdf.API.Sentence;
import rdf.API.StringUtilities;

/**
 * controller del pop-up della finestra sentenceManager
 *
 */
public class SentenceManagerDialogsController {

    @FXML
    private Text titleDialog;

    @FXML
    private TextField tfDialogTheme;

    @FXML
    private TextArea taDialogSentence;

    @FXML
    private Label lbDialogSuccess;

    @FXML
    private Button btnDialog;

    private Scene scene;
    private SentenceManagerViewController sentenceManagerViewController;
    private Sentence sentence = null;
    private String mex;
    
    /**
     * metodo che inizializza il pop-up in base al tipo inserito come parametro
     * @param scene
     * @param sentenceManagerViewController
     * @param typeDialog tipo di pop-up
     * @param sentence frase da utilizzare nel pop-up
     */
    public void initialize(Scene scene, SentenceManagerViewController sentenceManagerViewController, boolean typeDialog, Sentence sentence)
    {
    	this.scene = scene;
    	this.sentence = sentence;
    	this.sentenceManagerViewController = sentenceManagerViewController;
    	Platform.runLater(() -> {
    	if(typeDialog) {
			titleDialog.setText("Add new sentence");
			btnDialog.setText("Add");
			setActionButton(typeDialog);
		}
		else {
			titleDialog.setText("Modify sentence");
		    btnDialog.setText("Modify");
		    setActionButton(typeDialog);
				tfDialogTheme.setText(sentence.theme);
				taDialogSentence.setText(sentence.sentence);
		    
		}
    	});
    	
    	lbDialogSuccess.setStyle("-fx-text-fill:#ff5131");
    }
	
	/**
	 * metodo per chiudere il pop-up
	 */
	@FXML
	public void closeDialog(MouseEvent event) {
		Platform.runLater(() -> {
			Stage window = (Stage)scene.getWindow();
			  window.close();
		});
	}
	
	/**
	 * Metodo per aggiungere una nuova frase
	 */
	public void addSentenceToDB() {
		if(!sentenceChecker()) return;
		
		Sentence sentence = new Sentence(taDialogSentence.getText(), tfDialogTheme.getText());
		int result = sentenceManagerViewController.addSentence(sentence);
		
		if(result != 1)
		{
			mex = result == 0 ? "Frase gi� esistente" : "Frase non caricate";
			Platform.runLater(() -> {
				 lbDialogSuccess.setText(mex);
			    });
			return;
		}
		closeDialog(null);
	}
	
	/**
	 * metodo per caricare la frase selezionata
	 */
	public void updateSelected() {
		if(!sentenceChecker()) return;
		
		int id = sentence.sentence_id;
		Sentence sentence = new Sentence(id, taDialogSentence.getText(), tfDialogTheme.getText());
		sentenceManagerViewController.updateSentence(sentence);
		
		closeDialog(null);
	}
	
	/**
	 * metodo per controllare la correttezza della frase
	 * @return boleanea di conferma
	 */
	private boolean sentenceChecker() {
		
		boolean check = false;
		mex = "";
		
		if(tfDialogTheme.getText().isEmpty() || taDialogSentence.getText().isEmpty())
			mex = "I campi sono obbligatori";
		else if(taDialogSentence.getText().length() > 60)
			mex = "La frase non pu� superare i 60 caratteri";
		else if(StringUtilities.splitSentenceString(taDialogSentence.getText()) == null)
			mex = "La frase inserita non pu� essere spezzata correttamente";
		else
			check = true;
		
		 Platform.runLater(() -> {
			 lbDialogSuccess.setText(mex);
		    });
		
		return check;
	}
	
	
	/**
	 * Settare l'azione del bottone in base al pop-up creato 
	 * @param typeDialog tipo di pop-up
	 */
	private void setActionButton(boolean typeDialog){
		btnDialog.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				if(typeDialog)
					addSentenceToDB();
				else
					updateSelected();			
			}
		}); 
		
	}


	
}
