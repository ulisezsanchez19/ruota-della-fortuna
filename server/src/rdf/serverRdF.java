package rdf;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Scanner;

import rdf.API.StringUtilities;
import rdf.API.UserDetails;
import rdf.API.interfaces.AccountManagerInterface;
import rdf.API.interfaces.ClientRdFInterface;
import rdf.API.interfaces.LobbyManagerInterface;
import rdf.API.interfaces.SentenceManagerInterface;
import rdf.API.interfaces.StatisticsManagerInterface;
import rdf.API.interfaces.serverRdFInterface;
import rdf.DAO.DAOFactory;
import rdf.DAO.interfaces.MancheDAO;
import rdf.DAO.interfaces.MatchDAO;
import rdf.DAO.interfaces.UserDAO;

public class serverRdF extends UnicastRemoteObject implements serverRdFInterface {

	private static final long serialVersionUID = 1L;
	static AccountManager accountManager;
	static LobbyManager lobbyManager;
	static SentenceManager sentenceManager;
	static StatisticsManager statisticsManager;
	static HashMap<Integer, ClientRdFInterface> clients;
	static Registry registry;
	static boolean connected = false;
	public static boolean DEBUG = false;
	public static boolean DEBUG_QUERY = false;

	protected serverRdF() throws RemoteException {
		super();

	}

	public static void main(String args[]) throws IOException {

		System.out.println(" __        __ _   _  _____  _____  _        ___   _____ \r\n"
				+ " \\ \\      / /| | | || ____|| ____|| |      / _ \\ |  ___|\r\n"
				+ "  \\ \\ /\\ / / | |_| ||  _|  |  _|  | |     | | | || |_   \r\n"
				+ "   \\ V  V /  |  _  || |___ | |___ | |___  | |_| ||  _|  \r\n"
				+ "    \\_/\\_/   |_| |_||_____||_____||_____|  \\___/ |_|    \r\n"
				+ "      _____  ___   ____  _____  _   _  _   _  _____     \r\n"
				+ "     |  ___|/ _ \\ |  _ \\|_   _|| | | || \\ | || ____|    \r\n"
				+ "     | |_  | | | || |_) | | |  | | | ||  \\| ||  _|      \r\n"
				+ "     |  _| | |_| ||  _ <  | |  | |_| || |\\  || |___     \r\n"
				+ "     |_|    \\___/ |_| \\_\\ |_|   \\___/ |_| \\_||_____|    \r\n"
				+ "                                                        \r\n");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (args == null || args.length != 3) {
			System.out.println(
					"Server launch error. To succesfully launch, insert database's data as parameters (<URL> <username> <password>)");
			System.exit(0);
		} else {
			try {
				// jdbc:postgresql://localhost:5432/rdf?&rewriteBatchedStatements=true postgres
				// ruota
				DatabaseConnection.getDatabaseConnection().InitDatabaseConnection(args[0], args[1], args[2]);
			} catch (SQLException e) {
				System.out.println("Error! Incorrect database's data. Relaunch server and try again\n(Details: "
						+ e.getMessage() + ")");
				System.exit(0);
			}
		}

		accountManager = new AccountManager();
		lobbyManager = new LobbyManager();
		sentenceManager = new SentenceManager();
		statisticsManager = new StatisticsManager();
		clients = new HashMap<>();
		DAOFactory.setDatabaseType("PostgreSQL");

		Scanner in = new Scanner(System.in);
		showMenu(in);
		in.close();
		DatabaseConnection.getDatabaseConnection().close();
		System.exit(0);
	}

	private static void showMenu(Scanner in) {
		boolean close = false;
		while (!close) {
			System.out.println("\n\n");
			System.out.println("==== RdF Server ====");
			System.out.println("status: " + (connected ? "connected" : "not connected"));
			System.out.println("\nAvariable options:");
			System.out.println("1 - Enter SystemAccount details.");
			System.out.println("2 - Register a new Administrator Account.");
			System.out.println("3 - Open server connection.");
			System.out.println("4 - Exit.");
			System.out.println("Insert a number to choose an option...");

			switch (in.nextLine()) {
			case "1":
				readSystemAccounts(in);
				break;
			case "2":
				registerNewAdmin(in);
				break;
			case "3":
				openServerConnection(in);
				break;
			case "4":
				if (registry != null)
					closeServerConnection(in);
				System.out.println("System closing...");
				close = true;
				break;
			default:
				System.out.println("The number inserted is not an option.");
			}
		}
	}

	private static void readSystemAccounts(Scanner in) {
		
		
		String email = "";
		boolean correct = false;
		
		while (!correct){
			System.out.println("Enter SystemAccount email:");
			email = in.nextLine();
			if(StringUtilities.checkEmail(email))
				correct = true;
			else
				System.out.println("The inserted email is not valid.");
		}

		System.out.println("Enter SystemAccount password:");
		String password = in.nextLine();
		EmailSender.accountSystem = email;
		EmailSender.passwordSystem = password;
		System.out.println("SystemAccount details added successfully.");
	}

	private static void registerNewAdmin(Scanner in) {

		if (EmailSender.accountSystem == null || EmailSender.passwordSystem == null) {
			System.out.println("\nThe server need a SystemAccount to register an Admin.");
			return;
		}

		boolean close = false;
		String[] fields = new String[] { "", "", "", "", "" };
		while (!close) {
			String hiddenPassword = "";
			if (!fields[4].equals(""))
				for (int i = 0; i < fields[4].length(); i++)
					hiddenPassword += "*";
			System.out.println("\n\n");
			System.out.println("==== Admin Registration ====");
			System.out.println("1 - name: " + fields[0]);
			System.out.println("2 - surname: " + fields[1]);
			System.out.println("3 - username: " + fields[2]);
			System.out.println("4 - email: " + fields[3]);
			System.out.println("5 - password: " + hiddenPassword);
			System.out.println("6 - Confirm registration.");
			System.out.println("7 - Exit.");
			System.out.println("Select the field to modify...");

			String field = in.nextLine();
			int selected = -1;
			try {
				selected = Integer.parseInt(field);
			} catch (NumberFormatException e) {
			}

			System.out.println("\n\n");
			if (selected > 0 && selected < 6) {
				System.out.println("Insert the desired value...");
				String input = in.nextLine();
				if (selected == 4 && !StringUtilities.checkEmail(input))
					System.out.println("The inserted email is invalid.");
				else if (selected == 5 && !StringUtilities.checkPassword(input))
					System.out.println("The inserted password is too weak.");
				else
					fields[selected - 1] = input;
			} else if (selected == 6 && confirmRegistration(in, fields))
				close = true;
			else if (selected == 7)
				close = true;
			else
				System.out.println("The number inserted is not an field.");
		}
	}

	private static boolean confirmRegistration(Scanner in, String[] fields) {

		UserDetails user = null;
		if (fields[0].equals("") || fields[1].equals("") || fields[2].equals("") || fields[3].equals("")
				|| fields[4].equals("")) {
			System.out.println("You must fill in all the fields.");
			return false;
		} else {
			System.out.println("\n\nExecuting...\n\n");
			user = new UserDetails(0, fields[2], fields[3], fields[0], fields[1]);
			int result = accountManager.register(user, StringUtilities.getMD5(fields[4]), true);
			switch (result) {
			case 1:
				System.out.println("Confirmation code sent.");
				break;
			case 0:
				System.out.println("ERROR!");
				return false;
			case -1:
				System.out.println("Email already used!");
				return false;
			case -2:
				System.out.println("Username already used!");
				return false;
			case -3:
				System.out.println("Confirmation code already sent!");
				break;
			}
		}

		System.out.println("Insert the confirmation code");
		String code = in.nextLine();
		int result = 3;
		try {
			result = accountManager.confirmRegistration(user, code);
		} catch (RemoteException e) {
		}

		if (result == 0)
			System.out.println("Registration complete!");
		else {
			switch (result) {
			case 1:
				System.out.println("Account not found!");
				return false;
			case 2:
				System.out.println("Wrong code!");
				return false;
			case 3:
				System.out.println("ERROR!");
				return false;
			}
		}
		return true;
	}

	private static void openServerConnection(Scanner in) {
		if (connected) {
			System.out.println("\nServer already connected.");
			return;
		}

		if (EmailSender.accountSystem == null || EmailSender.passwordSystem == null) {
			System.out.println("\nThe server need a SystemAccount to connect.");
			return;
		}

		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		int count = userDAO.getAdminCount();
		userDAO.releaseConnection();
		if (count == 0) {
			System.out.println("\nThe server need an Administator account to connect.");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}

		System.out.println("\n\nExecuting...\n\n");
		try {
			serverRdFInterface obj = new serverRdF();
			resetStatus();
			registry = LocateRegistry.createRegistry(1099);
			registry.rebind("ServerRdF", obj);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Open server connection: Failed!");
			return;
		}

		System.out.println("Open server connection: Completed!");
		connected = true;
	}

	private static void closeServerConnection(Scanner in) {
		System.out.println("\n\nExecuting...\n\n");
		try {
			registry.unbind("ServerRdF");
		} catch (RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Close server connection: Failed!");
			return;
		}

		System.out.println("Close server connection: Completed!");
	}

	private static void resetStatus() {

		MatchDAO matchDAO = (MatchDAO) DAOFactory.getDAO("MatchDAO");
		matchDAO.resetMatchStatus();
		matchDAO.releaseConnection();

		MancheDAO mancheDAO = (MancheDAO) DAOFactory.getDAO("MancheDAO");
		mancheDAO.resetMancheStatus();
		mancheDAO.releaseConnection();
	}

	@Override
	public AccountManagerInterface getAccountManager() throws RemoteException {
		return accountManager;
	}

	@Override
	public LobbyManagerInterface getLobbyManager() throws RemoteException {
		return lobbyManager;
	}

	@Override
	public SentenceManagerInterface getSentenceManager() throws RemoteException {
		return sentenceManager;
	}

	@Override
	public StatisticsManagerInterface getStatisticsManager() throws RemoteException {
		return statisticsManager;
	}

}
