package rdf.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import rdf.API.User;
import rdf.API.UserDetails;
import rdf.serverRdF;
import rdf.API.Manche;
import rdf.API.Sentence;
import rdf.DAO.interfaces.UserDAO;
/**
 * Oggetto DAO che si occupa delle query riguardanti l'utente.
 * Fa utilizzo della classe GenericDAO.
 *
 */
class UserDAOImpl extends GenericDAOImpl implements UserDAO {
		
	public UserDAOImpl()
	{
		super();
	}

	@Override
	public User login(String email, String pswHash, boolean isAdmin) {
		
		if(connection == null) return null;
		
		try {
			PreparedStatement statement = null;
			String sql = "SELECT * FROM users WHERE email = ? AND psw_hash = ? AND is_admin = ?";
			
			statement = connection.prepareStatement(sql);
			statement.setString(1,  email);
			statement.setString(2,  pswHash);
			statement.setBoolean(3, isAdmin);
			
			ResultSet result = statement.executeQuery();
			User user = null;
			while (result.next()) {
				user =  new User(
						result.getInt("user_id"),
						result.getString("username")
						);
			}
			return user;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean registerUser(UserDetails user, String pswHash, boolean isAdmin) {
		
		String[] names = new String[] {"users", "name", "surname", "username", "email", "psw_hash", "is_admin"};
		Object[] objects = new Object[] {user.getName(), user.getSurname(), user.getUsername(), user.getEmail(), pswHash, isAdmin};
		
		return (insert(names, objects, "") == -1 ? false : true);
	}
	
	@Override
	public UserDetails getUserProfile(User user) {
		
		if(connection == null) return null;
		
		try {
			PreparedStatement statement = null;
			String sql = "SELECT * FROM users WHERE user_id = ?";
			
			statement = connection.prepareStatement(sql);
			statement.setInt(1,  user.getId());
			
			ResultSet result = statement.executeQuery();
			UserDetails userDetails = null;
			while (result.next()) {
				userDetails =  new UserDetails(
						result.getInt("user_id"),
						result.getString("username"),
						result.getString("email"),
						result.getString("name"),
						result.getString("surname")
						);
			}
			return userDetails;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public int getAdminCount() {
		
		if(connection == null) return -1;
		
		try {
			Statement statement = null;
			String sql = "SELECT COUNT(*) AS count FROM users WHERE is_admin = TRUE";
			
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			
			int countAdmin = -1;
			while (result.next()) {
				countAdmin =  result.getInt("count");
			}
			
			return countAdmin;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	@Override
	public boolean updateUserDetails(UserDetails user) {
		
		String[] names = new String[] {"users", "name", "surname", "username", "user_id"};
		Object[] objects = new Object[] {user.getName(), user.getSurname(), user.getUsername(), user.getId()};
		
		return update(names, objects);
	}
	
	@Override
	public boolean updateUserPassword(User user, String pswHash) {

		String[] names = new String[] {"users", "psw_hash", "user_id"};
		Object[] objects = new Object[] {pswHash, user.getId()};

		return update(names, objects);
	}
	
	@Override
	public boolean resetUserPassword(String email, String pswHash) {

		String[] names = new String[] {"users", "psw_hash", "email"};
		Object[] objects = new Object[] {pswHash, email};

		return update(names, objects);
	}

	@Override
	public boolean userHasSeenASentencBatch(List<User> usersList, Sentence sentence) {

		String[] names = {"has_seen", "id_user", "id_sentence"};
		
		return userManyToManyBatch(usersList, sentence.sentence_id, names, true);
	}

	@Override
	public boolean UserHasPlayedAMancheBatch(List<User> usersList, Manche manche) {

		String[] names = {"has_played", "id_user", "id_manche"};
		
		return userManyToManyBatch(usersList, manche.manche_id, names, false);
	}

	@Override
	public boolean userHasObservedAMancheBatch(List<User> usersList, Manche manche) {

		String[] names = {"has_observed", "id_user", "id_manche"};
		
		return userManyToManyBatch(usersList, manche.manche_id, names, false);
	}

	private boolean userManyToManyBatch(List<User> usersList, int id_other, String[] names, boolean ignoreConflict)
	{
		Object[] objects = new Object[usersList.size() * 2];
		int i = 0;
		for(User user : usersList)
		{
			objects[i++] = user.getId();
			objects[i++] = id_other;
		}
				
		return insertBatch(names, objects, ignoreConflict);
	}
	
	@Override
	public boolean verifyPassword(User user, String pswHash)
	{
		return checkUserColumn(new String[] {"user_id = '" + user.getId() + "'", "psw_hash = '" + pswHash + "'"});
	}
	
	@Override
	public boolean checkAlreadyUsedEmail(String email, int user_id) {
		
		if(user_id == -1)
			return checkUserColumn(new String[] {"email = '" + email + "'"});
		else
			return checkUserColumn(new String[] {"email = '" + email + "'", "user_id != '" + user_id + "'"});
	}
	
	@Override
	public boolean chechAlreadyUsedUsername(String username , int user_id) {
		
		if(user_id == -1)
			return checkUserColumn(new String[] {"username = '" + username + "'"});
		else
			return checkUserColumn(new String[] {"username = '" + username + "'", "user_id != '" + user_id + "'"});
	}
	
	private boolean checkUserColumn(String[] variables) {
		if(connection == null) return false;
		
		try {
			Statement statement = connection.createStatement();
			String sql = "SELECT 1 AS result FROM Users WHERE ";
			for(int i = 0; i < variables.length; i++)
			{
				sql += variables[i];
				if(i < variables.length - 1)
					sql += " AND ";
			}
			
			if(serverRdF.DEBUG_QUERY)
				System.out.println(sql);
			ResultSet resultSet = statement.executeQuery(sql);
			
			int result = -1;
			while (resultSet.next()) {
				result = resultSet.getInt("result");
			};
			
			return result == 1 ? true : false;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
}
