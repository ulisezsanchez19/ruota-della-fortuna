package rdf.DAO.interfaces;

import java.util.List;

import rdf.API.Manche;
import rdf.API.Sentence;
import rdf.API.User;
import rdf.API.UserDetails;

public interface UserDAO {
	
	boolean registerUser(UserDetails user, String pswHash, boolean isAdmin);
	
	User login(String username, String pswHash, boolean isAdmin);
	
	boolean userHasSeenASentencBatch(List<User> usersList, Sentence sentence);
	
	boolean UserHasPlayedAMancheBatch(List<User> usersList, Manche manche);
	
	boolean userHasObservedAMancheBatch(List<User> usersList, Manche manche);

	boolean updateUserDetails(UserDetails user);

	boolean updateUserPassword(User user, String pswHash);

	UserDetails getUserProfile(User user);
	
	boolean verifyPassword(User user, String pswHash);

	boolean resetUserPassword(String email, String pswHash);

	boolean checkAlreadyUsedEmail(String email, int user_id);

	boolean chechAlreadyUsedUsername(String username, int user_id);
	
	boolean releaseConnection();

	int getAdminCount();

}
