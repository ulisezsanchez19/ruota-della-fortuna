package rdf.API.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * interfaccia (usata dal client) del modulo server che gestisce la partita
 *
 */
public interface GameMasterInterface  extends Remote {
	
	/**
	 * metodo di selezione mossa da eseguire
	 * @param move mossa da eseguire
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void selectMove(int move) throws RemoteException;
	
	/**
	 * metodo per dare una lettera o la soluzione
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void giveAnswer(String answer) throws RemoteException;
	
	/**
	 * metodo per abbandonare il gioco
	 * @param gameManager oggetto remoto del modulo gameManager del client che abbandona la partita
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	public void leaveGame(GameManagerInterface gameManager) throws RemoteException;
		
}
