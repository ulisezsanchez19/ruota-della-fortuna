package rdf.controllers;

import java.rmi.RemoteException;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.VBox;
import rdf.InstanceData;
import rdf.API.StringUtilities;
import rdf.API.UserDetails;
import rdf.API.interfaces.AccountManagerInterface;

/**
 * controller della finestra del profilo
 *
 */
public class ProfileViewController {

    @FXML
    private ToggleButton tgbtnEdit;

    @FXML
    private TextField tfName;

    @FXML
    private TextField tfSurname;

    @FXML
    private TextField tfUsername;

    @FXML
    private TextField tfEmail;

    @FXML
    private VBox vboxAdvancedEdit;

    @FXML
    private Label lblDetailsLog;

    @FXML
    private PasswordField pfCurrentPassword;

    @FXML
    private PasswordField pfNewPassword;

    @FXML
    private Label lblPasswordLog;

    UserDetails userDetails;  
    
    private boolean editable = true;
 
    /**
     * metodo usato per inizializzare la finestra prendendo i dati dal server
     */
    public void initialize()
    {
    	try {
			AccountManagerInterface accountManager = InstanceData.stub.getAccountManager();
			userDetails = accountManager.getUserProfile(InstanceData.user);
			
			setFields();
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
    	
    	editSwitch(null);
    }   
    
    /**
     * metodo che setta i dati nei campi
     */
    private void setFields()
    {
    	Platform.runLater( () -> 
		{
			tfName.setText(userDetails.getName());
			tfSurname.setText(userDetails.getSurname());
			tfUsername.setText(userDetails.getUsername());
			tfEmail.setText(userDetails.getEmail());
		});
    }
    
    /**
     * metodo usato per abilitare o disattivare la modifica dei campi
     */
    @FXML
    void editSwitch(ActionEvent event) {
    	editable = !editable;
    	vboxAdvancedEdit.setDisable(!editable);
    	vboxAdvancedEdit.setVisible(editable);
    	tfName.setEditable(editable);
		tfSurname.setEditable(editable);
		tfUsername.setEditable(editable);
		if(!editable)setFields();
    }

    /**
     * metodo usato per salvare le modifiche apportate ai campi
     */
    @FXML
    void updateDetails(ActionEvent event) {
    	
		try {
			UserDetails user = new UserDetails(userDetails.getId(), tfUsername.getText(), userDetails.getEmail(), tfName.getText(), tfSurname.getText());
			AccountManagerInterface accountManager = InstanceData.stub.getAccountManager();
			int result = accountManager.updateProfile(user);
			
			if(result == -2)
				lblDetailsLog.setText("Username already used");
			else if(result == 1)
				lblDetailsLog.setText("Profile data updated");
			if(result == 0)
				lblDetailsLog.setText("ERROR");
			
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
    }

    /**
     * metodo usato per aggiornare la password
     */
    @FXML
    void updatePassword(ActionEvent event) {
    	try {
    		
    		String newPass = pfNewPassword.getText();
    		if(StringUtilities.checkPassword(newPass))
    		{
    			AccountManagerInterface accountManager = InstanceData.stub.getAccountManager();
    			boolean result = accountManager.updatePassword(InstanceData.user, StringUtilities.getMD5(pfCurrentPassword.getText()), StringUtilities.getMD5(pfNewPassword.getText()));
			
				lblPasswordLog.setText(result ? "Password Updated" : "ERROR");
    		}
    		else
    			lblPasswordLog.setText("Error: Password to weak");
			
		} catch (RemoteException e) {
			InstanceData.interruptClient();
		}
    }
    
    /**
     * metodo usato per tornare alla home
     */
    @FXML
	void gotoHome(ActionEvent event) {
		InstanceData.gotoHome();
	}
}
