package rdf.controllers;

import java.rmi.RemoteException;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import rdf.InstanceData;
import rdf.PlayerGameManager;
import rdf.API.*;

/**
 * controller della finestra di gioco
 *
 */
public class GameViewController {

	static final int INITIAL_WHEEL_STEPS = 95; // ((NspicchiRuota - 1) * N) + N-1

	@FXML
	private GridPane gpTable;

	@FXML
	private TextArea taLog;

	@FXML
	private GridPane gpScoreboard;

	@FXML
	private Label lblTheme;
	
	@FXML
	private Label lblPlayer1;

	@FXML
	private Label lblPlayer2;

	@FXML
	private Label lblPlayer3;

	@FXML
	private ProgressBar pbTimer;

	@FXML
	private GridPane gpJollies;

	@FXML
	private TabPane tabPanel;

	@FXML
	private ImageView ivRuota;

	@FXML
	private ToggleButton tbtWheel;

	@FXML
	private TextField tfAnswer;

	@FXML
	private Button btSelectWheel;

	@FXML
	private Button btSend;

	@FXML
	private Button btJollyYes;

	@FXML
	private Button btJollyNo;

	@FXML
	private Label lblCurrentJollies;

	@FXML
	private Button btWheelTab;
	
    @FXML
    private ImageView ivPlaying1;

    @FXML
    private ImageView ivPlaying2;

    @FXML
    private ImageView ivPlaying3;

	@FXML
	private TreeView<String> treeView;

	private TreeItem<String> tiManche;

	private Label[] lblPlayers;
	Label[] scoreboardCells;
	Label[] jolliesCells;
	Label[] tableCells;
	static DoubleProperty timerUpdater = new SimpleDoubleProperty(.0);
	public int timerTurn = 0;
	public boolean forceWheelSpin = false;
	public boolean canWheelSpin = false;

	/**
	 * metodo per inizializzare la cronologia della partita
	 */
	public void initializeDetails() {
		TreeItem<String> root = new TreeItem<>("Match\t\t\t    Points\t     Details");
		root.setExpanded(true);
		treeView.setRoot(root);
		treeView.setStyle("-fx-font-family: Consolas;");
		lblPlayers = new Label[] { lblPlayer1, lblPlayer2, lblPlayer3 };

	}

	/**
	 * metodo per aggiungere una nuova manche alla cronologia della partita
	 * @param nManche numero della manche
	 */
	public void detailsNewManche(int nManche) {
		TreeItem<String> root = treeView.getRoot();
		root.getChildren().add(new TreeItem<>("Manche " + nManche));
		tiManche = root.getChildren().get(nManche);
		tiManche.setExpanded(true);
	}

	/**
	 * metodo per aggiungere un nuovo turno alla cronologia della partita
	 * @param turn il turno da aggiungere
	 */
	public void detailsNewTurn(Turn turn) {
		TreeItem<String> turnItem = new TreeItem<>("Turn: " + turn.player.getUsername());
		for (Move move : turn.moves) {

			if (move.getMoveType() == null)
				continue;

			String item = move.getMoveType().label;
			String blank = new String(new char[20 - item.length()]).replace('\0', ' ');
			item += blank;

			String temp = "";
			if (move.getMoveType() == MoveType.SPINTHEWHEEL && move.getDetails().equals("LOSE"))
				temp = move.getPoints().toString();
			else if (move.getMoveType() == MoveType.GIVECONSONANT || move.getMoveType() == MoveType.BUYVOCAL)
				temp = move.getPoints().toString();

			blank = new String(new char[7 - temp.length()]).replace('\0', ' ');
			temp = blank + temp;

			blank = new String(new char[20 - temp.length()]).replace('\0', ' ');
			item += temp + blank + move.getDetails();

			TreeItem<String> moveItem = new TreeItem<String>(item);
			turnItem.getChildren().add(moveItem);
			moveItem.setExpanded(true);
		}

		tiManche.getChildren().add(turnItem);
		turnItem.setExpanded(true);
	}

	/**
	 * metodo per inizializzare una nuova partita
	 */
	public void initializeNewGame() {
		initializeScoreboard();
		InitializeTable();
		initializeJollies();
		initializeDetails();
		updateImage(0);
		tfAnswer.setTextFormatter(new TextFormatter<>((change) -> {
			change.setText(change.getText().toUpperCase());
			return change;
		}));

		for (int i = 0; i < 3; i++)
			lblPlayers[i].setText(InstanceData.gameManager.playersNames.get(i));

		pbTimer.progressProperty().bind(timerUpdater);
	}

	/**
	 * metodo per inizializzare il tabellone segnapunti
	 */
	public void initializeScoreboard() {
		scoreboardCells = new Label[6];
		int pos = 0;
		for (int i = 0; i < 3; i++) {
			for (int a = 0; a < 2; a++) {
				Label l = new Label("0");
				l.setOpacity(100);
				l.setAlignment(Pos.CENTER);
				l.setBorder(
						new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
				l.setTextFill(Color.WHITE);
				scoreboardCells[pos] = l;
				scoreboardCells[pos].setMaxWidth(Double.MAX_VALUE);
				scoreboardCells[pos].setMaxHeight(Double.MAX_VALUE);
				gpScoreboard.add(scoreboardCells[pos], a, i);
				GridPane.setHalignment(scoreboardCells[pos], HPos.CENTER);
				GridPane.setValignment(scoreboardCells[pos], VPos.CENTER);
				GridPane.setFillWidth(scoreboardCells[pos], true);
				GridPane.setFillHeight(scoreboardCells[pos], true);

				pos += 1;
			}
		}
	}

	/**
	 * metodo per inizializzare il tabellone dei jolly
	 */
	public void initializeJollies() {
		jolliesCells = new Label[3];
		int pos = 0;
		for (int i = 0; i < 3; i++) {
			Label l = new Label("0");
			l.setOpacity(100);
			l.setAlignment(Pos.CENTER);
			l.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));
			l.setTextFill(Color.WHITE);
			jolliesCells[pos] = l;
			jolliesCells[pos].setMaxWidth(Double.MAX_VALUE);
			jolliesCells[pos].setMaxHeight(Double.MAX_VALUE);
			gpJollies.add(jolliesCells[pos], 0, i);
			GridPane.setHalignment(jolliesCells[pos], HPos.CENTER);
			GridPane.setValignment(jolliesCells[pos], VPos.CENTER);
			GridPane.setFillWidth(jolliesCells[pos], true);
			GridPane.setFillHeight(jolliesCells[pos], true);

			pos += 1;
		}
	}

	/**
	 * metodo per inizializzare il tabellone della frase
	 */
	public void InitializeTable() {
		tableCells = new Label[60];
		int pos = 0;
		for (int i = 0; i < 4; i++) {
			for (int a = 0; a < 15; a++) {
				Label l = new Label();
				l.setOpacity(100);
				l.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
				l.setFont(new Font(30));
				l.setAlignment(Pos.CENTER);
				tableCells[pos] = l;
				tableCells[pos].setMaxWidth(40);
				tableCells[pos].setMaxHeight(70);
				gpTable.add(tableCells[pos], a, i);
				GridPane.setHalignment(tableCells[pos], HPos.CENTER);
				GridPane.setValignment(tableCells[pos], VPos.CENTER);
				GridPane.setFillWidth(tableCells[pos], true);
				GridPane.setFillHeight(tableCells[pos], true);

				pos += 1;
			}
		}
	}

	/**
	 * metodo per inizializzare una nuova manche
	 * @param SymbolsPositionsMap posizioni delle lettere sul tabellone iniziale
	 * @param theme thema della frase della manche
	 */
	public void initializeNewManche(HashMap<Character, Integer[]> SymbolsPositionsMap, String theme) {
		Platform.runLater(() -> {
			lblTheme.setText(theme);
			for (int i = 0; i < 60; i++) {
				
				Image image = new Image(GameViewController.class.getResource("/rdf.images/table_cell_hidden.png").toExternalForm());

				BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
				BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
				Background background = new Background(backgroundImage);
				
				tableCells[i].setBackground(background);
				tableCells[i].setText("");
				tableCells[i].setOpacity(100);
			}

			Image image = new Image(GameViewController.class.getResource("/rdf.images/table_cell.png").toExternalForm());	
			BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
			BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
			Background background = new Background(backgroundImage);
			SymbolsPositionsMap.forEach((k, v) -> {
				for (int i = 0; i < v.length; i++) {
					if (k != '*')
					{
						tableCells[v[i]].setText(k.toString());
						tableCells[v[i]].setBackground(background);
					}
					else
						tableCells[v[i]].setOpacity(0);
				}
			});
		});

		// tfAnswer.requestFocus();
	}

	/**
	 * metodo che aggiorna la grafica relativa al giocatore in possesso del turno corrente 
	 * @param player player in possesso del turno
	 */
	public void updatePlaying(int player) {
	    ivPlaying1.setVisible(false);
	    ivPlaying2.setVisible(false);
	    ivPlaying3.setVisible(false);
	    switch(player)
	    {
	    case 0:
	    	ivPlaying1.setVisible(true);
	    	break;
	    case 1:
	    	ivPlaying2.setVisible(true);
	    	break;
	    case 2:
	    	ivPlaying3.setVisible(true);
	    	break;
	    }
	}
	
	/**
	 * metodo per aggironare il tabellone della frase
	 * @param pos posizioni in cui inserire la lettera
	 * @param letter lettera da inserire
	 */
	public void updateTable(Integer[] pos, char letter) {
		Platform.runLater(() -> {
			
				String path = "/rdf.images/table_cell.png";
				Image image = new Image(GameViewController.class.getResource(path).toExternalForm());	

				BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
				BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
				Background background = new Background(backgroundImage);
				
			for (int i = 0; i < pos.length; i++) {
				tableCells[pos[i]].setText("" + letter);
				tableCells[pos[i]].setBackground(background);
			}
		});
	}

	/**
	 * metodo per aggiornare il tabellone dei punti della manche
	 * @param points array contenente il nuovo punteggio
	 */
	public void updateMancheScore(int[] points) {
		Platform.runLater(() -> {
			for (int i = 0; i < 6; i += 2) {
				scoreboardCells[i].setText("" + points[i / 2]);
			}
		});
	}

	/**
	 * metodo per aggiornare il tabellone dei punti della partita
	 * @param points array contenente il nuovo punteggio
	 */
	public void updateMatchScore(int[] points) {
		Platform.runLater(() -> {
			for (int i = 1; i < 6; i += 2) {
				scoreboardCells[i].setText("" + points[i / 2]);
			}
		});
		int[] zero = { 0, 0, 0 };
		updateMancheScore(zero);
	}

	/**
	 * metodo per far partire il thread che gira la ruota
	 */
	@FXML
	public void spinTheWheel(ActionEvent event) {
		tbtWheel.setDisable(true);
		forceWheelSpin = false;
		timerTurn++;
		spinWheelThread(InstanceData.gameManager.spinResult + INITIAL_WHEEL_STEPS);
	}

	/**
	 * metodo per eseguire una mossa
	 */
	@FXML
	void selectMoveButton(ActionEvent event) {
		if (forceWheelSpin)
			return;
		
		Button bt = (Button) event.getSource();
		((PlayerGameManager) InstanceData.gameManager).selectMove(bt.getId());
	}

	/**
	 * metodo per inviare una risposta
	 */
	@FXML
	void sendAnswerButton(ActionEvent event) {
		String answer = tfAnswer.getText();
		if (answer.length() > 0) {
			((PlayerGameManager) InstanceData.gameManager).sendAnswer(answer);
			tfAnswer.clear();
		}
	}

	/**
	 * metodo per lasciare la partita
	 * @throws RemoteException lanciata in caso di errore nella connessioe col server
	 */
	@FXML
	void leaveGameButton(ActionEvent event) throws RemoteException {
		InstanceData.gameManager.leaveGame(true, false);
	}

	/**
	 * metodo per aggiornare il tabellone dei jolly
	 * @param jollies array contenente i nuovo valori del jolly
	 */
	public void updateJollies(int[] jollies, int myTurn) {
		
		Platform.runLater(() -> {
			for (int i = 0; i < 3; i++)
				jolliesCells[i].setText("" + jollies[i]);
			lblCurrentJollies.setText(jollies[myTurn] + "");
		});
	}

	/**
	 * metodo per aggiornare il log della partita
	 * @param text messaggio da aggiungere al log
	 */
	public void updateLog(String text) {
		Platform.runLater(() -> {
			
			String lines[] = text.split("\\r?\\n");
			for(int i = 0; i < lines.length; i++)
			{	
				String s = lines[i];
				if(i == 0)
					taLog.appendText((java.time.LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))) + "\t" + s + "\n");
				else
					taLog.appendText("\t\t\t" + s + "\n");
			}
		});
	}

	/**
	 * metodo per abilitare o meno i vari bottoni
	 * @param value booleana col nuovo valore dei bottoni
	 */
	public void inputSwitch(boolean value) {
		btSend.setDisable(!value);
		ivRuota.setDisable(!value);
		tbtWheel.setDisable(!value);
		canWheelSpin = value;
	}

	/**
	 * metodo per settare il timer
	 * @param t tempo da impostare
	 */
	public void setTimerThread(int t) {
		timerTurn++;
		new Thread(new Runnable() {
			int turn = timerTurn;

			public void run() {
				int ms = forceWheelSpin ? t / 3 : t;
				for (int i = 0; i <= 100; i++) {
					timerUpdater.set(i * 0.01);
					try {
						Thread.sleep(ms / 100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (turn != timerTurn) {
						timerUpdater.set(0);
						return;
					}
				}
				if (forceWheelSpin) {
					tbtWheel.setDisable(true);
					spinWheelThread(InstanceData.gameManager.spinResult + INITIAL_WHEEL_STEPS);
					forceWheelSpin = false;
				}
			}
		}).start();
	}

	/**
	 * metodo per girare la ruota
	 * @param steps passi da far compiere alla ruota
	 */
	public void spinWheelThread(int steps) {
		new Thread(new Runnable() {
			public void run() {
				int section = steps / 20;

				for (int i = 0; i <= steps + 1; i++) {
					if (!canWheelSpin)
						return;

					updateImage(i);

					int t = 50;
					if (i > steps - section)
						t = 400;
					else if (i > steps - section * 2)
						t = 350;
					else if (i > steps - section * 3)
						t = 300;
					else if (i > steps - section * 4)
						t = 250;
					else if (i > steps - section * 5)
						t = 150;
					else if (i > steps - section * 6)
						t = 100;

					try {
						Thread.sleep(t);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (InstanceData.gameManager != null)
					((PlayerGameManager) InstanceData.gameManager).selectMove("tbtWheel");
				tbtWheel.setDisable(false);
			}
		}).start();
	}

	/**
	 * metodo per aggiornare la grafica della ruota
	 * @param i indice della nuova immagine
	 */
	public void updateImage(int i) {
		int newImage = (i % 24);
		String path = "/rdf.images/" + String.format("%02d", newImage) + ".png";
		Image image = new Image(GameViewController.class.getResource(path).toExternalForm());			
			
		ivRuota.setImage(image);
	}

	@FXML
	public void switchToWheelTab(ActionEvent event) {
		tabPanel.getSelectionModel().select(0);
	}
	
	public void switchToMovesTab() {
		tabPanel.getSelectionModel().select(1);
	}
	
	@FXML
	public void switchToDetailsTab(ActionEvent event) {
		tabPanel.getSelectionModel().select(2);
	}	
	
	public void switchToSendAnswerTab() {
		
		tfAnswer.clear();
		tabPanel.getSelectionModel().select(3);
	}
	
	public void switchToJolliesTab() {
		tabPanel.getSelectionModel().select(4);
	}
	
	public void resetTab()
	{
		int index = tabPanel.getSelectionModel().getSelectedIndex();
		if(index != 0 && index != 2)
			switchToWheelTab(null);
	}

}
