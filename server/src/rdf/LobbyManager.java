package rdf;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import rdf.API.LobbyData;
import rdf.API.Match;
import rdf.API.Sentence;
import rdf.API.Status;
import rdf.API.User;
import rdf.API.interfaces.GameManagerInterface;
import rdf.API.interfaces.LobbyListenerInterface;
import rdf.API.interfaces.LobbyManagerInterface;
import rdf.API.interfaces.ObserverGameManagerInterface;
import rdf.API.interfaces.PlayerGameManagerInterface;
import rdf.DAO.DAOFactory;
import rdf.DAO.interfaces.MatchDAO;
import rdf.DAO.interfaces.SentenceDAO;

/**
 * modulo che gestisce le lobby
 *
 */
public class LobbyManager extends UnicastRemoteObject implements LobbyManagerInterface {

	Map<Integer, LobbyListenerInterface> listeners; // oggetti remoti dei client interessati agli aggiornamenti della lista delle lobby
	Map<Integer, PlayerGameManagerInterface> playersMap; // oggetti remoti dei client iscritti in una lobby come giocatori
	Map<Integer, ObserverGameManagerInterface> observersMap; // oggetti remoti dei client iscritti in una lobby come osservatori
	Map<Integer, LobbyData> lobbiesMap; // mappa di tutte le lobby
	Map<Integer, GameMaster> gamesMap; // mappa di tutti moduli gameMaster in eseguzione
	int lobbyNumber = 0; // numero seriale delle lobby a livello locale
	
	/**
	 * costruttore del modulo lobby manager. Inizializza le mappe
	 * @throws RemoteException lanciata in caso di errori con l'oggetto remoto
	 */
	public LobbyManager() throws RemoteException {
		super();
		listeners = new ConcurrentHashMap<>();
		playersMap = new ConcurrentHashMap<>();
		observersMap = new ConcurrentHashMap<>();
		lobbiesMap = new ConcurrentHashMap<>();
		gamesMap = new ConcurrentHashMap<>();
	}

	/**
	 * restituisce l'elenco delle lobby presenti
	 * @return lista lobby
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public Map<Integer, LobbyData> getLobbiesList() throws RemoteException {
		return lobbiesMap;
	}
	
	/**
	 * avvia la procudura per creare una lobby
	 * @param name nome lobby
	 * @param player oggetto remoto del player che crea la lobby
	 * @return la lobby, se viene creata
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public synchronized LobbyData createLobby(String name, PlayerGameManagerInterface player) throws RemoteException {

		List<User> playersList = new ArrayList<>();
		playersList.add(player.getUser());
		LobbyData newLobby = new LobbyData(lobbyNumber, name, Status.OPEN, LocalDateTime.now(), playersList, new ArrayList<User>());
		lobbiesMap.put(lobbyNumber, newLobby);
		sendUpdateToListeners(1, newLobby);
		playersMap.put(player.getUser().getId(), player);
		
		return newLobby;
	}
	
	/**
	 * iscrive il client alla ricezione degli aggiornamenti delle lobby
	 * @param userId user da iscrivere
	 * @param listener oggetto remoto del modulo lobbyListener del client
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void enableLobbyUpdate(int userId, LobbyListenerInterface listener) throws RemoteException {
		listeners.put(userId, listener);
	}

	/**
	 * disiscrive il client alla ricezione degli aggiornamenti delle lobby
	 * @param userId user da disiscivere
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public void disableLobbyUpdate(int userId) throws RemoteException {
		listeners.remove(userId);
	}
	
	/**
	 * avvia la procedura per entrare in una lobby come giocatore
	 * @param lobbyNumber numero della lobby in cui si vuole entrare
	 * @param player oggetto remoto del modulo GameManager del client
	 * @return la lobby in cui si � entrati, se l'operazione � andata a buon fine
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public LobbyData joinLobbyAsPlayer(Integer lobbyNumber, PlayerGameManagerInterface player) throws RemoteException {

		LobbyData lobby =  lobbiesMap.get(lobbyNumber);
		
		if(!lobby.addPlayer(player.getUser())) return null;
		
		sendUpdateToListeners(0, lobby);		
		playersMap.put(player.getUser().getId(), player);
		
		sendMessageToLobby(lobby, player.getUser().getUsername() + " � entrato come giocatore.");
		if(lobby.getStatus() == Status.CLOSED)
		{
			sendMessageToLobby(lobby, "Partita al completo.\nLa partit� inizier� fra:");
			
			new Thread(new Runnable() {
				public void run() {
					for (int i = 0; i < 5; i++) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(lobby.getStatus() == Status.CLOSED)
							sendMessageToLobby(lobby, "" + (5 - i));
						else
							return;
					}
					startGame(lobbyNumber);
				}
			}).start();
		}
		return lobby;
	}

	/**
	 * avvia la procedura per entrare in una lobby come osservatore
	 * @param lobbyNumber numero della lobby in cui si vuole entrare
	 * @param observer oggetto remoto del modulo GameManager del client
	 * @return la lobby in cui si � entrati, se l'operazione � andata a buon fine
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public synchronized LobbyData joinLobbyAsObserver(Integer lobbyNumber, ObserverGameManagerInterface observer) throws RemoteException {
		LobbyData lobby =  lobbiesMap.get(lobbyNumber);
		
		lobby.addObvserver(observer.getUser());
		sendUpdateToListeners(0, lobby);
		
		observersMap.put(observer.getUser().getId(), observer);
		if(lobby.getStatus().equals(Status.PLAYING))
		{
			GameMaster gm = gamesMap.get(lobbyNumber);
			listeners.remove(observer.getUser().getId());
			gm.addObserver(observer);
			return null;
		}
		else
		{
			sendMessageToLobby(lobby, observer.getUser().getUsername() + " � entrato come osservatore.");
			return lobby;
		}	
	}

	/**
	 * avvia la procedura per uscire da una lobby nella quale si � giocatore
	 * @param lobbyNumber numero della lobby dalla quale si vuole uscire
	 * @param player oggetto remoto del modulo GameManager del client
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public synchronized void leaveLobbyAsPlayer(Integer lobbyNumber, Integer player) throws RemoteException {
		LobbyData lobby =  lobbiesMap.get(lobbyNumber);
		User user = playersMap.get(player).getUser();
		lobby.removePlayer(user);
		playersMap.remove(player);
		sendMessageToLobby(lobby, user.getUsername() + " ha lasciato la lobby.");
		if(lobby.getPlayersNumber() == 0)
		{
			List<User> observers = lobby.getObserversList();
			for(User s : observers)
				observersMap.remove(s.getId());
			lobbiesMap.remove(lobbyNumber);
			sendUpdateToListeners(-1, lobby);
		}
		else
			sendUpdateToListeners(0, lobby);
	}

	/**
	 * avvia la procedura per uscire da una lobby nella quale si � osservatore
	 * @param lobbyNumber numero della lobby dalla quale si vuole uscire
	 * @param observer oggetto remoto del modulo GameManager del client
	 * @throws RemoteException lanciata nel caso di interruzione di connesione tra client e server
	 */
	@Override
	public synchronized void leaveLobbyAsObserver(Integer lobbyNumber, Integer observer) throws RemoteException {
		LobbyData lobby =  lobbiesMap.get(lobbyNumber);
		User user = observersMap.get(observer).getUser();
		lobby.removeObserver(user);
		sendMessageToLobby(lobby, user.getUsername() + " ha lasciato la lobby");
		observersMap.remove(observer);
		sendUpdateToListeners(0, lobby);
	}
	
	/**
	 * metodo per comunicare gli aggiornamenti delli lobby ai client iscritti
	 * @param command tipo di aggiornamento
	 * @param newLobby lobby sul quale effettuare l'aggiornamento
	 */
	private void sendUpdateToListeners(Integer command, LobbyData newLobby)
	{
		List<Integer> deadListeners = new ArrayList<>();
		
		listeners.forEach( (k,v) -> {
			try { 
				v.updateLobbyData(command, newLobby); }
			catch (RemoteException e)
				{ System.out.println("listener dead");
				deadListeners.add(k); }}
			);
		
		for(Integer i : deadListeners)
			listeners.remove(i);
	}
	
	/**
	 * metodo per mandare messaggi ai client all'interno di una lobby
	 * @param lobby
	 * @param message
	 */
	private void sendMessageToLobby(LobbyData lobby, String message)
	{
		List<Integer> idList = lobby.getObserversIDs();
		idList.addAll(lobby.getPlayersIDs());
				
		for(Integer i : idList)
		{
			try {
				LobbyListenerInterface listener = listeners.get(i);
				if(listener != null) 
					listener.sendMessage(message);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * metodo usato per estrarre tutti i GameManager dei client in una lobby
	 * @param lobby
	 * @return lista GameManager
	 */
	private List<GameManagerInterface> getGameManagersFromLobby(LobbyData lobby)
	{
		if(lobby.getPlayersList().size() < 3) return null;
		
		// creazione lista players (PlayerInterface)
		PlayerGameManagerInterface[] players = new PlayerGameManagerInterface[3];
		for(int i = 0; i < 3; i++)
			players[i] = playersMap.get(lobby.getPlayersIDs().get(i));
		
		// creazione lista observers (ObserverInterface)
		List<ObserverGameManagerInterface> observers = new ArrayList<>();
		for(Integer i : lobby.getObserversIDs())
			observers.add(observersMap.get(i));
		
		// creazione lista dei completa dei partecipanti e avvio del gioco
		List<GameManagerInterface> gameManagers = new ArrayList<>(observers);
		gameManagers.addAll(Arrays.asList(players));
		 
		return gameManagers;
	}

	/**
	 * metodo che avvia una partita di una lobby piena
	 * @param lobbyNumber lobby della partita
	 */
	protected synchronized void startGame(Integer lobbyNumber)
	{		
		LobbyData lobby = lobbiesMap.get(lobbyNumber);
		if(lobby.getStatus() == Status.OPEN)
			return;
		
		SentenceDAO sentenceDAO = (SentenceDAO) DAOFactory.getDAO("SentenceDAO");
		Sentence[] sentences = sentenceDAO.getGameSentences(lobby.getPlayersList());
		
		Match match = new Match(lobby);
		MatchDAO matchDAO = (MatchDAO) DAOFactory.getDAO("MatchDAO");
		int id = matchDAO.insertMatch(match);
		match.match_id = id;
		
		if(sentences == null || id == -1)
		{			
			sendMessageToLobby(lobby, (id == -1 ? "Errore caricamento match nel db" : "Non ci sono abbastanza frasi per i giocatori correntemente nella lobby. Cambiare giocatori."));
			return;
		}
		
		List<GameManagerInterface> gameManagers = getGameManagersFromLobby(lobby);
		if(gameManagers == null) return;
		
		GameMaster gameMaster = new GameMaster(match, gameManagers, sentences, this);
		
		// rimozione dei partecipanti dalla lista di client in attesa di aggiornamenti lobby
		for(GameManagerInterface g : gameManagers)
		{
			try {
				listeners.remove((Integer) g.getUser().getId());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		lobby.start();
		gamesMap.put(lobbyNumber, gameMaster);
		sendUpdateToListeners(0, lobby);
	}

	/**
	 * metodo che rimuove i dati di una partita conclusa
	 * @param gameMaster
	 */
	protected synchronized void gameEnded(GameMaster gameMaster)
	{
		if(gameMaster.error)
			System.out.println("Match error");
		else
			System.out.println("Match ended");
		
		LobbyData lobby = lobbiesMap.get(gameMaster.match.getNumber());
		for(Integer i : lobby.getPlayersIDs())
			playersMap.remove(i);
		for(Integer i : lobby.getObserversIDs())
			observersMap.remove(i);
			
		sendUpdateToListeners(-1, lobby);
		gamesMap.remove(lobby.getNumber());
		lobbiesMap.remove(lobby.getNumber());
	}
	
	/**
	 * metodo usato per forzare la disconnesione di un client
	 * @param user 
	 * @param disconnectFromGameMaster indica se l'user va rimosso da una partita in corso o meno
	 */
	public synchronized void forceClientDisconnection(User user, boolean disconnectFromGameMaster){
		
		listeners.remove(user.getId());
			
		if(disconnectFromGameMaster)
		{
			GameManagerInterface client = playersMap.get(user.getId());
			if(client == null)
				client = observersMap.get(user.getId());
			
			if(client == null) return;
			try {
			for(GameMaster gm : gamesMap.values())
				if(gm.listeners.contains(client))
					
						gm.leaveGame(client);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		playersMap.remove(user.getId());
		observersMap.remove(user.getId());	
	}
}








