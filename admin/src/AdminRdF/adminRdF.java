package AdminRdF;

import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import rdf.ClientRdF;

public class adminRdF extends Application {
		
	ClientRdF main;

	@Override
	public void start(Stage primaryStage) throws IOException {
		
		ClientRdF main = new ClientRdF();
		main.start(primaryStage, true);
		
	}

	public static void main(String[] args) {
		launch(args);
	}
	
}