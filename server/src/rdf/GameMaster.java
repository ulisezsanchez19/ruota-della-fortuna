package rdf;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import rdf.API.*;
import rdf.API.interfaces.*;
import rdf.DAO.DAOFactory;
import rdf.DAO.interfaces.MancheDAO;
import rdf.DAO.interfaces.MatchDAO;
import rdf.DAO.interfaces.MoveDAO;
import rdf.DAO.interfaces.ScoreDAO;
import rdf.DAO.interfaces.UserDAO;

/**
 * modulo che gestisce la partita
 *
 */
public class GameMaster extends Thread implements GameMasterInterface {

	public Match match;
	Manche manche;
	Turn turn;
	List<GameManagerInterface> listeners; // oggetti remoti dei player e degli observer in partita
	int playerOrder; // indica che giocatore ha il turno corrente

	int move; // indica la mossa scelta dal giocatore
	String answer = ""; // l'input del giocatore (vocale, consonate, frase)
	boolean waiting = false; // usata per il controllo sulla ricezione degli input da parte degli utenti
	Sentence[] sentences;
	public boolean error = false; // indica se durante la partita si verifica un errore
	LobbyManager lobbyManager;
	HashSet<Character> usedLetters;

	/**
	 * Costruttore del modulo che inizializza gli attributi e fa partire la partita
	 * su tutti i client interessati
	 * 
	 * @param match        la partita che deve gestire
	 * @param listeners    tutti i client interessati
	 * @param sentences    le frasi della partita
	 * @param lobbyManager il modulo di gestione lobby
	 */
	public GameMaster(Match match, List<GameManagerInterface> listeners, Sentence[] sentences,
			LobbyManager lobbyManager) {

		this.match = match;
		this.listeners = listeners;
		this.sentences = sentences;
		this.lobbyManager = lobbyManager;
		playerOrder = ThreadLocalRandom.current().nextInt(0, 3);

		GameMasterInterface stub = null;
		try {
			stub = (GameMasterInterface) UnicastRemoteObject.exportObject(this, 0);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		callMethodOnAllListeners("startMatch", new Object[] { stub, match.getPlayersNames() });

		start();
	}

	/**
	 * metodo usato per aggiungere un osservatore a partita in corso.
	 * All'osservatore viene avviata la partita e gli vengono inviati tutti i dati
	 * di quest'ultima
	 * 
	 * @param observer da aggiungere
	 */
	public void addObserver(ObserverGameManagerInterface observer) {
		try {
			observer.startMatch(this, match.getPlayersNames());
			observer.updateLog("Sei entrato in una partita in corso.");

			Map<Character, List<Integer>> lettersPositionsMap = manche.getLettersPositionsMap();
			Map<Character, List<Integer>> letters = new HashMap<>();
			for (Character c : usedLetters)
				letters.put(c, lettersPositionsMap.get(c));

			Manche[] manches = new Manche[5];
			for (int i = 0; i < 5; i++) {
				if (match.manches[i] != null) {
					manches[i] = new Manche();
					manches[i].turnsList = match.manches[i].turnsList;
				} else
					break;
			}
			HashMap<Character, Integer[]> symbolsPositionsMap = new HashMap<>();
			symbolsPositionsMap.put('*', manche.getLetter(2, '*'));
			for (Character c : manche.objSentence.sentence.toCharArray()) {
				if (!Character.isLetter(c) && c != ' ')
					symbolsPositionsMap.put(c, manche.getLetter(2, c));
			}
			observer.sendGameData(symbolsPositionsMap, manche.objSentence.theme, letters, match.jollies, manche.points, match.getScores(),
					manches);
			listeners.add(observer);

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * metodo di servizio, utilizzato per richiamare un metodo su tutti gli oggetti
	 * remoti dei listener. Nel la connesione con un listener risultasse assente,
	 * questo viene rimosso dalla partita. Nel caso il listener rimosso sia un
	 * giocatore, viene avviata la procedura per terminare la partita.
	 * 
	 * @param methodName nome del metodo da chiamare
	 * @param parameters parametri del metodo da chiamare
	 */
	private void callMethodOnAllListeners(String methodName, Object[] parameters) {

		if (error)
			return;

		Method method = null;
		Method[] methods = GameManagerInterface.class.getMethods();

		for (Method m : methods) {
			if (m.getName().equals(methodName)) {
				method = m;
				break;
			}
		}

		if (method == null)
			return;

		List<Integer> deadListeners = new ArrayList<>();
		for (int i = 0; i < listeners.size(); i++) {
			GameManagerInterface listener = listeners.get(i);
			try {
				method.invoke(listener, parameters);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				if (listener instanceof PlayerGameManagerInterface) {
					if(serverRdF.DEBUG)
						System.out.println("player dead");
					listeners.remove(i);
					callMethodOnAllListeners("endGame", new Object[] { true });
					error = true;
					return;
				}
				if(serverRdF.DEBUG)
					System.out.println("observer dead");
				deadListeners.add(i);
			}
		}
		for (int i = deadListeners.size() - 1; i >= 0; i++)
			listeners.remove(deadListeners.get(i).intValue());
	}

	/**
	 * metodo principale del modulo. Esegue le 5 manche, seleziona il vincitore e
	 * termina la partita
	 */
	public void run() {
		// ciclo delle 5 manche
		for (int i = 0; i < 5; i++) {

			// inizializza le variabili della manche
			match.manches[i] = new Manche(sentences[i]);
			if(serverRdF.DEBUG)
				System.out.println("SENTENCE: " + sentences[i].sentence);
			MancheDAO mancheDAO = (MancheDAO) DAOFactory.getDAO("MancheDAO");
			int manche_id = mancheDAO.insertManche(match, sentences[i]);
			mancheDAO.releaseConnection();
			manche = match.manches[i];
			manche.manche_id = manche_id;
			usedLetters = new HashSet<>();

			// crea il tabellone della frase
			HashMap<Character, Integer[]> symbolsPositionsMap = new HashMap<>();
			symbolsPositionsMap.put('*', manche.getLetter(2, '*'));
			for (Character c : sentences[i].sentence.toCharArray()) {
				if (!Character.isLetter(c) && c != ' ')
					symbolsPositionsMap.put(c, manche.getLetter(2, c));
			}

			// inizia la manche sui client e gli passa il tabellone da usare
			callMethodOnAllListeners("newManche", new Object[] { symbolsPositionsMap, i, sentences[i].theme});

			playManche();

			// termina la manche sui client, passandogli i dati necessari
			callMethodOnAllListeners("endManche",
					new Object[] { sentences[i].sentence, usedLetters, i, match.getScores() });

			// salva la manche sul DB
			saveDataOnDB();
			startTimer(10000, 0);
			if (error)
				break;
		}

		// se la partita si � conclusa senza errori, viene calcolato il vincitore
		// e la partita viene salvata sul DB
		if (!error) {
			int winner = winnerSelectionSection();
			startTimer(10000, 4);
			callMethodOnAllListeners("endGame", new Object[] { false });

			MatchDAO matchDAO = (MatchDAO) DAOFactory.getDAO("MatchDAO");
			matchDAO.addWinner(match, match.getPlayersList().get(winner));
			matchDAO.releaseConnection();
			ScoreDAO scoreDAO = (ScoreDAO) DAOFactory.getDAO("ScoreDAO");
			scoreDAO.insertMatchScoresBatch(match);
			scoreDAO.releaseConnection();
		}
		lobbyManager.gameEnded(this);
	}

	/**
	 *  Metodo che esegue l'intera manche
	 */
	public void playManche() {

		boolean mancheEnded = false;
		while (!mancheEnded && !error) {

			turn = new Turn(match.getPlayersList().get(playerOrder));
			manche.turnsList.add(turn);
			callMethodOnAllListeners("newTurn", new Object[] { playerOrder });

			mancheEnded = playTurn();

			playerOrder = (playerOrder + 1) % 3;
			callMethodOnAllListeners("endTurn", new Object[] { turn });
		}
	}

	/**
	 * metodo che esegue un turno
	 * @return booleana che indica se il giocatore ha vinto o meno la manche
	 */
	public boolean playTurn() {

		int turnResult = 1; // 0 = passa; 1 = rigioca; 2 = vinto manche
		while (turnResult == 1 && !error) {
			if (manche.consonantsRemaining()) {
				int spinResult = spinTheWheelSection();

				// -1 = PERDE; -2 = JOLLY; -3 = PASSA
				if (spinResult == -1)
					return false;
				else if (spinResult == -2)
					continue;
				else if (spinResult == -3)
					turnResult = 0;
				else if(!giveConsonantSection(spinResult))
					turnResult = 0;
			}

			if (turnResult == 1) {
				turnResult = askMoveSection();
				if (turnResult == 2)
					return true;
			}

			if (turnResult == 0 && askJolly()) {
				turnResult = 1;
			}
		}

		return false;
	}

	/**
	 * esegue la sezione dove si gira la ruota
	 * @return il risultato della ruota
	 */
	public int spinTheWheelSection() {
		// calcolo random del risultato della ruota
		int steps = ThreadLocalRandom.current().nextInt(0, 24);
		int result = Wheel.values[steps];

		if(serverRdF.DEBUG)
			System.out.println("Result: " + result);

		// invio del risultato ai player
		callMethodOnAllListeners("spinResult", new Object[] { steps });

		// attendere che la ruota del player finisca di girare
		startTimer(30000, 0);

		String details = "" + result;
		int points = 0;

		// se result < 0, si ha ottenuto o PERDE o JOLLY o PASSA
		if (result == -1) {
			points = -manche.points[playerOrder];
			manche.points[playerOrder] = 0;
		} else if (result == -2)
			match.jollies[playerOrder]++;

		String text = "The wheel ha selected... ";

		if (result == -1) {
			callMethodOnAllListeners("updateScore", new Object[] { manche.points });
			text += "LOSE, you lost your manche's points!";
			details = "LOSE";
		} else if (result == -2) {
			callMethodOnAllListeners("updateJollies", new Object[] { match.jollies, -1 });
			text += "JOLLY, you earned a jolly!";
			details = "JOLLY";
		} else if (result == -3) {
			text += "PASS, you lost your turn!";
			details = "PASS";
		} else {
			text += result + "!";
		}

		callMethodOnAllListeners("updateLog", new Object[] { text });

		Move move = new Move(MoveType.SPINTHEWHEEL, points, details, false);
		turn.moves.add(move);
		try {
			sleep(3000);
		} catch (InterruptedException e) {}

		return result;
	}

	/**
	 * metodo che esegue la sezione della scelta di una consonante
	 * @param result il risultato della ruota
	 * @return indica se la sezione � andata a buon fine o meno
	 */
	public boolean giveConsonantSection(int result) {

		callMethodOnAllListeners("updateLog",
				new Object[] { "When the timer start, you'll have 5 seconds to guess a consonant!" });
		try {
			sleep(3000);
		} catch (InterruptedException e) {}

		// attendere che il player dia una consonate: MAX 5s
		answer = "";
		Move move = null;
		boolean turnResult = true;
		if (!startTimer(5000, 1) || answer.length() != 1) {
			callMethodOnAllListeners("updateLog", new Object[] { "You haven't selected any consonant! Turn ended." });
			turnResult = false;
			move = new Move(MoveType.GIVECONSONANT, 0, "Nothing", true);
		} else {
			String mex = "";
			char letter = answer.charAt(0);
			Integer[] pos = null;

			if (usedLetters.contains(letter))
				mex = "This letter has already been selected! Turn ended.";
			else {
				pos = manche.getLetter(0, letter);

				if (pos == null) {
					turnResult = false;
					mex += "Wrong answer! Turn ended.";
				} else {
					usedLetters.add(letter);
					manche.points[playerOrder] += result * pos.length;
					mex = "Right answer! the letter \"" + letter + "\" appears " + pos.length
							+ " times!\nYou've earned " + result * pos.length + " points!";
					callMethodOnAllListeners("updateTable", new Object[] { pos, letter, manche.points });
				}

				mex = "You chose the letter: " + letter + "\n" + mex;
				mex += manche.consonantsRemaining() ? ""
						: "\n There are no more consonants! From now on, there will be no more wheel spin!";
			}
			callMethodOnAllListeners("updateLog", new Object[] { mex });

			if (pos == null)
				move = new Move(MoveType.GIVECONSONANT, 0, answer, true);
			else
				move = new Move(MoveType.GIVECONSONANT, result * pos.length, answer, false);
		}
		turn.moves.add(move);

		try {
			sleep(3000);
		} catch (InterruptedException e) {}

		return turnResult;
	}

	/**
	 * esegue la sezione in cui i giocatori scelgono la mossa da eseguire
	 * @return il risultato della sezione (0 = passa; 1 = rigioca; 2 = vinto manche)
	 */
	public int askMoveSection() {

		callMethodOnAllListeners("updateLog",
				new Object[] { "When the timer start, you'll have 5 seconds to select your next move!" });
		try {
			sleep(3000);
		} catch (InterruptedException e) {}

		int turnResult = 1;
		while (turnResult == 1 && !error) {
			// il player deve scegliere una mossa: MAX 5s
			String mex = "Choose between:" + (manche.consonantsRemaining() ? "\n- Spin the wheel" : "")
					+ "\n- Guess the solution\n- Buy a vocal (1000p)";
			callMethodOnAllListeners("updateLog", new Object[] { mex });

			move = -1;
			if (!startTimer(5000, 2)) {
				turnResult = 0;
				turn.moves.add(new Move(null, 0, "Null move", true));
			} else {
				switch (move) {
				case 0:
					mex = "You chose to spin the wheel!";
					if (!manche.consonantsRemaining()) {
						mex += "\nYou can't spin the wheel now!";
						callMethodOnAllListeners("updateLog", new Object[] { mex });
						turnResult = 0;
						turn.moves.add(new Move(null, 0, "Null move", true));
					} else {
						callMethodOnAllListeners("updateLog", new Object[] { mex });
						return 1;
					}
					break;
				case 1:// return true se indovina la vocale, false se abaglia o non ha punti abbastanza o non
						// risponde
					turnResult = buyVocalSection() ? 1 : 0;
					break;
				case 2:// return true se indovina la soluzione, false se la sbaglia o non risponde
					turnResult = giveSolutionSection() ? 2: 0;
					break;
				case 3:
					turnResult = 0;
					turn.moves.add(new Move(null, 0, "Null move", true));
					break;
				}
			}
		}

		return turnResult;
	}

	/**
	 * esegue la sezione in cui il giocatore compra una vocale
	 * @return il risultato della sezione
	 */
	public boolean buyVocalSection() {

		boolean turnResult = true;
		Move move = null;
		String mex = "You chose to buy a vocal!";
		if (manche.points[playerOrder] < 1000) {
			mex += "\nYou haven't enough points to buy a vocal! Turn ended.";
			turnResult = false;
			move = new Move(MoveType.BUYVOCAL, 0, "Not enough points", true);
		} else
			mex += "\nWhen the timer start, you'll have 5 seconds to select a vocal!";

		callMethodOnAllListeners("updateLog", new Object[] { mex });

		if (turnResult) {

			try {
				sleep(3000);
			} catch (InterruptedException e) {}

			answer = "";
			mex = "You spent 1000 points to buy the vocal: ";
			manche.points[playerOrder] -= 1000;

			if (!startTimer(5000, 1) || answer.length() != 1) {
				callMethodOnAllListeners("updateLog",
						new Object[] { mex + "\nYou haven't selected any vocal. Turn ended." });
				turnResult = false;
				move = new Move(MoveType.BUYVOCAL, -1000, "Nothing", true);
			} else {
				char letter = answer.charAt(0);
				Integer[] pos = null;
				if (usedLetters.contains(letter))
					mex += letter + "\nThis letter has already been selected! Turn ended.";
				else {
					pos = manche.getLetter(1, letter);

					if (pos == null) {
						mex += letter + "\nWrong answer! Turn ended.";
						turnResult = false;
					} else {
						usedLetters.add(letter);
						mex += letter + "\n Right answer! the vocal " + letter + " appears " + pos.length + " times!";
						callMethodOnAllListeners("updateTable", new Object[] { pos, letter, manche.points });
					}
				}
				callMethodOnAllListeners("updateLog", new Object[] { mex });
				move = new Move(MoveType.BUYVOCAL, -1000, answer, pos == null ? true : false);
			}
		}

		turn.moves.add(move);
		return turnResult;
	}

	/**
	 * esegue la sezione in cui il giocatore indovina la soluzione
	 * @return il risultato della sezione
	 */
	public boolean giveSolutionSection() {

		boolean turnResult = true;
		answer = "";
		Move move = null;
		callMethodOnAllListeners("updateLog", new Object[] {
				"You chose to guess the solution!\nWhen the timer start, you'll have 5 seconds to enter the solution!" });
		try {
			sleep(3000);
		} catch (InterruptedException e) {}

		// scelto di dare la soluzione: MAX 10s
		if (!startTimer(10000, 1)) {
			turnResult = false;
			move = new Move(MoveType.GIVESOLUTION, 0, "Nothing", true);
		} else {
			String mex = "Solution given: " + answer;
			if (!answer.equals(manche.getSentence())) {
				turnResult = false;
				mex += "\nWrong answer! Turn ended.";
			} else {
				manche.winner = playerOrder;
				mex += "\nRight answer! You've won the manche! Your manche points have been added to your total points!\nManche ended.";
			}

			callMethodOnAllListeners("updateLog", new Object[] { mex });
			move = new Move(MoveType.GIVESOLUTION, 0, answer, !turnResult);
		}
		turn.moves.add(move);
		return turnResult;
	}

	/**
	 * metodo che seleziona il vincitore in base al punteggio totale.
	 * Nel caso di pareggio, il vincitore viene estratto casualmente
	 * @return il vincitore
	 */
	public int winnerSelectionSection() {
		List<Integer> winners = new ArrayList<>();
		int[] points = match.getScores();
		for (int i = 0; i < 3; i++) {
			if (points[i] >= points[(i + 1) % 3] && points[i] >= points[(i + 2) % 3])
				winners.add(i);
		}

		int winner = winners.get(0);
		String mex = "Match ended, ";
		if (winners.size() > 1) {
			String winnersNames = "";
			for (int i = 0; i < winners.size(); i++) {
				winnersNames += match.getPlayersList().get(i).getUsername();
				if (i < winners.size() - 1)
					winnersNames += ", ";
			}

			mex += "it's a draw! The winner will be selected randomly between: " + winnersNames + "...\n\n";
			winner = ThreadLocalRandom.current().nextInt(0, winners.size());
		}

		mex += "The winner is: " + match.getPlayersList().get(winner).getUsername();
		callMethodOnAllListeners("updateLog", new Object[] { mex });
		return winner;
	}

	/**
	 * metodo che salva tutti i dati necessari della manche sul DB
	 */
	private void saveDataOnDB() {
		if(error) return;
		MancheDAO mancheDAO = (MancheDAO) DAOFactory.getDAO("MancheDAO");
		mancheDAO.addWinner(manche, match.getPlayersList().get(manche.winner));
		mancheDAO.releaseConnection();
		UserDAO userDAO = (UserDAO) DAOFactory.getDAO("UserDAO");
		userDAO.UserHasPlayedAMancheBatch(match.getPlayersList(), manche);
		userDAO.userHasObservedAMancheBatch(match.getObserversList(), manche);
		List<User> users = new ArrayList<User>(match.getObserversList());
		users.addAll(match.getPlayersList());
		userDAO.userHasSeenASentencBatch(users, manche.objSentence);
		userDAO.releaseConnection();
		MoveDAO moveDAO = (MoveDAO) DAOFactory.getDAO("MoveDAO");
		moveDAO.insertMovesFromMancheBatch(manche);
		moveDAO.releaseConnection();
		if(error) return;
		ScoreDAO scoreDAO = (ScoreDAO) DAOFactory.getDAO("ScoreDAO");
		scoreDAO.insertMancheScoresBatch(manche, match.getPlayersList());
		scoreDAO.releaseConnection();
	}

	/**
	 * metodo che gestisce le attese eseguite durante il gioco
	 * @param t tempo d'attesa
	 * @param gameSection la sezione del gioco al momento dell'attesa
	 * @return booleana che indica se l'attesa � stata interrotta da un player
	 */
	public boolean startTimer(int t, int gameSection) {
		if (error)
			return false;

		callMethodOnAllListeners("setTimer", new Object[] { t, gameSection });
		waiting = true;
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			waiting = false;
		}
		boolean ret = !waiting;
		waiting = false;
		return ret;
	}

	/**
	 * metodo per chiedere l'utilizzo di un jolly ad un player
	 * @return la scelta del player
	 */
	public boolean askJolly() {
		if (match.jollies[playerOrder] > 0) {

			move = -1;
			if (startTimer(5000, 3) && move == 3) {
				match.jollies[playerOrder]--;
				callMethodOnAllListeners("updateJollies", new Object[] { match.jollies, playerOrder });
				Move move = new Move(MoveType.USEJOLLY, 0, "", false);
				turn.moves.add(move);
				return true;
			}
		}
		return false;
	}

	/**
	 * metodo chiamato dai player per dare le risposte
	 */
	@Override
	public void giveAnswer(String answer) throws RemoteException {
		this.answer = answer;
		if (!waiting)
			return;
		interrupt();
	}

	/**
	 * metodo chiamato dai player per indicare la mossa scelta
	 */
	@Override
	public void selectMove(int move) throws RemoteException {
		this.move = move;
		if (!waiting)
			return;
		interrupt();
	}

	/**
	 * metodo chiamato dai player per abbandonare la partita
	 */
	public void leaveGame(GameManagerInterface gameManager) throws RemoteException {
		listeners.remove(gameManager);
		if (gameManager instanceof PlayerGameManagerInterface) {
			callMethodOnAllListeners("endGame", new Object[] { true });
			error = true;
			if (!waiting)
				return;
			interrupt();
		}
	}

}
