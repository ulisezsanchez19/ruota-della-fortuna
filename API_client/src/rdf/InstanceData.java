package rdf;

import java.io.IOException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import rdf.API.User;
import rdf.API.interfaces.serverRdFInterface;
import rdf.controllers.GameViewController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * classe statica utilizzata dal client. Viene usata per tenere le variabili generali del programma
 * e per navigare le finestre.
 *
 */
public class InstanceData {

	public static boolean isAdmin;
	public static User user;
	public static serverRdFInterface stub;
	public static ClientRdF ThisclientRdF;
	public static Stage stage;
	public static GameManager gameManager;
	public static String[] userStatisticsCache;
	public static LobbyListener lobbyListener;

	static public void gotoAuthentication() {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InstanceData.class.getResource("/rdf.view/AuthenticationView.fxml"));
			Scene authenticationScene = new Scene(loader.load(), 1280, 720);
			changeScene(authenticationScene);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public void gotoHome() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InstanceData.class.getResource("/rdf.view/HomeView.fxml"));
			changeScene(new Scene(loader.load(), 1280, 720));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static public void gotoLobbyList() {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InstanceData.class.getResource("/rdf.view/LobbyListView.fxml"));
			changeScene(new Scene(loader.load(), 1280, 720));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public void gotoGame() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InstanceData.class.getResource("/rdf.view/GameView.fxml"));
			Scene gameScene = new Scene(loader.load(), 1280, 720);
			GameViewController controller = loader.getController();
			controller.initializeNewGame();
			gameManager.gameViewController = controller;
			userStatisticsCache = null;
			changeScene(gameScene);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void gotoStatistics() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InstanceData.class.getResource("/rdf.view/StatisticsView.fxml"));
			changeScene(new Scene(loader.load(), 1280, 720));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void gotoSentencesManager() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InstanceData.class.getResource("/rdf.view/SentenceManagerView.fxml"));
			changeScene(new Scene(loader.load(), 1280, 720));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void gotoProfile() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InstanceData.class.getResource("/rdf.view/ProfileView.fxml"));
			changeScene(new Scene(loader.load(), 1280, 720));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void gotoRankings() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InstanceData.class.getResource("/rdf.view/RankingsView.fxml"));
			changeScene(new Scene(loader.load(), 1280, 720));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void changeScene(Scene scene) {
		Platform.runLater(() -> {
			stage.setScene(scene);
			stage.setResizable(false);
			stage.show();
		});
	}

	/**
	 * metodo usato per disconnettersi
	 * @param notifyServer booleana che indica se avvisare o meno il server
	 */
	public static void returnToAuthentication(boolean notifyServer) {
		
		if (lobbyListener != null) {
			try {
				lobbyListener.leaveLobby();
				UnicastRemoteObject.unexportObject(lobbyListener, true);
			} catch (NoSuchObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lobbyListener = null;
		}
		
		if(notifyServer)
		{
			try {
				InstanceData.stub.getAccountManager().logout(user, ThisclientRdF);
			} catch (RemoteException e) {
				InstanceData.interruptClient();
			}
		}
		try {
			UnicastRemoteObject.unexportObject(ThisclientRdF, true);
		} catch (NoSuchObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		user = null;
		userStatisticsCache = null;
		lobbyListener = null;

		gotoAuthentication();
	}

	public static void interruptClient() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText("Could not connect to ServerRdF");
		alert.setContentText("Please check your network connection or try again later!");

		alert.showAndWait();
		System.err.println("ERROR: Server not found!");
		System.exit(0);
	}

}
